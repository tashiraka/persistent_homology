#!/bin/bash

data_dir=data/GSE117874
bin_dir=bin


jids=()

#GM12878 used
for name3dg in $(find ${data_dir}/gm12878_*.3dg | xargs -n 1 basename)
do
    name=${name3dg%.3dg}

    echo "Job of ${name}"
    
    out_dir=products/${name}
    if [ ! -d $out_dir ]; then mkdir $out_dir; fi

    log=${out_dir}/${name}.log

    threedg=${data_dir}/${name}.3dg
    prefix=${out_dir}/${name}
    xyz=${prefix}.xyz
    filtration=${prefix}.filtration
    phat_input=${prefix}.filtration_for_phat
    map_phat_to_xyz=${prefix}.map_phat_to_xyz
    persistence=${prefix}.ph

    cforest=${prefix}.cforest
    cforest_pre=${cforest}_pre
    cforest_filtered=${cforest}_filtered

    cforest_inside=${cforest}_inside
    cforest_inside_pre=${cforest_inside}_pre
    cforest_inside_filtered=${cforest_inside}_filtered

    transcript=data/RNAseq_Barbara/ENCFF300QDV.tsv
    annotation=data/RNAseq_Barbara/gencode.v29lift37.annotation.gtf
    tpm_xyz=${out_dir}/tpm_xyz
    tpm=${cforest_filtered}_tpm
    tpm_inside=${cforest_inside_filtered}_tpm

    tpm_pval=${tpm}_pval
    tpm_inside_pval=${tpm_inside}_pval

    num_samples=(100000) #(100 500 1000 2000 5000 7500 10000 100000)
    rounds=1 #10
    
    qsub -S /bin/bash -V -cwd -l mem_free=5g -o ${log} -e ${log} \
                  -j y -r y -N ${name}_ph <<EOF
#!/bin/bash
                                                                                                               
    set -x
    #time bash ${bin_dir}/dipCFormat3dg_to_xyz.sh $threedg $xyz
    #time ${bin_dir}/compute_filtration $xyz $filtration
    #time ${bin_dir}/change_format_of_filtration_for_phat $filtration $phat_input $map_phat_to_xyz
    #time ${bin_dir}/phat --verbose --ascii --vector_vector --standard $phat_input $persistence
    #time ${bin_dir}/compute_cavity_tree $persistence $cforest_pre
    #time ${bin_dir}/finalize $cforest_pre $map_phat_to_xyz $phat_input $xyz $cforest $cforest_inside

    #time ${bin_dir}/filter_cavities $cforest $xyz $cforest_filtered
    #time ${bin_dir}/filter_cavities $cforest_inside $xyz $cforest_inside_filtered

    #time ${bin_dir}/map_transcripts_to_points $transcript $annotation $threedg $tpm_xyz
    #time ${bin_dir}/map_transcripts_to_cavities $tpm_xyz $cforest_filtered $tpm
    #time ${bin_dir}/map_transcripts_to_cavities $tpm_xyz $cforest_inside_filtered $tpm_inside
    set +x
EOF


    
    for num_sample in ${num_samples[@]}
    do
        for round in `seq 1 ${rounds}`
        do
            tpm_pval=${tpm}_pval_${num_sample}_${round}
            tpm_inside_pval=${tpm_inside}_pval_${num_sample}_${round}
            jid=${name}_pval_${num_sample}_${round}
            jids+=(${jid})
            
            qsub -S /bin/bash -V -cwd -pe smp 12 -l mem_free=5g -o ${log} -e ${log} \
                 -j y -r y -N $jid -hold_jid ${name}_ph <<EOF
            set -x
            #time ${bin_dir}/test_transcription_enrichment $num_sample $cforest_filtered $xyz $tpm_xyz $tpm_pval
            #time ${bin_dir}/test_transcription_enrichment $num_sample $cforest_inside_filtered $xyz $tpm_xyz $tpm_inside_pval
            set +x
EOF
        done
    done
done



log=products/intersect.log

tpms=()
for elem in $(find products/gm12878_*/gm12878_*.cforest_filtered_tpm)
do
    tpms+=($elem)
done

tpm_xyzs=()
for elem in $(find products/gm12878_*/tpm_xyz)
do
    tpm_xyzs+=($elem)
done
            
cforests=()
for elem in $(find products/gm12878_*/gm12878_*.cforest_filtered)
do
    cforests+=($elem)
done

threedgs=()
for elem in $(find ${data_dir}/gm12878_*.3dg)
do
    threedgs+=($elem)
done

xyzs=()
for elem in $(find products/gm12878_*/gm12878_*.xyz)
do
    xyzs+=($elem)
done

common_genes=products/common_genes
common_loci=products/common_loci
function join_by { local IFS="$1"; shift; echo "$*"; }
jids_str=$(join_by , ${jids[@]})


qsub -S /bin/bash -V -cwd -pe smp 1 -l mem_free=5g -o ${log} -e ${log} \
     -j y -r y -N intersection -hold_jid $jids_str <<EOF

set -x
#time ${bin_dir}/intersect_cave_faced_genes_between_replicates ${tpms[@]} $common_genes
time ${bin_dir}/intersect_cave_faced_loci_between_replicates ${threedgs[@]} ${cforests[@]} $common_loci
set +x
EOF

num_samples=(100000) #(100 500 1000 2000 5000 7500 10000 100000)
rounds=1 #10


for num_sample in ${num_samples[@]}
do
    for round in $(seq 1 ${rounds})
    do
        num_gene_pval=${common_genes}_pval_${num_sample}_${round}
        num_loci_pval=${common_loci}_pval_${num_sample}_${round}

        qsub -S /bin/bash -V -cwd -pe smp 24 -l mem_free=5g -o ${log} -e ${log} \
             -j y -r y -N pval_intersection_${num_sample}_${round} -hold_jid intersection <<EOF
set -x
#        time ${bin_dir}/test_gene_intersection $num_sample $common_genes ${cforests[@]} ${xyzs[@]} ${tpm_xyzs[@]} $num_gene_pval
set +x
EOF
        qsub -S /bin/bash -V -cwd -pe smp 24 -l mem_free=5g -o ${log} -e ${log} \
             -j y -r y -N pval_intersection_${num_sample}_${round} -hold_jid intersection <<EOF
set -x
        time ${bin_dir}/test_loci_intersection $num_sample $common_loci ${threedgs[@]} ${cforests[@]} ${xyzs[@]} $num_loci_pval
set +x
EOF
    done
done

set +x
