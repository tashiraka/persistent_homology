import sys
import matplotlib.pyplot as plt
import numpy as np

persistence_volume_filename = sys.argv[1]
out_filename = sys.argv[2]


x = []
y = []

fin = open(persistence_volume_filename)

line = fin.readline()
while line:
    if line[0] != '$':
        fields = line.strip('\n').strip().split()

        birth = float(fields[0])
        death = float(fields[1])
        volume = float(fields[2])
        persistence = death - birth
        
        if persistence > 0:
            x += [persistence]
            y += [volume]
        
    line = fin.readline()


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.scatter(x, y, marker='.')
ax.set_xlabel("Persistence")
ax.set_ylabel("Volume")
plt.savefig(out_filename)
#plt.show()
#plt.close()
