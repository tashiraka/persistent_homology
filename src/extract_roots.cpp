#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>


typedef std::pair<double, std::vector<long>> map_type;
typedef std::pair<long, std::vector<long>> phat_type;

class Node {
public:
  double birth, death;
  std::string pIDs;
  std::vector<std::string> transcripts;
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  
  Node(double b, double d, std::string pids, std::vector<std::string> t)
    : birth(b), death(d), pIDs(pids), transcripts(t) {};
  ~Node() {};
};


class Ctree {
private:
  Node* root;
  
  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _load_ctree(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      std::string pIDs;
      ss >> pIDs;
      
      std::vector<std::string> tpms;
      std::string tpm;
      while (ss >> tpm)
        tpms.push_back(tpm);

      // new root
      Node* n = new Node(birth, death, pIDs, tpms);      
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;
      
      _load_ctree(ifs, n, true);
      _load_ctree(ifs, n, false);
    }
  }


  size_t _size(Node* n) {
    if (n) {
      return _size(n->left) + 1 + _size(n->right);
    }
    else return 0;
  }
  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree() {deleteNode(root);}

  size_t size() {return _size(root);}
  double root_persistence() {return root->death - root->birth;}
  
  void load_ctree(std::ifstream& ifs) {
    _load_ctree(ifs, root, true);
    _load_ctree(ifs, root, false);
  }

  void save_root(std::ofstream& ofs) const {
    ofs << root->birth << " " << root->death << " " << root->pIDs;
    
      for (const auto p : root->transcripts)
        ofs << " " << p;
      ofs << "\n";
  }
};


class Cforest {
private:
  std::vector<Ctree*> ctrees;

public:
  Cforest() {};
  ~Cforest() {};

  void load_ph_tree_with_transcripts(const std::string ph_tree_filename) {
    std::ifstream ifs(ph_tree_filename);
    std::string line;

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      std::string pIDs;
      ss >> pIDs;

      std::vector<std::string> tpms;
      std::string tpm;
      while (ss >> tpm)
        tpms.push_back(tpm);

      // new root
      Node* root = new Node(birth, death, pIDs, tpms);      
      Ctree* ctree = new Ctree(root);
      
      ctree->load_ctree(ifs);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  void save_roots(const std::string out_filename) {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees) {
      ctree->save_root(ofs);
    }
    ofs.close();
  }
};


void extract_roots(const std::string ph_tree_filename,
                        const std::string out_filename) {
  Cforest cforest;
  std::cout << "Load" << std::endl;
  cforest.load_ph_tree_with_transcripts(ph_tree_filename);

  std::cout << "Save" << std::endl;  
  cforest.save_roots(out_filename);
}

              
int main(int argc, char** argv) {
  std::string ph_tree_filename = argv[1];
  std::string out_filename = argv[2];

  extract_roots(ph_tree_filename, out_filename);
}
