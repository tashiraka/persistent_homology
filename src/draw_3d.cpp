#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cmath>
#include <GL/glut.h>
#include <algorithm>


class Point
{
public:
  double x, y, z;
  double CpG;
  
  Point(const double a,
        const double b,
        const double c,
        const double cpg)
    : x(a), y(b), z(c), CpG(cpg) {}
};


// globals
std::vector<Point> points;
GLdouble radius = 0.015;
float alpha = 1;
GLfloat c[4]{0, 0.5, 1, alpha};  
bool clipped_flag = false;
GLdouble current_angles[3] = {0, 0, 0};
GLdouble front_z = 0.015;
GLdouble back_z = -0.015;
double line_width = radius / 10.0;
GLfloat c_line[4]{0, 0, 0, alpha};


GLdouble* get_plane_eq(std::vector<GLdouble*>& v)
{
  GLdouble a = (v[1][1] - v[0][1]) * (v[2][2] - v[0][2]) - (v[2][1] - v[0][1]) * (v[1][2] - v[0][2]);
  GLdouble b = (v[1][2] - v[0][2]) * (v[2][0] - v[0][0]) - (v[2][2] - v[0][2]) * (v[1][0] - v[0][0]);
  GLdouble c = (v[1][0] - v[0][0]) * (v[2][1] - v[0][1]) - (v[2][0] - v[0][0]) * (v[1][1] - v[0][1]);
  GLdouble d = -(a * v[0][0] + b * v[0][1] + c * v[0][2]);

  return new GLdouble[4]{a, b, c, d};
}



void render_spheres(const std::vector<Point> points,
                    const double radius)
{
  std::cout << "sphere" << std::endl;
  for (const auto& p : points) {
    glPushMatrix();
    glTranslatef(p.x, p.y, p.z);
    glutSolidSphere(radius, 20, 5);
    glPopMatrix();
  }
  std::cout << "end" << std::endl;
}


double distance_from_front_clip_plane(const Point& p,
                                      const double na, const double nb, const double nc)
{
  return std::fabs(na*p.x + nb*p.y + nc*p.z - front_z) / std::sqrt(na*na + nb*nb + nc*nc);
}


void add_stencil_of_back(const std::vector<Point> points,
                         const double radius,
                         const double radius_threshold,
                         const double n[3])
{
  glStencilOp(GL_KEEP, GL_KEEP, GL_INCR);
  // face culling GL_BACK and quadric orientation GLU_INSIDE are a pair
  glCullFace(GL_BACK);

  glPushMatrix();
  glRotatef(current_angles[0], 1, 0, 0);
  glRotatef(current_angles[1], 0, 1, 0);
  glRotatef(current_angles[2], 0, 0, 1);
  
  for (const auto& p : points) {
    /*
      The front clip plane z = front_z is rotated by a [rad] around x axis and b [rad] around y axis.
      The transformed normal vector of the new clip plane is 
      (p, q, r) = [sin(-b), -sin(-a) * cos(-b), cos(-a) * cos(-b)].
      The equation of the transformed plane is 
      f(x,y,z) = sin(-b)*x - sin(-a)*cos(-b)*y + cos(-a)*cos(-b)*(z - front_z) = 0.
      The distance between a point (u, v, w) and the transformed plane is calculated as
      fabs(f(u, v, w)) / sqrt(p^2 + q^2 + r^2).
     */
    if (distance_from_front_clip_plane(p, n[0], n[1], n[2]) <= radius_threshold) {
      glPushMatrix();
      glTranslatef(p.x, p.y, p.z);
        
      // These lines from the source code of glutSolidSphere
      // I added gluQuadricOrientation(quadObj1, GLU_INSIDE);
      GLUquadricObj *quadObj = gluNewQuadric();
      gluQuadricDrawStyle(quadObj, GLU_FILL);
      gluQuadricNormals(quadObj, GLU_SMOOTH);
      gluQuadricOrientation(quadObj, GLU_INSIDE); // GLU_INSIDE
      gluSphere(quadObj, radius, 20, 5);

      glPopMatrix();
    }
  }

  glPopMatrix();
}


void sub_stencil_of_front(const std::vector<Point> points,
                          const double radius,
                          const double radius_threshold,
                          const double n[3])
{
  glStencilOp(GL_KEEP, GL_KEEP, GL_DECR);
  // face culling GL_FRONT and quadric orientation GLU_INSIDE are a pair
  glCullFace(GL_FRONT);

  glPushMatrix();
  glRotatef(current_angles[0], 1, 0, 0);
  glRotatef(current_angles[1], 0, 1, 0);
  glRotatef(current_angles[2], 0, 0, 1);

  for (const auto& p : points) {
    if (distance_from_front_clip_plane(p, n[0], n[1], n[2]) <= radius_threshold) {
      glPushMatrix();
      glTranslatef(p.x, p.y, p.z);

      // These lines from the source code of glutSolidSphere
      // I added gluQuadricOrientation(quadObj1, GLU_INSIDE);
      GLUquadricObj *quadObj = gluNewQuadric();
      gluQuadricDrawStyle(quadObj, GLU_FILL);
      gluQuadricNormals(quadObj, GLU_SMOOTH);
      gluQuadricOrientation(quadObj, GLU_INSIDE); // GLU_INSIDE
      gluSphere(quadObj, radius, 20, 5);

      glPopMatrix();
    }
  }

  glPopMatrix();
}


static void cb_draw()
{
  glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);

  glPushMatrix();
  glRotatef(current_angles[0], 1, 0, 0);
  glRotatef(current_angles[1], 0, 1, 0);
  glRotatef(current_angles[2], 0, 0, 1);

  glMaterialfv(GL_FRONT, GL_DIFFUSE, c);
  render_spheres(points, radius);

  glPopMatrix();
  glFlush();
  glutSwapBuffers();
}


void draw_clipped_edge()
{
  clipped_flag = true;
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, c);

  double normal_of_front_clip_plane[3] =
    {std::sin(-current_angles[1] * 2.0 * M_PI / 360.0),
     -std::sin(-current_angles[0] * 2.0 * M_PI / 360.0) * std::cos(-current_angles[1] * 2.0 * M_PI / 360.0),
     std::cos(-current_angles[0] * 2.0 * M_PI / 360.0) * std::cos(-current_angles[1] * 2.0 * M_PI / 360.0)};
  
  // Points defining a cliping plane
  std::vector<GLdouble*> v(4);
  v[0] = new GLdouble[3]{1, 1, front_z};
  v[1] = new GLdouble[3]{1, -1, front_z};
  v[2] = new GLdouble[3]{-1, -1, front_z};
  v[3] = new GLdouble[3]{-1, 1, front_z};

  // Clipping
  GLuint FRONT_CLIP_PLANE = GL_CLIP_PLANE0;
  GLdouble* front_clip_plane_eq = get_plane_eq(v);
  glEnable(FRONT_CLIP_PLANE);
  glClipPlane(FRONT_CLIP_PLANE, front_clip_plane_eq);
    
  // Stencil
  // A glutSolidSphere destroyed the front texture of a clipped edge
  // which was drawn as a plane through a stencil.
  // I did not why it happens, but I solved this problem.
  // When I changed gluQuadricOrientation from GLU_OUTSIDE to GLU_INSIDE
  // and flip glCullFace between GL_FRONT and GL_BACK,
  // the clipped of a sphere was correctly colored.
  //
  // Create the black edge lines
  //
  // Make a stencil slightly small than that for the edge face
  glEnable(GL_STENCIL_TEST);
  glClear(GL_STENCIL_BUFFER_BIT);
  glDisable(GL_DEPTH_TEST);
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
  
  glStencilFunc(GL_ALWAYS, 0, 0);
  glEnable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);

  // + back stencil
  glMaterialfv(GL_FRONT, GL_DIFFUSE, c_line);
  add_stencil_of_back(points, radius, radius, normal_of_front_clip_plane);
    
  // - front stencil
  glMaterialfv(GL_FRONT, GL_DIFFUSE, c_line);
  sub_stencil_of_front(points, radius, radius, normal_of_front_clip_plane);

  glDisable(GL_CULL_FACE);
  
  // stencil test will pass only when stencil buffer value = 0; (~0 = 0x11...11)
  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glEnable(GL_DEPTH_TEST);
  glDisable(FRONT_CLIP_PLANE);
  glStencilFunc(GL_NOTEQUAL, 0, ~0);

  glBegin(GL_QUADS);
  for (int j=3; j>=0; j--) {
    glMaterialfv(GL_FRONT, GL_DIFFUSE, c_line);
    glVertex3dv(v[j]);
  }
  glEnd();

  //
  // Create edge face
  //
  glClear(GL_STENCIL_BUFFER_BIT);
  glDisable(GL_DEPTH_TEST);
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
  
  glStencilFunc(GL_ALWAYS, 0, 0);
  glEnable(GL_CULL_FACE);
  glEnable(FRONT_CLIP_PLANE);
  glDisable(GL_DEPTH_TEST);

  // + back stencil
  glMaterialfv(GL_FRONT, GL_DIFFUSE, c);
  add_stencil_of_back(points, std::max(radius - line_width, 0.0), radius, normal_of_front_clip_plane);
    
  // - front stencil
  glMaterialfv(GL_FRONT, GL_DIFFUSE, c);
  sub_stencil_of_front(points, std::max(radius - line_width, 0.0), radius, normal_of_front_clip_plane);
    
  glDisable(GL_CULL_FACE);
  
  // stencil test will pass only when stencil buffer value = 0; (~0 = 0x11...11)
  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glEnable(GL_DEPTH_TEST);
  glDisable(FRONT_CLIP_PLANE);
  glStencilFunc(GL_NOTEQUAL, 0, ~0);

  glBegin(GL_QUADS);
  for (int j=3; j>=0; j--) {
    glMaterialfv(GL_FRONT, GL_DIFFUSE, c);
    glVertex3dv(v[j]);
  }
  glEnd();
        
  glDisable(GL_STENCIL_TEST);
  glEnable(FRONT_CLIP_PLANE);

  // Clipping back
  std::vector<GLdouble*> v2(4);
  v2[0] = new GLdouble[3]{1, 1, back_z};
  v2[1] = new GLdouble[3]{1, -1, back_z};
  v2[2] = new GLdouble[3]{-1, -1, back_z};
  v2[3] = new GLdouble[3]{-1, 1, back_z};

  GLuint BACK_CLIP_PLANE = GL_CLIP_PLANE1;
  GLdouble* back_clip_plane_eq = get_plane_eq(v2);
  for (int i=0; i<4; i++) *(back_clip_plane_eq + i) *= -1;
  std::cout << *(back_clip_plane_eq) << " " << *(back_clip_plane_eq + 1) << " " << *(back_clip_plane_eq + 2) << " " << *(back_clip_plane_eq + 3) << std::endl;
  glEnable(BACK_CLIP_PLANE);
  glClipPlane(BACK_CLIP_PLANE, back_clip_plane_eq);
    
  // Render spheres
  glMaterialfv(GL_FRONT, GL_DIFFUSE, c);

  glPushMatrix();
  glRotatef(current_angles[0], 1, 0, 0);
  glRotatef(current_angles[1], 0, 1, 0);
  glRotatef(current_angles[2], 0, 0, 1);

  render_spheres(points, radius);
  glPopMatrix();
  
  glFlush();
  glutSwapBuffers();
}

static void rotate(int axis, float x)
{
  const float step = 5.0f;
  current_angles[axis] = current_angles[axis] + step * x;

  if (clipped_flag) draw_clipped_edge();
  else cb_draw();
}



static void cb_special_key(int key, int x, int y)
{
  if (key == GLUT_KEY_UP)         rotate(0, -1.0f);
  else if (key == GLUT_KEY_DOWN)  rotate(0, 1.0f);
  else if (key == GLUT_KEY_LEFT)  rotate(1, -1.0f);
  else if (key == GLUT_KEY_RIGHT) rotate(1, 1.0f);
}


void load_points(const std::string filename)
{
  std::ifstream ifs(filename);
  std::string line;
  
  while (std::getline(ifs, line)) {
    if (line[0] != '#') {
      std::stringstream ss(line);

      std::string chrom;
      int loc;
      double x, y, z, CpG;

      ss >> chrom >> loc >> x >> y >> z >> CpG;

      points.push_back(Point(x, y, z, CpG));
    }
  }
  
  ifs.close();
}



static void cb_key(unsigned char key, int x, int y)
{
  if (key == '0') {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    double PlaneEq[] = {0, 0, -1, 0.01};
    glEnable(GL_CLIP_PLANE0);
    glClipPlane(GL_CLIP_PLANE0, PlaneEq);

    glMaterialfv(GL_FRONT, GL_DIFFUSE, c);
    glPushMatrix();
    glRotatef(current_angles[0], 1, 0, 0);
    glRotatef(current_angles[1], 0, 1, 0);
    glRotatef(current_angles[2], 0, 0, 1);
    
    render_spheres(points, radius);

    glPopMatrix();
    glFlush();
    glutSwapBuffers();
  }
  else if (key == '1') {
    draw_clipped_edge();
  }
  else if (key == 'x') {
    glDisable(GL_CLIP_PLANE0);
    glDisable(GL_CLIP_PLANE1);
    cb_draw();
  }
  else if (key == 'q') {
    std::exit(0);
  }
}


double l2_dist(const Point& p, const Point& q)
{
  double x = p.x - q.x;
  double y = p.y - q.y;
  double z = p.z - q.z;
  return std::sqrt(x*x + y*y + z*z);
}


void scale(std::vector<Point>& points)
{
  // Translate the center of the point set to (0,0,0)
  Point center(0,0,0,0);
  
  for (const auto& p : points) {
    center.x += p.x;
    center.y += p.y;
    center.z += p.z;
  }

  center.x /= points.size();
  center.y /= points.size();
  center.z /= points.size();
  
  // Limit the radius of the point set
  double max_radius = 0;
  for (const auto& p : points) {
    auto d = l2_dist(p, center);
    max_radius = d > max_radius ? d : max_radius;
  }

  // Change the Scale of the point set to fit the visualization window
  for (auto& p : points) {
    p.x = (p.x - center.x) / max_radius;
    p.y = (p.y - center.y) / max_radius;
    p.z = (p.z - center.z) / max_radius;
  }
}

int main(int argc, char** argv) {
  std::string threedg_filename = argv[1];
  //std::string ph_tree_filename = argv[2];
  //std::string out_filename = argv[3];

  load_points(threedg_filename);

  scale(points);
  
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_ALPHA | GLUT_DOUBLE);

  int width = 700;
  glutInitWindowSize(width, width);
  glutCreateWindow("View3D");

  // Camera
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum(-1, 1, -1, 1, 9, 11);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(0, 0, 10, 0, 0, 0, 0, 1, 0);
  
  GLfloat light_position[] = { 0.5, 0.5, 3.0, 0.0 };
  GLfloat white_light[] = { 1.0, 1.0, 1.0, 1.0 };

  glLightfv(GL_LIGHT0, GL_POSITION, light_position);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
  glLightfv(GL_LIGHT0, GL_AMBIENT, white_light);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  glutDisplayFunc(cb_draw);
  glutKeyboardFunc(cb_key);
  glutSpecialFunc(cb_special_key);

  glutMainLoop();
}
