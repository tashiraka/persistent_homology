#include <iostream>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <set>
#include <unordered_set>
#include <random>
#include <limits>
#include <omp.h>



size_t get_num_points_of_caves(const std::string filename)
{
  std::ifstream ifs(filename);
  std::string line, not_used;
  std::unordered_set<int> point_ids;
  int pid;
  
  while (std::getline(ifs, line)) {
    std::stringstream ss(line);

    if (line[0] != '!' && line[0] != '$') {
      // birth, death, net persistence, volume
      ss >> not_used >> not_used >> not_used >> not_used;
      while (ss >> pid) point_ids.insert(pid);
    }
  }

  ifs.close();
  return point_ids.size();
}


size_t get_num_points(const std::string xyz_filename)
{
  size_t count = 0;
  std::ifstream ifs(xyz_filename);
  std::string line;
  while (std::getline(ifs, line)) count++;
  return count;
}


std::vector<size_t> get_num_common_loci(const std::string filename)
{
  std::vector<size_t> counts;
  std::ifstream ifs(filename);
  std::string line;
  size_t num_lines = 1;
  
  while (std::getline(ifs, line)) {
    std::stringstream ss(line);
    std::string col;
    size_t k;

    ss >> k;

    if (k != num_lines++) {
      std::cerr << "Error: Missing the number of intersection" << std::endl;
      std::exit(1);
    }
    
    size_t count = 0;
    
    while (std::getline(ss, col, ' '))
      count++;

    counts.push_back(count);
  }
  
  return counts;
}



void permutation_test(const int bin_size,
                      const int num_mergin_bins,
                      const size_t num_samples,
                      const std::string common_loci_file,
                      const std::vector<std::string>& cforest_filenames,
                      const std::vector<std::string>& threedg_filenames,
                      const std::vector<std::string> xyz_filenames,
                      const std::string out_filename)
{
  std::cout << "Get the number of the common genes" << std::endl;
  std::vector<size_t> obs_num_common_loci = get_num_common_loci(common_loci_file);
  
  std::cout << "Load cavity trees" << std::endl;
  size_t num_replicates = cforest_filenames.size();
  std::vector<size_t> num_points_of_caves;
  for (const auto& filename : cforest_filenames)
    num_points_of_caves.push_back(get_num_points_of_caves(filename));

  std::cout << "Get the number of points" << std::endl;
  std::vector<size_t> num_points;
  for (const auto& filename : xyz_filenames)
    num_points.push_back(get_num_points(filename));
  
  std::cout << "Load all loci" << std::endl;
  std::vector<std::vector<std::pair<std::string, int>>> map_point_id_to_loci;

  for (size_t rep=0; rep<num_replicates; rep++) {
    auto threedg_filename = threedg_filenames[rep];
    // Load the loci of points
    std::ifstream ifs_threedg(threedg_filename);
    std::vector<std::pair<std::string, int>> tmp_map_point_id_to_loci;
    std::string line;
    
    while (std::getline(ifs_threedg, line)) {
      if (line[0] != '#') {
        std::stringstream ss(line);
        
        std::string chrom;
        int loc;
        ss >> chrom >> loc;

        // Remove the annotation of diploids, e.g. 1a to 1
        std::string chrom_hap = chrom.substr(0, chrom.size() - 1);

        tmp_map_point_id_to_loci.push_back(std::make_pair(chrom_hap, loc));
      }
    }

    map_point_id_to_loci.push_back(tmp_map_point_id_to_loci);
  }

  
  std::cout << "Perform permutation test" << std::endl;
  #pragma omp declare reduction(vec_int_plus : std::vector<int> : \
                              std::transform(omp_out.begin(), omp_out.end(), \
                                             omp_in.begin(), omp_out.begin(), std::plus<int>())) \
                                             initializer(omp_priv = omp_orig)

  std::vector<int> pval_counts(num_replicates, 0);

  #pragma omp parallel for num_threads(24) reduction(vec_int_plus : pval_counts)
  for (size_t i=0; i<num_samples; i++) {
    /*
     * Sampling
     */
    // A k-th element is the set of gene IDs of k-intersection among replicates
    std::vector<std::vector<std::string>> loci;

    // Sample randamized caves and gene IDs on them
    std::random_device seed_gen;
    //std::mt19937 mt(seed_gen());
    std::default_random_engine mt(seed_gen());

    for (size_t k=0; k<num_replicates; k++) {
      const auto n = num_points_of_caves[k];
      std::uniform_int_distribution<> rand_uni(0, num_points[k] - 1);

      // Randamize the positions of the caves
      std::unordered_set<int> sampled_pids;
      size_t counter = 0;

      while (sampled_pids.size() < n) {
        // Sometimes sagmentation fault occur this line. I guess this is a bug of gcc.
        // This may be solved by replacing mt19937 with default_random_engine
        int sample = rand_uni(mt);
        
        sampled_pids.insert(sample);
        if (counter++ > num_points[k]) {
          std::cerr << "Error: this random sample method is inefficient " << counter << std::endl;
          std::exit(1);
        }
      }

      std::vector<std::string> tmp;
      for (const auto pid : sampled_pids) {
        auto chrom_hap = map_point_id_to_loci[k][pid].first;
        auto loc = map_point_id_to_loci[k][pid].second;
        tmp.push_back(chrom_hap + "_" + std::to_string(loc));

        // Push bins around the center
        for (int a=bin_size; a<(num_mergin_bins+1)*bin_size; a+=bin_size) {
          tmp.push_back(chrom_hap + "_" + std::to_string(loc - a));
          tmp.push_back(chrom_hap + "_" + std::to_string(loc + a));
        }      
      }

      std::sort(tmp.begin(), tmp.end());
      loci.push_back(tmp);
    }


    /*
     * Intersection
     */
    // Get variable-sized intersection
    for (size_t k=0; k<num_replicates; k++) {
      // intersection of k replicates
      std::unordered_set<std::string> common_loci;
      bool first_flag = true;

      std::string bitmask(k + 1, 1);
      bitmask.resize(num_replicates, 0);

      do {
        // Get a k-intersection
        std::vector<std::string> tmp_common_loci;
      
        for (size_t j = 0; j < num_replicates; j++) {
          if (bitmask[j]) {

            if (first_flag) {
              tmp_common_loci = loci[j];
              first_flag = false;
            }
            else {      
              std::vector<std::string> tmp_out;
              std::set_intersection(tmp_common_loci.begin(), tmp_common_loci.end(),
                                    loci[j].begin(), loci[j].end(),
                                    std::back_inserter(tmp_out));
              tmp_common_loci = tmp_out;
            }
          }
        }

        common_loci.insert(tmp_common_loci.begin(), tmp_common_loci.end());

      } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
    
      // Count p value
      if (common_loci.size() > obs_num_common_loci[k])
        pval_counts[k]++;
    }
  }
  

  std::cout << "Save p values" << std::endl;
  std::ofstream ofs(out_filename);

  for (size_t i=0; i<num_replicates; i++) {
    double pval = pval_counts[i]; // int to double
    pval /= num_samples;
    ofs << (i + 1) << " " << pval << "\n";
    std::cout << (i + 1) << " " << pval << "\n";
  }
  ofs.close();
}



int main(int argc, char** argv)
{
  int num_replicates = (argc - 3) / 3;
  int bin_size = 20000;
  int num_mergin_bins = 4;

  size_t num_samples = std::atoi(argv[1]);
  std::string common_loci_file = argv[2];

  // Three dg
  std::vector<std::string> threedg_filenames;
  for (int i=3; i<(num_replicates + 3); i++)
    threedg_filenames.push_back(argv[i]);

  // Point IDs around caves
  std::vector<std::string> cforest_filenames;
  for (int i=num_replicates + 3; i<(num_replicates * 2 + 3); i++)
    cforest_filenames.push_back(argv[i]);

  std::vector<std::string> xyz_filenames;
  for (int i=(num_replicates * 2 + 3); i<(num_replicates * 3 + 3); i++)
    xyz_filenames.push_back(argv[i]);

  std::string out_filename = argv[argc - 1];
  
  permutation_test(bin_size, num_mergin_bins, num_samples, common_loci_file,
                   cforest_filenames, threedg_filenames,
                   xyz_filenames, out_filename);
}
