#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Delaunay_triangulation_cell_base_3.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unordered_map>
#include <limits>
#include <cassert>


// Configuration and types used in Delaunay triangulation
typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_3<unsigned, K> Vb;
typedef CGAL::Delaunay_triangulation_cell_base_3<K> Cb;
typedef CGAL::Triangulation_data_structure_3<Vb, Cb> Tds;
typedef CGAL::Delaunay_triangulation_3<K, Tds, CGAL::Fast_location> Dt;
typedef Dt::Point Point;
typedef Dt::Vertex Vertex;
typedef Dt::Edge Edge;
typedef Dt::Facet Facet;
typedef Dt::Cell Cell;
typedef Dt::Cell_handle Cell_handle;
typedef Dt::Vertex_handle Vertex_handle;



class Alpha_complex: Dt
{
private:
  /*
  using Dt::finite_facets_begin;
  using Dt::finite_facets_end;
  using Dt::finite_edges_begin;
  using Dt::finite_edges_end;
  using Dt::finite_vertices_begin;
  using Dt::finite_vertices_end;
  using Dt::finite_cells_begin;
  using Dt::finite_cells_end;
  using Dt::is_infinite;
  using Dt::incident_edges;
  using Dt::incident_facets;
  */
  // Maps the correspondence of alpha values and simplices
  typedef std::multimap<double, Vertex_handle> Alpha_vertex_map;
  typedef std::multimap<double, Facet> Alpha_facet_map;
  typedef std::multimap<double, Edge> Alpha_edge_map;
  typedef std::multimap<double, Cell_handle> Alpha_cell_map;

  // Maps the simplices and boundaris
  // The order is the same as the map of alpha value to simplices defined above.
  typedef std::vector<std::vector<long>> Vertex_boundary_map;
  typedef std::vector<std::vector<long>> Edge_boundary_map;
  typedef std::vector<std::vector<long>> Facet_boundary_map;
  typedef std::vector<std::vector<long>> Cell_boundary_map;  

  // Maps the simplices to the index in the order of alpha value
  typedef std::unordered_map<Vertex*, long> Vertex_idx_map;
  typedef std::unordered_map<Cell*,
                             std::unordered_map<long,
                                                std::unordered_map<long, long> > > Edge_idx_map;
  typedef std::unordered_map<Cell*, std::unordered_map<long, long>> Facet_idx_map;
  typedef std::unordered_map<Cell*, long> Cell_idx_map;  
  
  Alpha_vertex_map alpha_vertex_map;
  Alpha_edge_map alpha_edge_map;  
  Alpha_facet_map alpha_facet_map;
  Alpha_cell_map alpha_cell_map;

  Vertex_boundary_map vertex_boundary_map;
  Edge_boundary_map edge_boundary_map;
  Facet_boundary_map facet_boundary_map;
  Cell_boundary_map cell_boundary_map;  
  
  Vertex_idx_map vertex_idx_map;
  Edge_idx_map edge_idx_map;
  Facet_idx_map facet_idx_map;
  Cell_idx_map cell_idx_map;  

  
  //The p0 lies out the minimum circumsphere of p1 and p2 => true.
  //otherwise => false.
  bool is_Gabriel(const long& i0, const Cell_handle& ch,
                  const long& i1, const long& i2) const
  {
    auto p0 = ch->vertex(i0)->point();
    auto p1 = ch->vertex(i1)->point();
    auto p2 = ch->vertex(i2)->point();
    auto midp = CGAL::midpoint(p1, p2);
    return CGAL::squared_distance(midp, p0) > CGAL::squared_radius(p1, p2);
  }

  // The p0 lies out the minimum circumsphere of p1, p2 and p3 => true.
  // otherwise => false.
  bool is_Gabriel(const long& i0, const Cell_handle& ch,
                  const long& i1, const long& i2, const long& i3) const
  {
    auto p0 = ch->vertex(i0)->point();
    auto p1 = ch->vertex(i1)->point();
    auto p2 = ch->vertex(i2)->point();
    auto p3 = ch->vertex(i3)->point();
    auto cc = CGAL::circumcenter(p1, p2, p3);
    return CGAL::squared_distance(p0, cc) > CGAL::squared_radius(p1, p2, p3);
  }

  double alpha(const Vertex_handle& v) const
  {
    return 0;
  }

  double alpha(const Edge& e) const
  {
    return sqrt(CGAL::to_double
                (CGAL::squared_radius(e.first->vertex(e.second)->point(),
                                      e.first->vertex(e.third)->point())));
  }

  double alpha(const Cell_handle& ch, const long& i0, const long& i1) const
  {
    return sqrt(CGAL::to_double
                (CGAL::squared_radius(ch->vertex(i0)->point(),
                                      ch->vertex(i1)->point())));
  }
  
  double alpha(const Cell_handle& ch,
               const long& i0, const long& i1, const long& i2) const
  {
    return sqrt(CGAL::to_double
                (CGAL::squared_radius(ch->vertex(i0)->point(),
                                      ch->vertex(i1)->point(),
                                      ch->vertex(i2)->point())));
  }

  double alpha(const Cell_handle& ch) const
  {
    return sqrt(CGAL::to_double
                (CGAL::squared_radius(ch->vertex(0)->point(),
                                      ch->vertex(1)->point(),
                                      ch->vertex(2)->point(),
                                      ch->vertex(3)->point())));
  }
  
  void compute_vertex_alpha() {
    for (auto it=Dt::all_vertices_begin(); it!=Dt::all_vertices_end(); ++it) {
      if (Dt::is_infinite(it)) 
        alpha_vertex_map.insert(Alpha_vertex_map::value_type(std::numeric_limits<double>::infinity(), it));
      else
        alpha_vertex_map.insert(Alpha_vertex_map::value_type(alpha(it), it));
    }
  }

  void compute_edge_alpha() {
    for(auto it=Dt::all_edges_begin(); it!=Dt::all_edges_end(); ++it) {
      if(Dt::is_infinite(*it))
        alpha_edge_map.insert(Alpha_edge_map::value_type(std::numeric_limits<double>::infinity(), *it));
      else
        alpha_edge_map.insert(Alpha_edge_map::value_type(alpha(*it), *it));
    } 
  }

  double select_facet_alpha(const Cell_handle& ch,
                            const std::vector<long>& index) const
  {
    if(!is_Gabriel(index[0], ch, index[1], index[2])) {
      return alpha(ch, index[1], index[2]);
    }
    else if(!is_Gabriel(index[1], ch, index[0], index[2])) {
      return alpha(ch, index[0], index[2]);
    }
    else if(!is_Gabriel(index[2], ch, index[0], index[1])) {
      return alpha(ch, index[0], index[1]);
    }
    else {
      return alpha(ch, index[0], index[1], index[2]);
    }       
  }

  double max_facet_alpha(const Cell_handle& ch) const
  {
    auto a0 = select_facet_alpha(ch, {1, 2, 3});
    auto a1 = select_facet_alpha(ch, {0, 2, 3});
    auto a2 = select_facet_alpha(ch, {0, 1, 3});
    auto a3 = select_facet_alpha(ch, {0, 1, 2});
    return std::max({a0, a1, a2, a3});
  }
  
  void compute_facet_alpha() {
    for(auto it=Dt::all_facets_begin(); it!=Dt::all_facets_end(); ++it) {
      // Get three indices of the veticies of the facet
      std::vector<long> index{0, 1, 2, 3};
      index.erase(index.begin() + it->second);  

      if(Dt::is_infinite(*it)) {
        alpha_facet_map.insert(Alpha_facet_map::value_type
                               (std::numeric_limits<double>::infinity(), *it));
      }
      else {
        alpha_facet_map.insert(Alpha_facet_map::value_type
                               (select_facet_alpha(it->first, index), *it));
      }
    }
  }
  
  void compute_cell_alpha()
  {
    for(auto it=Dt::all_cells_begin(); it!=Dt::all_cells_end(); ++it) {
      if(Dt::is_infinite(it)) {
        alpha_cell_map.insert(Alpha_cell_map::value_type(std::numeric_limits<double>::infinity(), it));
      }
      else {
        if(!is_Gabriel(0, it, 1, 2, 3) || !is_Gabriel(1, it, 0, 2, 3)
           || !is_Gabriel(2, it, 0, 1, 3) || !is_Gabriel(3, it, 0, 1, 2))
          {
            alpha_cell_map.insert(Alpha_cell_map::value_type(max_facet_alpha(it), it));
          }
        else {
          alpha_cell_map.insert(Alpha_cell_map::value_type(alpha(it), it));
        }
      }
    }  
  }
  
public:
  Alpha_complex(std::vector< std::pair<Point, unsigned> >& points) {
    // Compute the Delaunay triangulation     
    insert(points.begin(), points.end());
  }


  void compute_alpha_values() {
    // Infinite simplices have the infinit value of double type
    compute_vertex_alpha();
    compute_edge_alpha();
    compute_facet_alpha();
    compute_cell_alpha();
  }
  

  /*
   * Associate the simplices with their boundaries
   *
   * The boundaries must be stored as the indices in the order of the alpha values.
   * Those indices were used to compute persistent homology.
   */ 
  void associate_boundary()
  {
    // Did all simplices have alpha values?
    assert(alpha_vertex_map.size() == Dt::number_of_vertices() + 1); // + 1 indicates the infinite vertex
    assert(alpha_edge_map.size() == Dt::number_of_edges());
    assert(alpha_facet_map.size() == Dt::number_of_facets());    
    assert(alpha_cell_map.size() == Dt::number_of_cells());
    
    // Initialize boundary maps
    vertex_boundary_map.reserve(alpha_vertex_map.size());
    edge_boundary_map.reserve(alpha_edge_map.size());
    facet_boundary_map.reserve(alpha_facet_map.size());
    cell_boundary_map.reserve(alpha_cell_map.size());
    long idx = 0;

    std::cout << "Initialize associative array..." << std::endl;
    // Initialize vertex-index map
    idx = 0;
    for (auto it=alpha_vertex_map.begin(); it!=alpha_vertex_map.end(); ++it, idx++)
      vertex_idx_map.insert(Vertex_idx_map::value_type(&(*(it->second)), idx));

    // Were all vertices mapped?
    std::vector<Vertex*> mapped_v;
    for (auto& vi : vertex_idx_map) {
      mapped_v.push_back(vi.first);
    }
    std::sort(mapped_v.begin(), mapped_v.end());
    
    std::vector<Vertex*> alpha_v;
    for (auto& av : alpha_vertex_map) {
      alpha_v.push_back(&(*(av.second)));
    }
    std::sort(alpha_v.begin(), alpha_v.end());

    assert(mapped_v.size() == alpha_v.size());
    for (size_t i=0; i<mapped_v.size(); i++)
      assert(mapped_v[i] == alpha_v[i]);

    // Initialize edge-index map
    /*
     * Edge is triple.
     * CGAL triple seems not to have default constructor.
     * So it cannot be used for the key of C++ map.
     * Then, is the pointer of a CGAL triple is unique?
     * Cell and Vertex seem to be unique. 
     * But Edge and Facet could be copied and get different address per the copies.
     * The documents of CGAL is difficult for me. I cannot determine this is fact.
     * Finally, I am using triple-nested unordered_map.
     */
    /*
    for (auto eit=alpha_edge_map.begin(); eit!=alpha_edge_map.end(); ++eit) {
      Cell* c = &(*(eit->second.first));
      edge_idx_map.insert(std::make_pair(c,
                                         std::unordered_map<int,
                                         std::unordered_map<int, int> >{}));
    }
    for (auto eit=alpha_edge_map.begin(); eit!=alpha_edge_map.end(); ++eit) {
      Cell* c = &(*(eit->second.first));
      int idx1 = eit->second.second;
      edge_idx_map[c].insert(std::make_pair(idx1, std::unordered_map<int, int>{}));
    }
    idx = 0;
    for (auto eit=alpha_edge_map.begin(); eit!=alpha_edge_map.end(); ++eit, idx++) {
      Cell* c = &(*(eit->second.first));
      int idx1 = eit->second.second;
      int idx2 = eit->second.third;
      edge_idx_map[c][idx1].insert(std::make_pair(idx2, idx));
    }
    */

    for (auto it=Dt::all_cells_begin(); it!=Dt::all_cells_end(); ++it) {
      Cell* c = &(*(it));
      edge_idx_map.insert(std::make_pair(c,
                                         std::unordered_map<long,
                                         std::unordered_map<long, long> >{}));
    }
    
    idx = 0;

    for (auto it=alpha_edge_map.begin(); it!=alpha_edge_map.end(); ++it, idx++) {
      Edge e = it->second;
      Cell_handle ch = e.first;
      long idx1 = e.second;
      long idx2 = e.third;
      Vertex_handle v1 = ch->vertex(idx1);
      Vertex_handle v2 = ch->vertex(idx2);
      
      auto cell_circulator = incident_cells(e);
      Dt::Cell_circulator cc_begin = cell_circulator;

      if (cell_circulator != 0) {
        do {
          Cell* c = &(*cell_circulator);
          long i1 = c->index(v1);
          long i2 = c->index(v2);
          
          edge_idx_map[c].insert(std::make_pair(i1, std::unordered_map<long, long>{}));
          edge_idx_map[c].insert(std::make_pair(i2, std::unordered_map<long, long>{}));
          edge_idx_map[c][i1].insert(std::make_pair(i2, idx));
          edge_idx_map[c][i2].insert(std::make_pair(i1, idx));
          
        } while (++cell_circulator != cc_begin);
      }
    }

    // Were all edges mapped?
    // First Cells
    std::vector<Cell*> mapped_ec;
    for (auto& ei : edge_idx_map) {
      mapped_ec.push_back(ei.first);
    }
    std::sort(mapped_ec.begin(), mapped_ec.end());
    
    std::vector<Cell*> alpha_ec;
    for (auto& ac : alpha_cell_map) {
      alpha_ec.push_back(&(*(ac.second)));
    }
    std::sort(alpha_ec.begin(), alpha_ec.end());

    assert(mapped_ec.size() == alpha_ec.size());
    for (size_t i=0; i<mapped_ec.size(); i++)
      assert(mapped_ec[i] == alpha_ec[i]);

    // Second all edges of the cells
    for (auto it=Dt::all_cells_begin(); it!=Dt::all_cells_end(); ++it) {
      auto indices = edge_idx_map[&(*it)];

      assert(indices.size() == 4);

      std::vector<long> idx1;
      for (const auto& i : indices) {
        idx1.push_back(i.first);
      }

      std::sort(idx1.begin(), idx1.end());
      for (long i=0; i<4; i++)
        assert(idx1[i] == i);
    }

    for (auto it=Dt::all_cells_begin(); it!=Dt::all_cells_end(); ++it) {
      auto indices = edge_idx_map[&(*it)];

      std::vector<long> idx1;
      for (const auto& i : indices) {
        idx1.push_back(i.first);

        assert(i.second.size() == 3);

        std::vector<long> idx2;
        for (const auto& j : i.second)
          idx2.push_back(j.first);

        std::sort(idx2.begin(), idx2.end());
        long jj = 0;
        for (long j=0; j<3; j++, jj++) {
          if (i.first == j) jj++;
          assert(idx2[j] == jj);
        }
      }
    }
    
    // Initialize facet-index map
    // Same as Edge
    for (auto it=Dt::all_cells_begin(); it!=Dt::all_cells_end(); ++it) {
      Cell* c = &(*it);
      facet_idx_map.insert(std::make_pair(c, std::unordered_map<long, long>{}));
    }
    
    idx = 0;

    for(auto it=alpha_facet_map.begin(); it!=alpha_facet_map.end(); ++it, idx++) {
      // Facet are included two cells
      Facet f = it->second;

      // First cell
      Cell_handle ch1 = f.first;
      Cell* c1 = &(*ch1);
      long idx1 = f.second;
      
      facet_idx_map[c1].insert(std::make_pair(idx1, idx));

      // Second cell
      Cell_handle ch2 = mirror_facet(f).first;
      Cell* c2 = &(*ch2);
      long idx2 = mirror_index(ch1, idx1);      

      facet_idx_map[c2].insert(std::make_pair(idx2, idx));
    }

    std::vector<Cell*> mapped_fc;
    for (auto& fi : facet_idx_map) {
      mapped_fc.push_back(fi.first);
    }
    std::sort(mapped_fc.begin(), mapped_fc.end());
    
    std::vector<Cell*> alpha_fc;
    for (auto& ac : alpha_cell_map) {
      alpha_fc.push_back(&(*(ac.second)));
    }
    std::sort(alpha_fc.begin(), alpha_fc.end());

    assert(mapped_fc.size() == alpha_fc.size());
    for (size_t i=0; i<mapped_fc.size(); i++)
      assert(mapped_fc[i] == alpha_fc[i]);

    // All facet
    for (auto it=Dt::all_cells_begin(); it!=Dt::all_cells_end(); ++it) {
      auto indices = facet_idx_map[&(*it)];

      assert(indices.size() == 4);

      std::vector<long> idx1;
      for (const auto& i : indices) {
        idx1.push_back(i.first);
      }

      std::sort(idx1.begin(), idx1.end());
      for (long i=0; i<4; i++)
        assert(idx1[i] == i);
    }

    
    // Initialize cell-index map
    idx = 0;
    for (auto cit=alpha_cell_map.begin(); cit!=alpha_cell_map.end(); ++cit, idx++)
      cell_idx_map.insert(Cell_idx_map::value_type(&(*cit->second), idx));

    // test
    std::vector<Cell*> mapped_cc;
    for (auto& ci : cell_idx_map) {
      mapped_cc.push_back(ci.first);
    }
    std::sort(mapped_cc.begin(), mapped_cc.end());
    
    std::vector<Cell*> alpha_cc;
    for (auto& ac : alpha_cell_map) {
      alpha_cc.push_back(&(*(ac.second)));
    }
    std::sort(alpha_cc.begin(), alpha_cc.end());

    assert(mapped_cc.size() == alpha_cc.size());
    for (size_t i=0; i<mapped_cc.size(); i++)
      assert(mapped_cc[i] == alpha_cc[i]);


    std::cout << "Associating vertices and edges..." << std::endl;
    // Iterate the vertices and associate them with the edges as boundaries
    idx = 0;
    for (auto vit=alpha_vertex_map.begin(); vit!=alpha_vertex_map.end(); ++vit, idx++) {
      // Get the incident edeges
      std::vector<Edge> incidents;
      incident_edges(vit->second, std::back_inserter(incidents));

      // Each incident edge has the vertex with the index, idx, as boundary.
      for (auto eit=incidents.begin(); eit!=incidents.end(); ++eit) {
        Cell* c = &(*(eit->first));
        long idx1 = eit->second;
        long idx2 = eit->third;

        edge_boundary_map[edge_idx_map[c][idx1][idx2]].push_back(idx);
      }
    }

    std::cout << "Associating edges and facets..." << std::endl;
    // Iterate the edges and associate them with the facets as boundaries
    idx = 0;
    for (auto eit=alpha_edge_map.begin(); eit!=alpha_edge_map.end(); ++eit, idx++) {
      // Get the incident facets
      auto facet_circulator = incident_facets(eit->second);
      Dt::Facet_circulator fc_begin = facet_circulator;

      if (facet_circulator != 0) {
        do {
          Cell* c = &(*(facet_circulator->first));
          long idx1 = facet_circulator->second;
          facet_boundary_map[facet_idx_map[c][idx1]].push_back(idx);
        } while (++facet_circulator != fc_begin);
      }
    }

    std::cout << "Associating facets and cells..." << std::endl;
    // Iterate the facets and associate them with the cell as boundaries
    idx = 0;
    for (auto fit=alpha_facet_map.begin(); fit!=alpha_facet_map.end(); ++fit, idx++) {
      // Each incident Cell has the facet with the index, idx, as boundary.
      // Facet has two incident cells in three-dimensional space.
      auto incident1 = fit->second.first;
      cell_boundary_map[cell_idx_map[&(*incident1)]].push_back(idx);

      auto incident2 = mirror_facet(fit->second).first;
      cell_boundary_map[cell_idx_map[&(*incident2)]].push_back(idx);
    }
  }

  
  // Output the alpha values and the indices of .xyz file by the order of the alpha values
  void output_filtration(const std::string out_file) const
  {
    // Multimap is already sorted by default.

    // Output vetices (dimension 0)
    std::string out_file_dim0 = out_file + "_0";
    std::ofstream fout0(out_file_dim0);

    // The alpha values of vertices are 0.
    // Vertices have no boundaries.
    for(auto vit=alpha_vertex_map.begin(); vit!=alpha_vertex_map.end(); ++vit) {
      if (Dt::is_infinite(vit->second)) continue;
      
      // Alpha value: vit->first
      // Point index: vit->second->info()
      fout0 << vit->first << "\t" << vit->second->info() << "\n";
    }
    
    fout0.close();
    

    // Output Edge (dimension 1)
    std::string out_file_dim1 = out_file + "_1";
    std::ofstream fout1(out_file_dim1);
    long idx = 0;
    
    for(auto eit=alpha_edge_map.begin(); eit!=alpha_edge_map.end(); ++eit, idx++) {
      if (Dt::is_infinite(eit->second)) continue;

      // Alpha value: eit->first
      // Point 1 index: eit->second.first->vertex(eit->second.second)->info()
      // Point 2 index: eit->second.first->vertex(eit->second.third)->info()
      std::vector<unsigned> point_idxs = {eit->second.first->vertex(eit->second.second)->info(),
                                          eit->second.first->vertex(eit->second.third)->info()};
      std::sort(point_idxs.begin(), point_idxs.end());

      fout1 << eit->first  << "\t" << point_idxs[0] << "\t" << point_idxs[1];

      // Output boundaries
      auto boundaries = edge_boundary_map[idx];
      std::sort(boundaries.begin(), boundaries.end());
      for (auto bit=boundaries.begin(); bit!=boundaries.end(); ++bit) {
        fout1 << "\t" << *bit;
      }

      fout1 << "\n";
    }

    fout1.close();

    
    // Output Facets (dimension 2)
    std::string out_file_dim2 = out_file + "_2";
    std::ofstream fout2(out_file_dim2);

    idx = 0;
    for(auto fit=alpha_facet_map.begin(); fit!=alpha_facet_map.end(); ++fit, idx++) {
      if (Dt::is_infinite(fit->second)) continue;

      // CGAL stores a facet as a cell and the index of an excluded vertex; facet.second
      // Get the other three indices included in the facet
      std::vector<long> indices{0, 1, 2, 3};
      indices.erase(indices.begin() + fit->second.second);

      // Alpha value: fit->first
      // Point 1 index; fit->second.first->vertex(indices[0])->info()
      // Point 2 index: fit->second.first->vertex(indices[1])->info()
      // Point 3 index: fit->second.first->vertex(indices[2])->info()
      
      // Sort the orther of the point indices
      std::vector<unsigned> point_idxs= {fit->second.first->vertex(indices[0])->info(),
                                         fit->second.first->vertex(indices[1])->info(),
                                         fit->second.first->vertex(indices[2])->info()};
      std::sort(point_idxs.begin(), point_idxs.end());
            
      fout2 << fit->first << "\t"
            << point_idxs[0] << "\t" << point_idxs[1] << "\t"<< point_idxs[2];
 
      // Output boundaries
      auto boundaries = facet_boundary_map[idx];
      std::sort(boundaries.begin(), boundaries.end());
      for (auto bit=boundaries.begin(); bit!=boundaries.end(); ++bit) {
        fout2 << "\t" << *bit;
      }

      fout2 << "\n";
    }

    fout2.close();

    
    // Output Cells (dimension 3)
    std::string out_file_dim3 = out_file + "_3";
    std::ofstream fout3(out_file_dim3);
    idx = 0;
    
    for(auto cit=alpha_cell_map.begin(); cit!=alpha_cell_map.end(); ++cit, idx++) {
      if (Dt::is_infinite(cit->second)) continue;

      // Alpha value: cit->first
      // Point 1 index: cit->second->vertex(0)->info()
      // Point 2 index: cit->second->vertex(1)->info()
      // Point 3 index: cit->second->vertex(2)->info()
      // Point 4 index: cit->second->vertex(3)->info()
      std::vector<unsigned> point_idxs = {cit->second->vertex(0)->info(),
                                          cit->second->vertex(1)->info(),
                                          cit->second->vertex(2)->info(),
                                          cit->second->vertex(3)->info()};
      std::sort(point_idxs.begin(), point_idxs.end());
      
      fout3 << cit->first << "\t"
           << point_idxs[0] << "\t" << point_idxs[1] << "\t"
           << point_idxs[2] << "\t" << point_idxs[3];

      // Output boundaries
      auto boundaries = cell_boundary_map[idx];
      std::sort(boundaries.begin(), boundaries.end());
      for (auto bit=boundaries.begin(); bit!=boundaries.end(); ++bit) {
        fout3 << "\t" << *bit;
      }

      fout3 << "\n";
    }

    fout3.close();
  }
};



int main(int argc, char** argv) {
  auto pointsFile = argv[1]; // .xyz format
  auto outFile = argv[2];
  
  // input points from pointsFile
  std::ifstream fin(pointsFile);
  std::vector< std::pair<Point, unsigned> > points;
  Point p;
  unsigned idx = 0;

  std::cout << "Reading xyz" << std::endl;
  while(fin >> p) {
    points.push_back(std::make_pair(p, idx));
    idx++;
  }

  // Compute the filtration of an alpha complex
  std::cout << "Construct Delaunay complex" << std::endl;  
  Alpha_complex ac(points);

  // Compute alpha values
  std::cout << "Computing filtration" << std::endl;  
  ac.compute_alpha_values();
  
  // Associate the simplices with their boundaries
  std::cout << "Computing boundaries" << std::endl;  
  ac.associate_boundary();
  
  // Output the filtration
  std::cout << "Writing" << std::endl;  
  ac.output_filtration(outFile);

  return 0;
}
