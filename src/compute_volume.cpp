#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <unordered_set>
#include <cmath>

class Point {
public:
  double x, y, z;
  double CpG;

  Point(double a, double b, double c, double cpg): x(a), y(b), z(c), CpG(cpg) {}
};

Point operator+(const Point& p, const Point& q) {
  return Point(p.x + q.x,
               p.y + q.y,
               p.z + q.z, 0);
}

Point operator-(const Point& p, const Point& q) {
  return Point(p.x - q.x,
               p.y - q.y,
               p.z - q.z, 0);
}

double dotProduct(const Point& u, const Point& v) {
  return u.x * v.x + u.y * v.y + u.z * v.z;
}


Point outerProduct(const Point& u, const Point& v)
{
  return Point(u.y * v.z - u.z * v.y,
               u.z * v.x - u.x * v.z,
               u.x * v.y - u.y * v.x, 0);
}


double tripleProduct(const Point&  u, const Point& v, const Point& w)
{
  auto outer = outerProduct(u, v);
  return dotProduct(outer, w);
}


double tetrahedron_volume(const Point& p1,
                          const Point& p2,
                          const Point& p3,
                          const Point& p4) {
  return std::abs(tripleProduct(p2 - p1, p3 - p1, p4 - p1)) / 6.0;
}


class Node {
public:
  double birth, death;
  std::vector<long> pIDs;
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  double volume;
  double CpG_average;
  
  Node(double b, double d, std::vector<long> c) : birth(b), death(d), pIDs(c) {};
  ~Node() {};

  void compute_volume(const std::vector<Point>& points) {
    if (pIDs.size() % 4 != 0) {
      std::cerr << "Error: point IDs do not represent tetrahedrons." << std::endl;
      std::exit(1);
    }
    else {
      volume = 0;

      int num_cells = pIDs.size() / 4;
      for (int i=0; i<num_cells; i++) {
        volume += tetrahedron_volume(points[pIDs[4 * i]],
                                     points[pIDs[4 * i + 1]],
                                     points[pIDs[4 * i + 2]],
                                     points[pIDs[4 * i + 3]]);
      }
    }
  }

  void compute_CpG_average(const std::vector<Point>& points) {
    std::unordered_set<long> s;
    for (const auto pid : pIDs) s.insert(pid);
    std::vector<long> uniq_pIDs;
    uniq_pIDs.assign(s.begin(), s.end());
        
    double sum = 0;
    for (const auto pid : uniq_pIDs) sum += points[pid].CpG;
    CpG_average = sum / uniq_pIDs.size();
  }
};


class Ctree {
private:
  Node* root;
  
  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _load_ctree(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;
        
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      Node* n = new Node(birth, death, pIDs);
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;
      
      _load_ctree(ifs, n, true);
      _load_ctree(ifs, n, false);
    }
  }

  
  void _compute_volume(const std::vector<Point>& points, Node* n) {
    if (!n) return;
    else {
      n->compute_volume(points);
      _compute_volume(points, n->left);
      _compute_volume(points, n->right);
    }
  }


  void _compute_CpG_average(const std::vector<Point>& points, Node* n) {
    if (!n) return;
    else {
      n->compute_CpG_average(points);
      _compute_CpG_average(points, n->left);
      _compute_CpG_average(points, n->right);
    }
  }

  
  void _save_depth_first_persistence_and_volume(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      ofs << n->birth << " " << n->death << " " << n->volume << "\n";

      _save_depth_first_persistence_and_volume(ofs, n->left);
      _save_depth_first_persistence_and_volume(ofs, n->right);
    }
  }

  void _save_depth_first_persistence_volume_CpG(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      ofs << n->birth << " " << n->death << " " << n->volume << " " << n->CpG_average << "\n";
      
      _save_depth_first_persistence_volume_CpG(ofs, n->left);
      _save_depth_first_persistence_volume_CpG(ofs, n->right);
    }
  }

  size_t _size(Node* n) {
    if (n) {
      return _size(n->left) + 1 + _size(n->right);
    }
    else return 0;
  }
  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree() {deleteNode(root);}

  size_t size() {return _size(root);}
  double root_persistence() {return root->death - root->birth;}
  
  void load_ctree(std::ifstream& ifs) {
    _load_ctree(ifs, root, true);
    _load_ctree(ifs, root, false);
  }

  void compute_volume(const std::vector<Point>& points) {
    _compute_volume(points, root);
  }

  void compute_CpG_average(const std::vector<Point>& points) {
    _compute_CpG_average(points, root);
  }
  
  void save_persistence_and_volume(std::ofstream& ofs) const {
    _save_depth_first_persistence_and_volume(ofs, root);
  }

  void save_persistence_volume_CpG(std::ofstream& ofs) const {
    _save_depth_first_persistence_volume_CpG(ofs, root);
  }
};


class Cforest {
private:
  std::vector<Ctree*> ctrees;

public:
  Cforest() {};
  ~Cforest() {};

  void load_ph_tree(const std::string ph_tree_filename) {
    std::ifstream ifs(ph_tree_filename);
    std::string line;

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;
        
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      // new root
      Node* root = new Node(birth, death, pIDs);      
      Ctree* ctree = new Ctree(root);
      
      ctree->load_ctree(ifs);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  void compute_volume(const std::vector<Point>& points) {
    for (auto& ctree : ctrees) ctree->compute_volume(points);
  }

  void compute_CpG_average(const std::vector<Point>& points) {
    for (auto& ctree : ctrees) ctree->compute_CpG_average(points);
  }
  
  void save_persistence_and_volume(const std::string out_filename) const {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees) {
      ctree->save_persistence_and_volume(ofs);
    }
    ofs.close();
  }

  void save_persistence_volume_CpG(const std::string out_filename) const {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees) {
      ctree->save_persistence_volume_CpG(ofs);
    }
    ofs.close();
  }
};


std::vector<Point> load_points(const std::string filename) {
  std::vector<Point> points;
  std::ifstream ifs(filename);
  std::string line;
  double x, y, z;
  
  while (std::getline(ifs, line)) {
    if (line[0] != '#') {
      std::stringstream ss(line);
      std::string chrom;
      int start;
      double CpG;
      
      ss >> chrom >> start >> x >> y >> z >> CpG;
      points.push_back(Point(x, y, z, CpG));
    }
  }

  return points;
}


void compute_volume(const std::string ph_tree_filename,
                    const std::string point_filename,
                    const std::string out_filename) {
  Cforest cforest;
  std::cout << "Load cavities" << std::endl;
  cforest.load_ph_tree(ph_tree_filename);

  std::cout << "Load points" << std::endl;
  std::vector<Point> points = load_points(point_filename);

  std::cout << "Compute volume" << std::endl;
  cforest.compute_volume(points);

  std::cout << "Compute CpG average" << std::endl;
  cforest.compute_CpG_average(points);
  
  std::cout << "Save" << std::endl;  
  cforest.save_persistence_volume_CpG(out_filename);
  //cforest.save_persistence_and_volume(out_filename);
}

              
int main(int argc, char** argv) {
  std::string ph_tree_filename = argv[1];
  std::string threedg_filename = argv[2];
  std::string out_filename = argv[3];

  compute_volume(ph_tree_filename, threedg_filename, out_filename);
}
