#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <cmath>


/**
   Node class
   
   The nodes of the cycle tree structure (binary tree)
   are implemented by double-linked list.
 */
class Node {
public:
  long birth, death;
  std::vector<long> cells; // sorted
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  
  Node(long b, long d, std::vector<long> c) : birth(b), death(d), cells(c) {};
  ~Node() {};

  size_t size() const {return cells.size();}

  bool include(const Node* const n) const {
    // Assuming that the birth time of root is larger than that of n.
    // Then, when root and n have a intersection, root must include n.
    // Assuming that the cells are sorted.
    // Then, the inclusion can be determined by comparing the first elements.
    if (std::find(cells.begin(), cells.end(), n->cells[0]) != cells.end()) return true;
    else return false;
  }
};



/**
   Ctree class

   Binary tree of cycles (or cavities)
 */
class Ctree {
private:
  Node* root;

  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _insert(Node* n, Node* target) {
    // Target must include n.
    // search for the insertion position which is always a leaf.
    if (!target->left && !target->right) {
      target->left = n;

      // interpolate the other leaf
      long birth = n->birth;  // birth at the same time as n
      long death = target->death; // death at the same time as target
      std::vector<long> cells;
      std::set_difference(target->cells.begin(), target->cells.end(),
                          n->cells.begin(), n->cells.end(),
                          std::back_inserter(cells));
      target->right = new Node(birth, death, cells);
      
      // insert
    }
    else if (target->left->include(n)) {
      _insert(n, target->left);
    }
    else if (target->right->include(n)) {
      _insert(n, target->right);
    }
  }

  void _save_depth_first(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      ofs << n->birth << " " << n->death;
      for (const auto c : n->cells)
        ofs << " " << c;
      ofs << "\n";
    
      _save_depth_first(ofs, n->left);
      _save_depth_first(ofs, n->right);
    }
  }


  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree() {deleteNode(root);}

  bool include(const Node* const n) const {return root->include(n);}
  void insert(Node* n) {_insert(n, root);};

  void save_cavities(std::ofstream& ofs) const {
    _save_depth_first(ofs, root);
  }
};


/**
   Cforest class

   The vector of ctrees
 */
class Cforest {
private:
  std::vector<Ctree*> ctrees;
  std::unordered_map<long, Ctree*> cell_ctree_map;
  
  void insert(const long birth, const long death, const std::vector<long> cells) {
    Node* n = new Node(birth, death, cells);

    /*
    for (auto& ctree : ctrees) {
      if (ctree->include(n)){
        ctree->insert(n);
        return;
      }
    }
    */

    if (cell_ctree_map.find(n->cells[0]) != cell_ctree_map.end()) {
      // new node is included a ctree
      cell_ctree_map[n->cells[0]]->insert(n);
    }
    else {
      // construct a new ctree
      auto new_ctree = new Ctree(n);
      ctrees.push_back(new_ctree);

      // construct map only of root node
      for (const auto cell : n->cells)
        cell_ctree_map.insert({cell, new_ctree});
    }
  }
  
public:
  Cforest() {};
  ~Cforest() {};

  // Load an output of the modified PHAT
  // Fortmat: birth_time death_time cell1 cell2 cell3 ...\n
  void load_persistence(const std::string filename) {
    std::string line;

    /*
    std::ifstream ifs_tmp(filename);
    int line_count = 0;
    while(std::getline(ifs_tmp, line)) line_count++;
    int out_count = line_count / 1000;
    */
          
    std::ifstream ifs(filename);

    ctrees.clear();
    long prev_birth = -1;
    //int count = 0;
    //int count2 = 0;
    
    while (std::getline(ifs, line)) {
      if (line.empty()) continue;

      /*
      count++;
      if (count % out_count == 0) {
        count2++;
        std::cout << count2 << " / " << 1000 << std::endl;
      }
      */
      
      std::stringstream ss(line);
      long birth;
      ss >> birth;

      if (prev_birth > birth ) {
        std::cerr << "Error: Birth times are not sorted." << std::endl;
        std::exit(1);
      }
      prev_birth = birth;
      
      long death;
      ss >> death;

      std::vector<long> cells;
      long cell, prev_cell=-1;
      while (ss >> cell) {
        if (prev_cell < cell) {
          cells.push_back(cell);
          prev_cell = cell;
        }
        else {
          std::cerr << "Error: Cell indices are not sorted." << std::endl;
          std::exit(1);
        }
      }

      insert(birth, death, cells);
    }
  }

  void save_cavities(const std::string filename) const {
    std::ofstream ofs(filename);

    for (const auto& ctree : ctrees)
      ctree->save_cavities(ofs);

    ofs.close();
  }
};


void compute_cavity_tree(const std::string& input_filename,
                         const std::string& output_filename)
{
  Cforest cforest;

  cforest.load_persistence(input_filename);
  cforest.save_cavities(output_filename);
}


int main( int argc, char** argv )
{
  const std::string input_filename = argv[1]; // sorted by birth time
  const std::string output_filename = argv[2];
  
  compute_cavity_tree(input_filename, output_filename);

  return 0;
}
