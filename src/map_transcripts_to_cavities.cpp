#include <iostream>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <set>
#include <unordered_set>


class Transcript {
public:
  std::string gene_id;
  double TPM;
  std::vector<long> point_ids;
  
  Transcript() {};
  Transcript(const std::string gid, const double tpm, const std::vector<long> pids)
    : gene_id(gid), TPM(tpm), point_ids(pids) {}
};


std::vector<Transcript> load_transcript(const std::string transcript_filename) {
  std::vector<Transcript> transcripts;
  std::ifstream ifs(transcript_filename);
  std::string line;

  // Load each line
  std::getline(ifs, line); // header
    
  while (std::getline(ifs, line)) {
    std::stringstream ss(line);
    std::string gene_id, pids;
    double TPM;
    ss >> gene_id >> TPM >> pids;

    std::vector<long> point_ids;
    std::stringstream ss2(pids);
    std::string pid;
    while (std::getline(ss2, pid, ','))
      point_ids.push_back(std::stol(pid));
    
    transcripts.push_back(Transcript(gene_id, TPM, point_ids));
  }

  return transcripts;
}



class Node {
public:
  double birth, death, net_persistence, volume;
  std::vector<long> pIDs;
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  std::vector<Transcript*> transcripts;
  bool is_valid_net_persistence = true;
  
  Node(double b, double d, double np, double v, std::vector<long> c)
    : birth(b), death(d), net_persistence(np), volume(v), pIDs(c) {};
  ~Node() {};
};


class Ctree {
private:
  Node* root;
  
  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _load(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);

      std::string not_used;
      bool is_valid_net_persistence = true;
      if (line[0] == '!') {
        ss >> not_used;
        is_valid_net_persistence = false;
      }
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      Node* n = new Node(birth, death, net_persistence, volume, pIDs);
      n->is_valid_net_persistence = is_valid_net_persistence;
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;
      
      _load(ifs, n, true);
      _load(ifs, n, false);
    }
  }

  void _map_pids_to_caves(std::unordered_map<long, std::vector<Node*>*>& map_pids_to_caves, Node* n) {
    if (!n) {
      return;
    }
    else {
      // Remove duplicates
      std::vector<long> pIDs;
      std::set<long> s;
      for (const auto p : n->pIDs) s.insert(p);
      pIDs.assign(s.begin(), s.end());

      for (const auto p : pIDs) {
        // [] accessor makes entry if it does not exist.
        if (!map_pids_to_caves[p]) map_pids_to_caves[p] = new std::vector<Node*>; 
        map_pids_to_caves[p]->push_back(n);
      }
      
      _map_pids_to_caves(map_pids_to_caves, n->left);
      _map_pids_to_caves(map_pids_to_caves, n->right);
    }
  }

  void _remove_duplicated_genes(Node* n) {
    if (!n) {
      return;
    }
    else {
      // Remove duplicates
      std::unordered_set<Transcript*> s;
      for (const auto t : n->transcripts) s.insert(t);
      n->transcripts.assign(s.begin(), s.end());      
      
      _remove_duplicated_genes(n->left);
      _remove_duplicated_genes(n->right);
    }
  }
  
  void _save_depth_first_transcripts(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      if (!n->is_valid_net_persistence)
        ofs << "! ";

      if (n->transcripts.size() > 0) {
        ofs << n->transcripts[0]->gene_id << " " << n->transcripts[0]->TPM;
        for (size_t i=1; i<n->transcripts.size(); i++) {
          ofs << " " << n->transcripts[i]->gene_id << " " << n->transcripts[i]->TPM;
        }
      }
      
      ofs << "\n";
      
      _save_depth_first_transcripts(ofs, n->left);
      _save_depth_first_transcripts(ofs, n->right);
    }
  }

  size_t _size(Node* n) {
    if (n) {
      return _size(n->left) + 1 + _size(n->right);
    }
    else return 0;
  }
  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree() {deleteNode(root);}

  size_t size() {return _size(root);}
  double root_persistence() {return root->death - root->birth;}
  
  void load_ctree(std::ifstream& ifs) {
    _load(ifs, root, true);
    _load(ifs, root, false);
  }

  void map_pids_to_caves(std::unordered_map<long, std::vector<Node*>*>& map_pids_to_caves) {
    _map_pids_to_caves(map_pids_to_caves, root);
  } 

  void remove_duplicated_genes() {
    _remove_duplicated_genes(root);
  }
  
  void save_transcripts(std::ofstream& ofs) const {
    _save_depth_first_transcripts(ofs, root);
  }
};


class Cforest {
private:
  std::vector<Ctree*> ctrees;

public:
  Cforest() {};
  ~Cforest() {};

  void load(const std::string filename) {
    std::ifstream ifs(filename);
    std::string line;

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);

      std::string not_used;
      bool is_valid_net_persistence = true;
      if (line[0] == '!') {
        ss >> not_used;
        is_valid_net_persistence = false;
      }

      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      // new root
      Node* root = new Node(birth, death, net_persistence, volume, pIDs);
      root->is_valid_net_persistence = is_valid_net_persistence;
      Ctree* ctree = new Ctree(root);
      
      ctree->load_ctree(ifs);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  void map_pids_to_caves(std::unordered_map<long, std::vector<Node*>*>& map_pids_to_caves) {
    for (const auto& ctree : ctrees)
      ctree->map_pids_to_caves(map_pids_to_caves);
  }

  void remove_duplicated_genes() {
    for (const auto& ctree : ctrees)
      ctree->remove_duplicated_genes();
  }
  
  void save_with_transcripts(const std::string out_filename) const {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees) {
      ctree->save_transcripts(ofs);
    }
    ofs.close();
  }
};



void assign_transcripts(std::vector<Transcript>& transcripts,
                        std::unordered_map<long, std::vector<Node*>*>& map_pids_to_caves,
                        Cforest& cforest) {
  for (auto& transcript : transcripts) {
    for (const auto pid : transcript.point_ids) {
      if (map_pids_to_caves.find(pid) != map_pids_to_caves.end()) {
        for (auto& n : *(map_pids_to_caves.at(pid))) {
          n->transcripts.push_back(&transcript);
        }
      }
    }
  }

  // Remove duplicated genes
  cforest.remove_duplicated_genes();
}


void map_transcripts_to_caves(const std::string transcript_filename,
                               const std::string cforest_filename,
                               const std::string out_filename) {
  std::cout << "Load transcripts at points" << std::endl;
  auto transcripts = load_transcript(transcript_filename);

  std::cout << "Load cavity trees" << std::endl;
  Cforest cforest;
  cforest.load(cforest_filename);

  std::cout << "Map point ID to caves" << std::endl;
  std::unordered_map<long, std::vector<Node*>*> map_pids_to_caves;
  cforest.map_pids_to_caves(map_pids_to_caves);

  std::cout << "Assign transcripts to caves using the map" << std::endl;
  assign_transcripts(transcripts, map_pids_to_caves, cforest);

  std::cout << "Save" << std::endl;
  cforest.save_with_transcripts(out_filename);
}



int main(int argc, char** argv) {
  std::string transcript_filename = argv[1];
  std::string ctree_filename = argv[2];
  std::string out_filename = argv[3];

  map_transcripts_to_caves(transcript_filename, ctree_filename, out_filename);
}
