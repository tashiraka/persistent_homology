import std.stdio, std.string, std.conv, std.array, std.algorithm;


version(unittest)
{}
 else {
   void main(string[] args)
   {
     string filePrefix = args[1]; // e.g. gm12878_01.filtration
     string outFilename = args[2]; // e.g. gm12878_01.filtration_for_phat
     string ctableOutFilename = args[3]; // e.g. gm12878_01.corresponding_table_phat_points
     
     int maxDim = 3;
     Simplex[] simplices;
     // Construct map between the simplex IDs in single dimension to the simplex IDs in all dimension
     long[][] singleDimToAllDims;
     singleDimToAllDims.length = maxDim + 1;

     
     // Read a filtration
     writeln("Rading filtration");

     foreach (dim; 0..(maxDim + 1)) {
       auto filename = filePrefix ~ "_" ~ dim.to!string;
       long IDInSingleDim = 0;
       
       foreach (line; File(filename).byLineCopy) {
         auto fields = line.strip.split;

         auto alphaValue = fields[0].to!double;
         auto pointIDs = fields[1..(dim + 2)].map!(x => x.to!long).array;
         auto boundaryIDs = fields[(dim + 2)..$].map!(x => x.to!long).array();

         simplices ~= new Simplex(dim, alphaValue, pointIDs, IDInSingleDim, boundaryIDs);

         IDInSingleDim++;
       }

       singleDimToAllDims[dim].length = IDInSingleDim;
     }

     
     // Sort all simplices by alpha values
     writeln("Sorting filtration");
     sort!((a, b) => a.alphaValue != b.alphaValue ? a.alphaValue < b.alphaValue : a.dim < b.dim)(simplices);

     
     // Replace the boundary IDs with serial number of all simplices.
     writeln("Replacing IDs");
     foreach (long i, ref simplex; simplices) {
       simplex.IDInAllDims = i;
       singleDimToAllDims[simplex.dim][simplex.IDInSingleDim] = i;
     }

     foreach (ref simplex; simplices) {
       simplex.boundaryIDsInAllDims =
         simplex.boundaryIDsInSingleDim.map!(x => singleDimToAllDims[simplex.dim - 1][x]).array;
       sort(simplex.boundaryIDsInAllDims);
     }

     // Output data as the format for PHAT
     writeln("Writing IDs and corresponding table");
     auto fout = File(outFilename, "w"); 

     // Output the correspondence table between the indices of PHAT filtration and those of the points in .xyz
     // Real value of alpha value too.
     auto foutCtable = File(ctableOutFilename, "w");
    
     foreach (simplex; simplices) {
       auto boundaryString = simplex.boundaryIDsInAllDims.map!(x => x.to!string).join(" ");
       auto pIDString = simplex.pointIDs.map!(x => x.to!string).join(" ");

       fout.writeln(simplex.dim, " ", boundaryString);

       // the order is the indices of the PHAT filtration
       foutCtable.writeln(simplex.alphaValue.to!string, " ", pIDString);
     }
   }
 }



class Simplex
{
  int dim;
  double alphaValue;
  long[] pointIDs;
  
  long IDInSingleDim;
  long[] boundaryIDsInSingleDim;

  long IDInAllDims;
  long[] boundaryIDsInAllDims;
  

  this (int d, double a, long[] pIDs, long id, long[] b)
  {
    dim = d;
    alphaValue = a;
    pointIDs = pIDs;
    IDInSingleDim = id;
    boundaryIDsInSingleDim = b;
  }
}
