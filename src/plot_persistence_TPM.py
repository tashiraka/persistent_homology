import sys
import matplotlib.pyplot as plt
import numpy as np

tpm_filename = sys.argv[1]
out_filename = sys.argv[2]


x = []
y = []

fin = open(tpm_filename)

line = fin.readline()
while line:
    if line[0] != '$':
        fields = line.strip('\n').strip().split()

        birth = float(fields[0])
        death = float(fields[1])
        persistence = death - birth
        if persistence > 0:
            x += [persistence]
            size = 1#len(set(fields[2].split(',')))
            
            if len(fields) == 3:
                y += [0]
            else:
                y += [sum([float(elem.split(',')[1]) for elem in fields[3:]]) / size]
        
    line = fin.readline()


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.scatter(x, np.array(y) + 1, marker='.')
#ax.scatter(x, y, marker='.')
ax.set_xlabel("Persistence")
ax.set_ylabel("TPM")
ax.set_yscale("log")
#ax.set_xscale("log")
plt.savefig(out_filename)
#plt.show()
#plt.close()
