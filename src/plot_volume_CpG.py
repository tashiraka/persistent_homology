import sys
import matplotlib.pyplot as plt
import numpy as np

volume_filename = sys.argv[1]
out_filename = sys.argv[2]


x = []
y = []

fin_vol = open(volume_filename)
line_vol = fin_vol.readline()


while line_vol:
    if line_vol[0] != '$':
        fields_vol = line_vol.strip('\n').strip().split()

        birth = float(fields_vol[0])
        death = float(fields_vol[1])        
        persistence = death - birth        
        volume = float(fields_vol[2])
        CpG = float(fields_vol[3])
        
        if persistence > 0:
            x += [volume]
            y += [CpG]
            
    line_vol = fin_vol.readline()


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.scatter(np.array(x) + 1, np.array(y) + 1, marker='.')
#ax.scatter(x, np.array(y) + 1, marker='.')
ax.scatter(x, y, marker='.')
ax.set_xlabel("Volume")
ax.set_ylabel("CpG")
#ax.set_xlim(0, 0.4)
#ax.set_ylim(0, 100000)
#ax.set_yscale("log")
#ax.set_xscale("log")
plt.savefig(out_filename)
#plt.show()
#plt.close()
