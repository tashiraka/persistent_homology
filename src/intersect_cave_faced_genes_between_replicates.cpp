#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <unordered_set>
#include <vector>


void intersect_transcribed_genes(const std::vector<std::string>& transcript_filenames,
                                 const std::string out_filename)
{
  size_t num_replicates = transcript_filenames.size();

  // Get the intersection of transcribed genes among replicates
  std::vector<std::vector<std::string>> gene_ids;

  for (const auto& filename : transcript_filenames) {
    std::ifstream ifs(filename);
    std::unordered_set<std::string> tmp_gene_ids;
    std::string line;
    
    while(std::getline(ifs, line)) {

      if (line[0] != '!' and line[0] != '$') {        
        std::stringstream ss(line);
        std::string gene_id;
        double TPM;        
        
        while(ss >> gene_id >> TPM)
          if (TPM > 0) // Extracting transcribed genes.
            tmp_gene_ids.insert(gene_id);
      }
    }

    std::vector<std::string> tmp;
    tmp.assign(tmp_gene_ids.begin(), tmp_gene_ids.end());
    std::sort(tmp.begin(), tmp.end()); // Sorted here!
    gene_ids.push_back(tmp);
  }

  std::ofstream ofs(out_filename);

  for (size_t k=0; k<num_replicates; k++) {
    // intersection of k replicates
    std::unordered_set<std::string> common_gene_ids;
    
    std::string bitmask(k + 1, 1);
    bitmask.resize(num_replicates, 0);
    
    bool first_flag = true;

    do {
      // Get a k-intersection
      std::vector<std::string> tmp_common_gene_ids;
      
      for (size_t j = 0; j < num_replicates; j++) {
        if (bitmask[j]) {
          if (first_flag) {
            // gene_ids must be sorted
            tmp_common_gene_ids = gene_ids[j];
            first_flag = false;
          }
          else {
            std::vector<std::string> tmp_out;            
            std::set_intersection(tmp_common_gene_ids.begin(), tmp_common_gene_ids.end(),
                                  gene_ids[j].begin(), gene_ids[j].end(),
                                  std::back_inserter(tmp_out));
            tmp_common_gene_ids = tmp_out;
          }
        }
      }
      
      // Store the result
      common_gene_ids.insert(tmp_common_gene_ids.begin(), tmp_common_gene_ids.end());
      
    } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
    
    // Output gene IDs
    ofs << k + 1;
    for (const auto& gene_id : common_gene_ids)
      ofs << " " << gene_id;
    ofs << "\n";
  }
}


int main(int argc, char** argv)
{
  std::vector<std::string> transcript_filenames;
  for (int i=1; i<(argc - 1); i++)
    transcript_filenames.push_back(argv[i]);
  const std::string out_filename = argv[argc - 1];

  intersect_transcribed_genes(transcript_filenames, out_filename);
}
