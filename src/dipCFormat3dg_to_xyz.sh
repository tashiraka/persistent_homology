#!/bin/bash


dipCFile=$1
outFile=$2


cat $dipCFile | awk 'BEGIN{OFS=" "} $0!~/^#/{print($3, $4, $5)}' > $outFile
