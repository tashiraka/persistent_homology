#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <set>
#include <vector>
#include <sstream>
#include <algorithm>
#include <omp.h>


class Node {
public:
  long birth, death;
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  double birth_alpha, death_alpha;
  std::vector<long> pIDs;
  
  Node(long b, long d, std::vector<long> p) : birth(b), death(d), pIDs(p) {};
  ~Node() {};
};


class Ctree {
private:
  Node* root;

public:
  bool include_break_points = false;

private:
  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _load_pIDs(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);
      
      long birth;
      ss >> birth;
      
      long death;
      ss >> death;
        
      std::vector<long> pIDs;
      long p;
      while (ss >> p)
        pIDs.push_back(p);

      Node* n;
      if (is_left) {
        n = parent->left;
        n->pIDs = pIDs;
      }
      else {
        n = parent->right;
        n->pIDs = pIDs;
      }
      
      _load_pIDs(ifs, n, true);
      _load_pIDs(ifs, n, false);
    }
  }


    void _load_ctree(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);
      
      long birth;
      ss >> birth;
      
      long death;
      ss >> death;
        
      std::vector<long> pIDs;
      long p;
      while (ss >> p)
        pIDs.push_back(p);

      Node* n = new Node(birth, death, pIDs);
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;
      
      _load_ctree(ifs, n, true);
      _load_ctree(ifs, n, false);
    }
  }

  
  void _save_depth_first(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      ofs << n->birth_alpha << " " << n->death_alpha;
      for (const auto p : n->pIDs)
        ofs << " " << p;
      ofs << "\n";
      
      _save_depth_first(ofs, n->left);
      _save_depth_first(ofs, n->right);
    }
  }

  size_t _size(Node* n) {
    if (n) {
      return _size(n->left) + 1 + _size(n->right);
    }
    else return 0;
  }
  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree() {deleteNode(root);}

  size_t size() {return _size(root);}
  
  void load_ctree(std::ifstream& ifs) {
    _load_ctree(ifs, root, true);
    _load_ctree(ifs, root, false);
  }

  void load_pIDs(std::ifstream& ifs) {
    _load_pIDs(ifs, root, true);
    _load_pIDs(ifs, root, false);
  }

  void search_for_break_points(std::vector<long> break_points) {
    std::vector<long> v;
    std::set_intersection(root->pIDs.begin(), root->pIDs.end(),
                          break_points.begin(), break_points.end(),
                          std::back_inserter(v));
    if (v.empty()) include_break_points = false;
    else include_break_points = true;

    /*
    for (const auto p : break_points) {
      //if (std::find(root->pIDs.begin(), root->pIDs.end(), p) != root->pIDs.end()) {
      if (std::binary_search(root->pIDs.begin(), root->pIDs.end(), p)) {
        include_break_points = true;
        return;
      }
      else
        include_break_points = false;
    }
    */
  }

  void set_root_pIDs(std::vector<long> pIDs) {
    root->pIDs = pIDs;
  }
  
  void save(std::ofstream& ofs) const {
    _save_depth_first(ofs, root);
  }
};


class Cforest {
private:
  std::vector<Ctree*> ctrees;

  
public:
  Cforest() {};
  ~Cforest() {};

  void load_ph_tree(const std::string ph_tree_filename) {
    std::ifstream ifs(ph_tree_filename);
    std::string line;

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;
        
      std::vector<long> pIDs;
      long p;
      while (ss >> p)
        pIDs.push_back(p);

      // Remove duplicate and sort
      std::set<long> s;
      for (const auto p : pIDs) s.insert(p);
      pIDs.assign(s.begin(), s.end());

      // new root
      Node* root = new Node(birth, death, pIDs);
      Ctree* ctree = new Ctree(root);
      
      ctree->load_ctree(ifs);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  void load_pIDs(const std::string ph_tree_filename) {
    std::ifstream ifs(ph_tree_filename);
    std::string line;
    int i = 0;
    
    while (std::getline(ifs, line)) {
      std::stringstream ss(line);
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;
        
      std::vector<long> pIDs;
      long p;
      while (ss >> p)
        pIDs.push_back(p);

      // new root
      ctrees[i]->set_root_pIDs(pIDs);
      ctrees[i]->load_pIDs(ifs);
      i++;
    }

    ifs.close();
  }

  
  void search_for_break_points(std::vector<long> break_points) {
    #pragma omp parallel for
    for (size_t i=0; i<ctrees.size(); i++) {
      //  if (i % 1000 == 0)
      //  std::cout << i << " " << ctrees.size() << std::endl;
      ctrees[i]->search_for_break_points(break_points);
    }
    //    for (const auto& ctree : ctrees) 
    //  ctree->search_for_break_points(break_points);
  }
  
  void save_no_break_points(const std::string out_filename) const {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees)
      if (!ctree->include_break_points) ctree->save(ofs);
    ofs.close();
  }
};



void remove_break_point(std::string threeDG_file,
                        int bin_size,
                        std::string tree_file,
                        std::string tree_inside_file,
                        std::string out_filename,
                        std::string out_filename_inside)
{
  std::cout << "Load" << std::endl;
  // Loading chromosome sizes
  std::unordered_map<std::string, int> chrom_sizes;
  std::ifstream ifs(threeDG_file);
  std::string buf;

  while (ifs >> buf) {
    if (buf[0] == '#') {
      std::string chrom;
      int length;

      ifs >> chrom >> length;

      chrom_sizes.insert({chrom, length});
    }
    else {
      break;
    }
  }

  ifs.close();

  // Loading break points
  std::vector<long> break_points;
  std::set<long> s;
  long pointID = 0;
  std::string prev_chrom = "dummy";
  int prev_pos = -1;
  ifs.open(threeDG_file);

  while (ifs >> buf) {
    if (buf[0] != '#') {
      std::string chrom;
      int pos;
      double x, y, z, CpG;

      chrom = buf;
      ifs >> pos >> x >> y >> z >> CpG;
      
      if (chrom != prev_chrom) {
        // break point at the start and the end of chromosomes
        if (prev_chrom != "dummy" && prev_pos != chrom_sizes[prev_chrom])
          s.insert(pointID - 1);

        if (pos != 0) s.insert(pointID);
      }
      else if (pos - prev_pos != bin_size) {
        s.insert(pointID - 1);      
        s.insert(pointID);
      }

      pointID++;
      prev_chrom = chrom;
      prev_pos = pos;
    }
    else {
      ifs >> buf >> buf;
    }
  }

  break_points.assign(s.begin(), s.end());

  std::cout << "The number of brak points: "
            << break_points.size() << std::endl;
  
  Cforest cforest;
  // Load cycle and inside trees
  cforest.load_ph_tree(tree_inside_file);
    
  // Remove cavity trees with break points
  std::cout << "Remove break point" << std::endl;
  cforest.search_for_break_points(break_points);

  // Load cycle and inside trees
  std::cout << "Load boundaries and inside" << std::endl;  
  cforest.load_pIDs(tree_inside_file);

  // Save
  std::cout << "save" << std::endl;
  cforest.save_no_break_points(out_filename_inside);

  // Boundaries
  std::cout << "Load boundaries" << std::endl;  
  cforest.load_pIDs(tree_file);
  
  // Save
  std::cout << "save" << std::endl;
  cforest.save_no_break_points(out_filename);
}


int main(int argc, char** argv) {
  std::string threeDG_file = argv[1];
  int bin_size = 20000; // 40 kb
  std::string tree_file = argv[2];
  std::string tree_inside_file = argv[3];
  std::string out_filename = argv[4];
  std::string out_filename_inside = argv[5];

  remove_break_point(threeDG_file, bin_size, tree_file, tree_inside_file, out_filename, out_filename_inside);
}
