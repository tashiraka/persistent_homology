#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <unordered_set>
#include <vector>


void intersect_loci(const int bin_size,
                    const int num_mergin_bins,
                    const std::vector<std::string>& threedg_filenames,
                    const std::vector<std::string>& cforest_filenames,
                    const std::string out_filename)
{
  if (threedg_filenames.size() != cforest_filenames.size()) {
    std::cerr << "Error: sizes are different" << std::endl;
    std::exit(1);
  }

  size_t num_replicates = threedg_filenames.size();
  
  // Get the intersection of loci among replicates
  std::vector<std::vector<std::string>> loci;

  for (size_t rep=0; rep<cforest_filenames.size(); rep++) {
    auto threedg_filename = threedg_filenames[rep];
    auto cforest_filename = cforest_filenames[rep];
    
    // Load point IDs
    std::ifstream ifs_cforest(cforest_filename);
    std::unordered_set<int> point_ids;
    std::string line;
    
    while(std::getline(ifs_cforest, line)) {
      if (line[0] != '!' and line[0] != '$') {        
        std::stringstream ss(line);
        std::string not_used;

        double birth;
        ss >> birth;
      
        double death;
        ss >> death;
        
        double net_persistence;
        ss >> net_persistence;
        
        double volume;
        ss >> volume;

        int point_id;
        while (ss >> point_id) 
          point_ids.insert(point_id);
      }
    }

    // Load the loci of points
    std::ifstream ifs_threedg(threedg_filename);
    std::vector<std::pair<std::string, int>> map_point_id_to_loci;
    
    while (std::getline(ifs_threedg, line)) {
      if (line[0] != '#') {
        std::stringstream ss(line);
        
        std::string chrom;
        int loc;
        ss >> chrom >> loc;

        // Remove the annotation of diploids, e.g. 1a to 1
        std::string chrom_hap = chrom.substr(0, chrom.size() - 1);

        map_point_id_to_loci.push_back(std::make_pair(chrom_hap, loc));
      }
    }

    std::vector<std::string> tmp;
    for (const auto pid : point_ids) {
      auto chrom_hap = map_point_id_to_loci[pid].first;
      auto loc = map_point_id_to_loci[pid].second;

      tmp.push_back(chrom_hap + "_" + std::to_string(loc));

      // Push bins around the center
      for (int a=bin_size; a<(num_mergin_bins+1)*bin_size; a+=bin_size) {
        tmp.push_back(chrom_hap + "_" + std::to_string(loc - a));
        tmp.push_back(chrom_hap + "_" + std::to_string(loc + a));
      }      
    }
    std::sort(tmp.begin(), tmp.end());
    loci.push_back(tmp);
  }


  std::ofstream ofs(out_filename);

  for (size_t k=1; k<=num_replicates; k++) {
    // intersection of k replicates
    std::unordered_set<std::string> common_loci;
    bool first_flag = true;

    std::string bitmask(k, 1);
    bitmask.resize(num_replicates, 0);

    do {
      // Get a k-intersection
      std::vector<std::string> tmp_common_loci;
      
      for (size_t j = 0; j < num_replicates; j++) {
        if (bitmask[j]) {

          if (first_flag) {
            tmp_common_loci = loci[j];
            first_flag = false;
          }
          else {      
            std::vector<std::string> tmp_out;
            std::set_intersection(tmp_common_loci.begin(), tmp_common_loci.end(),
                                  loci[j].begin(), loci[j].end(),
                                  std::back_inserter(tmp_out));
            tmp_common_loci = tmp_out;
          }
        }
      }

      common_loci.insert(tmp_common_loci.begin(), tmp_common_loci.end());

    } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
    
    // Output gene IDs
    ofs << k;
    for (const auto& loc : common_loci)
      ofs << " " << loc;
    ofs << "\n";
  }
}



int main(int argc, char** argv)
{
  int num_replicates = (argc - 1) / 2;
  int bin_size = 20000;
  int num_mergin_bins = 4;
  
  // Three dg
  std::vector<std::string> threedg_filenames;
  for (int i=1; i<(num_replicates + 1); i++)
    threedg_filenames.push_back(argv[i]);

  // Point IDs around caves
  std::vector<std::string> cforest_filenames;
  for (int i=(num_replicates  + 1); i<(argc - 1); i++)
    cforest_filenames.push_back(argv[i]);

  const std::string out_filename = argv[argc - 1];

  intersect_loci(bin_size, num_mergin_bins, threedg_filenames, cforest_filenames, out_filename);
}
