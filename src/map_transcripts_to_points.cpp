#include <iostream>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <regex>

class Transcript {
public:
  std::string gene_id;
  double TPM;
  std::string chrom;
  int start, end;
  std::vector<long> point_ids;

  Transcript() {};
  Transcript(const std::string gid, const double tpm) : gene_id(gid), TPM(tpm) {}
};


class Point {
public:
  long point_id = -1;
  std::string chrom;
  int start, end;

  Point() {}
  Point(const std::string c, const int s, const int e)
    : chrom(c), start(s), end(e) {}
};


std::unordered_map<std::string, int> load_chrom_sizes(const std::string threedg_filename) {
  // Loading chromosome sizes
  std::unordered_map<std::string, int> chrom_sizes;
  std::ifstream ifs(threedg_filename);
  std::string buf;

  while (ifs >> buf) {
    if (buf[0] == '#') {
      std::string chrom;
      int length;

      ifs >> chrom >> length;

      chrom_sizes.insert({chrom, length});
    }
    else {
      break;
    }
  }

  ifs.close();

  return chrom_sizes;
}


std::unordered_map<std::string, std::string[2]> map_chrom_names() {
  // e.g. chr1 -> 1a, 1b
  std::unordered_map<std::string, std::string[2]> chrom_map;

  for (int i=0; i<23; i++) {
    std::stringstream ss1;
    ss1 << "chr" << i;
    std::stringstream ss2;
    ss2 << i << "a"; 
    std::stringstream ss3;
    ss3 << i << "b"; 

    chrom_map.insert({ss1.str(), {ss2.str(), ss3.str()}});
  }

  chrom_map.insert({"chrX", {"Xa", "Xb"}});
  
  return chrom_map;
}


  
std::unordered_map<std::string, std::vector<Point>>
load_and_complete_points(const std::string filename,
                         const std::unordered_map<std::string, int> chrom_sizes,
                         const int bin_size) {
  std::unordered_map<std::string, std::vector<Point>> points;

  // Construct points of chromosome size
  for (const auto& elem : chrom_sizes) {
    auto chrom = elem.first;
    auto chrom_size = elem.second;

    auto num_bins = (chrom_size - 1) / bin_size + 1;

    std::vector<Point> p(num_bins);
    for (int i=0; i<num_bins; i++) p[i] = Point(chrom,
                                                i * bin_size,
                                                std::min(chrom_size, (i + 1) * bin_size));

    points.insert({chrom, p});
  }

  // Load points
  std::ifstream ifs(filename);
  std::string line;
  long pid = 0;
  
  while (std::getline(ifs, line)) {
    if (line[0] != '#') {
      std::stringstream ss(line);
      std::string chrom;
      int start;
      double CpG, not_used;
      
      ss >> chrom >> start >> not_used >> not_used >> not_used >> CpG;

      int bin = start / bin_size;
      points[chrom][bin].point_id = pid;      
      pid++;
    }
  }

  /*
  size_t count(0);
  size_t total(0);
  for (const auto& elem : points) {
    total += elem.second.size();

    for (const auto& p : elem.second) {
      if (p.point_id != -1) count++;
    }
  }
  double a = count;
  std::cout << count << " " << total << " " << a / total * 100 << std::endl;
  */
  
  return points;
}


// GTF, [start, end], 1-based
void load_gene_loc(const std::string gene_filename,
                   std::unordered_map<std::string, Transcript>& transcripts) {
  std::ifstream ifs(gene_filename);
  std::string line;

  // Load each line
  while (std::getline(ifs, line)) {
    if (line[0] != '#') {
      std::stringstream ss(line);
      std::string chrom, annotation_source, type;
      ss >> chrom >> annotation_source >> type;
      
      /*
      int start, end;
      std::string chrom, type, additional_info;
      std::string field;
      int i = 0;
    
      while (std::getline(ss, field , '\t')) {
        if (i == 0) chrom = field;
        else if (i == 2) type = field;
        else if (i == 3) start = std::stoi(field);
        else if (i == 4) end = std::stoi(field);
        else if (i == 8) {
          additional_info = field;
          break;
        }
        i++;
      }

      if (type == "gene") {
      }
      */
      // Select genes
      if (type == "gene") {
        int start, end;
        std::string score, strand, phase, additional_info;
        ss >> start >> end >> score >> strand >> phase;

        // Select genes with gene_id
        std::string tag;
        while (ss >> tag) {
          std::string val;
          ss >> val;

          if (tag == "gene_id") {

            std::string gene_id = val.substr(1, val.size() - 3); // Strip '/"' and ';'.

            // Ignore gene version number and mapping version number
            // e.g. ENSG00000228327.3_2 -> ENSG00000228327
            std::stringstream ss2(gene_id);
            std::string bared_gene_id;
            std::getline(ss2, bared_gene_id, '.');
            
            if (transcripts.find(bared_gene_id) != transcripts.end()) {
              transcripts.at(bared_gene_id).chrom = chrom;
              transcripts.at(bared_gene_id).start = start;
              transcripts.at(bared_gene_id).end = end;
              break;              
            }
          }
        }
      }
    }
  }
}


std::unordered_map<std::string, Transcript> load_transcript(const std::string transcript_filename) {
  std::unordered_map<std::string, Transcript> transcripts;
  std::ifstream ifs(transcript_filename);
  std::string line;

  // Load each line
  std::getline(ifs, line); // header
    
  while (std::getline(ifs, line)) {
    std::stringstream ss(line);
    std::string gene_id, not_used;
    double TPM;
    ss >> gene_id >> not_used >> not_used >> not_used >> not_used >> TPM;

    // Ignore gene version number and mapping version number
    // e.g. ENSG00000228327.3_2 -> ENSG00000228327
    std::stringstream ss2(gene_id);
    std::string bared_gene_id;
    std::getline(ss2, bared_gene_id, '.');
    
    transcripts.insert({bared_gene_id, Transcript(gene_id, TPM)});
  }

  return transcripts;
}


// When a gene overlaps multiple bins, TPM are mapped to all their bins.
// If there are no points for a gene, it are discarded.
// Allel-specific expression are ignored.
void map_pid_to_gid(const std::unordered_map<std::string, std::vector<Point>>& complete_points,
                    std::unordered_map<std::string, Transcript>& transcripts,
                    const std::unordered_map<std::string, std::string[2]>& chrom_map,
                    const int bin_size) {
  for (auto& elem : transcripts) {
    auto& transcript = elem.second;

    if (chrom_map.find(transcript.chrom) != chrom_map.end()) {
      const auto& chroms = chrom_map.at(transcript.chrom);

      // Map TPM to two allels
      for (const auto& chrom : chroms) {
        // Map TPM to all overlapped bins
        auto start_bin = (transcript.start - 1) / bin_size;
        auto end_bin = (transcript.end - 1) / bin_size + 1; // to exclusive end

        for (int i=start_bin; i<end_bin; i++) {
          auto pid = complete_points.at(chrom)[i].point_id;
          if (pid != -1) transcript.point_ids.push_back(pid);
        }
      }
    }
  }
}


void save_transcripts(const std::string filename,
                      const std::unordered_map<std::string, Transcript>& transcripts) {
  std::ofstream ofs(filename);

  ofs << "gene_id TPM point_ids\n"; 
  
  for (const auto& elem : transcripts) {
    const auto& t = elem.second;
    if (!t.point_ids.empty()) {
      ofs << t.gene_id << " " << t.TPM << " " << t.point_ids[0];

      for (size_t i=1; i<t.point_ids.size(); i++)
        ofs << "," << t.point_ids[i];

      ofs << "\n";
    }
  }

  ofs.close();
}


void map_transcripts_to_points(const std::string transcript_filename,
                               const std::string gene_filename,
                               const int bin_size,
                               const std::string threedg_filename,
                               const std::string out_filename) {
  std::cout << "Load chromosome sizes" << std::endl;
  auto chrom_sizes = load_chrom_sizes(threedg_filename);

  std::cout << "Map chromosome names" << std::endl;
  auto chrom_map = map_chrom_names();

  std::cout << "Load transcripts" << std::endl;
  auto transcripts = load_transcript(transcript_filename);

  std::cout << "Load gene annotaions" << std::endl;
  load_gene_loc(gene_filename, transcripts);

  std::cout << "Load points" << std::endl;
  auto complete_points = load_and_complete_points(threedg_filename, chrom_sizes, bin_size);

  std::cout << "Map point ID to gene" << std::endl;
  map_pid_to_gid(complete_points, transcripts, chrom_map, bin_size);

  std::cout << "Save" << std::endl;
  save_transcripts(out_filename, transcripts);
}



int main(int argc, char** argv) {
  std::string transcript_filename = argv[1];
  std::string gene_filename = argv[2];
  int bin_size = 20000; //std::stoi(argv[3]);
  std::string threedg_filename = argv[3];
  std::string out_filename = argv[4];

  map_transcripts_to_points(transcript_filename, gene_filename, bin_size, threedg_filename, out_filename);
}
