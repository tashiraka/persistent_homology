#include <iostream>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <set>
#include <unordered_set>
#include <random>
#include <limits>
#include <omp.h>


class Transcript {
public:
  std::string gene_id;
  double TPM;
  std::vector<long> point_ids;
  
  Transcript() {};
  Transcript(const Transcript& t)
    : gene_id(t.gene_id), TPM(t.TPM),  point_ids(t.point_ids) {};
  Transcript(const std::string gid, const double tpm, const std::vector<long> pids)
    : gene_id(gid), TPM(tpm), point_ids(pids) {}
  Transcript(const std::string gid, const double tpm)
    : gene_id(gid), TPM(tpm) {}
};


std::vector<Transcript*> load_transcript(const std::string transcript_filename) {
  std::vector<Transcript*> transcripts;
  std::ifstream ifs(transcript_filename);
  std::string line;

  // Load each line
  std::getline(ifs, line); // header

  while (std::getline(ifs, line)) {
    std::stringstream ss(line);
    std::string gene_id, pids;
    double TPM;
    ss >> gene_id >> TPM >> pids;

    std::vector<long> point_ids;
    std::stringstream ss2(pids);
    std::string pid;
    while (std::getline(ss2, pid, ','))
      point_ids.push_back(std::stol(pid));

    transcripts.push_back(new Transcript(gene_id, TPM, point_ids));
  }
  
  return transcripts;
}



size_t get_num_points_of_caves(const std::string filename)
{
  std::ifstream ifs(filename);
  std::string line, not_used;
  std::unordered_set<int> point_ids;
  int pid;
  
  while (std::getline(ifs, line)) {
    std::stringstream ss(line);

    if (line[0] != '!' && line[0] != '$') {
      // birth, death, net persistence, volume
      ss >> not_used >> not_used >> not_used >> not_used;
      while (ss >> pid) point_ids.insert(pid);
    }
  }

  ifs.close();
  return point_ids.size();
}


void assign_pids_to_transcripts(const std::vector<size_t>& num_points,
                                std::vector<std::unordered_map<int, std::vector<Transcript*>*>>&
                                map_pids_to_transcripts,
                                const std::vector<std::vector<Transcript*>>& transcripts)
{
  for (size_t k=0; k<num_points.size(); k++) {
    std::unordered_map<int, std::vector<Transcript*>*> tmp;
    
    for (size_t i=0; i<num_points[k]; i++)
      tmp[i] = new std::vector<Transcript*>;

    for (const auto t : transcripts[k]) {
      for (const auto pid : t->point_ids) {
        tmp[pid]->push_back(t);
      }
    }

    map_pids_to_transcripts.push_back(tmp);
  }
}


void assign_gids_to_transcripts(std::vector<std::unordered_map<std::string, Transcript*>>&
                                map_gids_to_transcripts,
                                const std::vector<std::vector<Transcript*>>& transcripts)
{
  for (const auto& trns : transcripts) {
    std::unordered_map<std::string, Transcript*> tmp;

    for (const auto t : trns) {
      tmp[t->gene_id] = t;
    }
    
    map_gids_to_transcripts.push_back(tmp);
  }
}


size_t get_num_points(const std::string xyz_filename)
{
  size_t count = 0;
  std::ifstream ifs(xyz_filename);
  std::string line;
  while (std::getline(ifs, line)) count++;
  return count;
}


std::vector<size_t> get_num_common_genes(const std::string filename,
                                         std::vector<std::vector<size_t>>& max_gene_length,
                                         std::vector<std::unordered_map<std::string, Transcript*>>&
                                         map_gids_to_transcripts)
{
  max_gene_length.clear();
  std::vector<size_t> counts;
  std::ifstream ifs(filename);
  std::string line;
  
  while (std::getline(ifs, line)) { // loop of the number of intersection
    std::stringstream ss(line);
    std::string col;
    size_t count = 0;
    std::vector<size_t> max_glen(map_gids_to_transcripts.size(), 0);
    std::getline(ss, col, ' '); // the number of intersection
    
    while (std::getline(ss, col, ' ')) {
      count++;

      for (size_t rep=0; rep<map_gids_to_transcripts.size(); rep++) {
        if (map_gids_to_transcripts[rep].find(col) != map_gids_to_transcripts[rep].end()) {
          auto glen = map_gids_to_transcripts[rep].at(col)->point_ids.size();
          max_glen[rep] = max_glen[rep] < glen ? glen : max_glen[rep];
        }
      }
    }
    
    max_gene_length.push_back(max_glen);
    counts.push_back(count);
  }

  return counts;
}


void permutation_test(const size_t num_samples,
                      const std::string common_genes_file,
                      const std::vector<std::string>& cforest_filenames,
                      const std::vector<std::string>& xyz_filenames,
                      const std::vector<std::string>& transcript_on_point_filenames,
                      const std::string out_filename)
{
  size_t num_replicates = cforest_filenames.size();
  std::cout << "Load cavity trees" << std::endl;
  std::vector<size_t> num_points_of_caves;
  for (const auto& filename : cforest_filenames)
    num_points_of_caves.push_back(get_num_points_of_caves(filename));

  std::cout << "Load transcripts on points" << std::endl;
  std::vector<std::vector<Transcript*>> transcripts;
  for (const auto& filename : transcript_on_point_filenames)
    transcripts.push_back(load_transcript(filename));
  
  std::cout << "Get the number of points" << std::endl;
  std::vector<size_t> num_points;
  for (const auto& filename : xyz_filenames)
    num_points.push_back(get_num_points(filename));

  std::cout << "Map point ID to transcripts" << std::endl;
  std::vector<std::unordered_map<int, std::vector<Transcript*>*>> map_pids_to_transcripts;
  assign_pids_to_transcripts(num_points, map_pids_to_transcripts, transcripts);

  std::cout << "Map gene IDs to transcripts" << std::endl;
  std::vector<std::unordered_map<std::string, Transcript*>> map_gids_to_transcripts;
  assign_gids_to_transcripts(map_gids_to_transcripts, transcripts);

  std::cout << "Get the number of the common genes" << std::endl;
  std::vector<std::vector<size_t>> max_gene_length;
  std::vector<size_t> obs_num_common_genes = get_num_common_genes(common_genes_file, max_gene_length,
                                                                  map_gids_to_transcripts);

  
  std::cout << "Perform permutation test" << std::endl;
  #pragma omp declare reduction(vec_int_plus : std::vector<int> : \
                              std::transform(omp_out.begin(), omp_out.end(), \
                                             omp_in.begin(), omp_out.begin(), std::plus<int>())) \
                                             initializer(omp_priv = omp_orig)

  std::vector<int> pval_counts(num_replicates, 0);

  
  #pragma omp parallel for num_threads(24) reduction(vec_int_plus : pval_counts)
  for (size_t i=0; i<num_samples; i++) {
    if (i % 1000 == 0) std::cout << i << std::endl;
    /*
     * Sampling
     */
    // randamized_gene_ids[intersection num][replicate num]
    std::vector<std::vector<std::vector<std::string>>> randamized_gene_ids;

    // Sample randamized caves and gene IDs on them
    std::random_device seed_gen;
    //std::mt19937 mt(seed_gen());
    std::default_random_engine mt(seed_gen());

    for (size_t num_intersection=1; num_intersection<=num_replicates; num_intersection++) {
      std::vector<std::vector<std::string>> tmp_randamized_gene_ids;
      
      for (size_t rep=0; rep<num_replicates; rep++) {
        const auto n = num_points_of_caves[rep];
      
        // Randamize the positions of the caves
        std::unordered_set<int> sampled_pids;
        size_t counter = 0;
        std::uniform_int_distribution<> rand_uni(0, num_points[rep] - 1);

        while (sampled_pids.size() < n) {
          // Sometimes sagmentation fault occur this line. I guess this is a bug of gcc.
          // This may be solved by replacing mt19937 with default_random_engine
          int sample = rand_uni(mt);

          sampled_pids.insert(sample);
          if (counter++ > num_points[rep]) {
            std::cerr << "Error: this random sample method is inefficient" << std::endl;
            std::exit(1);
          }
        }

        // Get gene IDs on the randamized caves
        std::unordered_set<std::string> gene_ids;

        for (const auto pid : sampled_pids) {
          for (const auto t : *(map_pids_to_transcripts[rep]).at(pid)) {
            auto glen = t->point_ids.size();
            if (t->TPM > 0 && glen <= max_gene_length[num_intersection - 1][rep]) {
              // Extracting transcribed genes.
              gene_ids.insert(t->gene_id);
            }
          }
        }
        
        // Store randamized genes
        std::vector<std::string> tmp;
        tmp.assign(gene_ids.begin(), gene_ids.end());
        std::sort(tmp.begin(), tmp.end()); // Sorted here!
        tmp_randamized_gene_ids.push_back(tmp);
      }

      randamized_gene_ids.push_back(tmp_randamized_gene_ids);
    }

    /*
     * Intersection
     */
    // Get variable-sized intersection
    for (size_t num_intersection=1; num_intersection<=num_replicates; num_intersection++) {
      // intersection of k replicates
      std::unordered_set<std::string> common_gene_ids;

      std::string bitmask(num_intersection, 1);
      bitmask.resize(num_replicates, 0);

      bool first_flag = true;
      
      do {
        // Get a k-intersection
        std::vector<std::string> tmp_common_gene_ids;

        for (size_t rep = 0; rep < num_replicates; rep++) {
          if (bitmask[rep]) {
            if (first_flag) {
              // randamized_gene_ids must be sorted
              tmp_common_gene_ids = randamized_gene_ids[num_intersection - 1][rep];
              first_flag = false;
            }
            else {
              std::vector<std::string> tmp_out;
              std::set_intersection(tmp_common_gene_ids.begin(), tmp_common_gene_ids.end(),
                                    randamized_gene_ids[num_intersection - 1][rep].begin(),
                                    randamized_gene_ids[num_intersection - 1][rep].end(),
                                    std::back_inserter(tmp_out));
              tmp_common_gene_ids = tmp_out;
            }
          }
        }

        // Store the result
        common_gene_ids.insert(tmp_common_gene_ids.begin(), tmp_common_gene_ids.end());
        
      } while (std::prev_permutation(bitmask.begin(), bitmask.end()));


      // Count p value
      //      std::cout << k + 1 << " " << common_gene_ids.size() << " " <<  obs_num_common_genes[k]  << std::endl;
      if (common_gene_ids.size() > obs_num_common_genes[num_intersection - 1]) 
        pval_counts[num_intersection - 1]++;
    }
  }


  std::cout << "Save p values" << std::endl;
  std::ofstream ofs(out_filename);

  for (size_t i=0; i<num_replicates; i++) {
    double pval = pval_counts[i]; // int to double
    pval /= num_samples;
    ofs << (i + 1) << " " << pval << "\n";
    std::cout << (i + 1) << " " << pval << "\n";
  }
  ofs.close();
}


int main(int argc, char** argv)
{
  size_t num_replicates = (argc - 4) / 3;
  
  size_t num_samples = std::atoi(argv[1]);
  std::string common_gene_file = argv[2];
  
  std::vector<std::string> cforest_filenames;
  for (size_t i=3; i<(num_replicates + 3); i++)
    cforest_filenames.push_back(argv[i]);

  std::vector<std::string> xyz_filenames;
  for (size_t i=(num_replicates + 3); i<(num_replicates * 2 + 3); i++)
    xyz_filenames.push_back(argv[i]);

  std::vector<std::string> transcript_on_point_filenames;
  for (size_t i=(num_replicates * 2 + 3); i<(num_replicates * 3 + 3); i++)
    transcript_on_point_filenames.push_back(argv[i]);

  std::string out_filename = argv[argc - 1];
  
  permutation_test(num_samples, common_gene_file, cforest_filenames,
                   xyz_filenames, transcript_on_point_filenames, out_filename);
}
