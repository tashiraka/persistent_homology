import sys
import matplotlib.pyplot as plt
import numpy as np


pval_filename_prefix = sys.argv[1]
rounds = int(sys.argv[2])
out_filename = sys.argv[3]


x = []
y = []


for round in range(1, rounds + 1):
    filename = pval_filename_prefix + "_" + str(round)
    fin = open(filename)

line = fin.readline()
while line:
    if line[0] != '$':
        fields = line.strip('\n').strip().split()

        birth = float(fields[0])
        death = float(fields[1])
        volume = float(fields[2])
        persistence = death - birth
        
        if persistence > 0:
            x += [persistence]
            y += [volume]
            
    line = fin.readline()


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.scatter(x, y, marker='.')
ax.set_xlabel("Persistence")
ax.set_ylabel("Volume")
plt.savefig(out_filename)
#plt.show()
#plt.close()
