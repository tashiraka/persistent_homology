import sys
import matplotlib.pyplot as plt
import numpy as np

tpm_filename = sys.argv[1]
volume_filename = sys.argv[2]
out_filename = sys.argv[3]


x = []
y = []

fin_tpm = open(tpm_filename)
fin_vol = open(volume_filename)

line_tpm = fin_tpm.readline()
line_vol = fin_vol.readline()

while line_tpm:
    if line_tpm[0] != '$':
        fields_tpm = line_tpm.strip('\n').strip().split()
        fields_vol = line_vol.strip('\n').strip().split()

        birth = float(fields_tpm[0])
        death = float(fields_tpm[1])        
        persistence = death - birth

        volume = float(fields_vol[2])
        
        if persistence > 0:
            x += [volume]
            size = len(set(fields_tpm[2].split(',')))
            #x += [size]
            
            if len(fields_tpm) == 3:
                y += [0]
            else:
                y += [sum([float(elem.split(',')[1]) for elem in fields_tpm[3:]])]
        
    line_tpm = fin_tpm.readline()
    line_vol = fin_vol.readline()


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.scatter(np.array(x) + 1, np.array(y) + 1, marker='.')
#ax.scatter(x, np.array(y) + 1, marker='.')
ax.scatter(x, y, marker='.')
ax.set_xlabel("Volume")
ax.set_ylabel("TPM")
#ax.set_xlim(0, 0.4)
#ax.set_ylim(0, 100000)
#ax.set_yscale("log")
#ax.set_xscale("log")
plt.savefig(out_filename)
#plt.show()
#plt.close()
