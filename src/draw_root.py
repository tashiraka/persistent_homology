import os
threedg_filename = os.environ['THREEDG']
cavity_filename = os.environ['CAVITY']
cavity_number = int(os.environ['CAVITY_NUM'])
out_filename = os.environ['OUT_FILE']


def setup():
    size(1000, 1000, P3D)
    background(0)
    stroke(255)
    translate(500, 500, 500);

def draw():
    # Load a 3D model of genome
    fin = open(threedg_filename)

    line = fin.readline()
    while line:
        if line[0] != '#':
            fields = line.strip().split()
            x = float(fields[2])
            y = float(fields[3])
            z = float(fields[4])

            translate(-x, -y, -z);
            sphere(100);
            translate(x, y, z);
            
        line = fin.readline()
    
    fin.close()


    # Save
    save(out_filename)
