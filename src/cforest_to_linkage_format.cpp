
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <array>
#include <algorithm>


class Node {
  void _delete_all_children(Node* n)
  {
    if (n) {
      _delete_all_children(n->left);
      _delete_all_children(n->right);

      delete n;
    }
  }

public:
  double birth, death, net_persistence, volume;
  std::vector<long> pIDs;
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  bool is_valid_net_persistence = true;
  
  Node(double b, double d, double np, double v, std::vector<long> c)
    : birth(b), death(d), net_persistence(np), volume(v), pIDs(c) {};
  ~Node() {};

  void delete_all_children()
  {
    _delete_all_children(left);
    _delete_all_children(right);
    
    left = nullptr;
    right = nullptr;
  }
};


class Ctree {
private:
  Node* root;
  
  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _load(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);

      std::string not_used;
      bool is_valid_net_persistence = true;
      if (line[0] == '!') {
        ss >> not_used;
        is_valid_net_persistence = false;
      }

      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      Node* n = new Node(birth, death, net_persistence, volume, pIDs);
      n->is_valid_net_persistence = is_valid_net_persistence;
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;
      
      _load(ifs, n, true);
      _load(ifs, n, false);
    }
  }

  
  void _save_depth_first(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      if (n->is_valid_net_persistence)
        ofs << "! ";
        
      ofs << n->birth << " " << n->death << " " << n->net_persistence << " " << n->volume;
      for (const auto p : n->pIDs)
        ofs << " " << p;
      ofs << "\n";
      
      _save_depth_first(ofs, n->left);
      _save_depth_first(ofs, n->right);
    }
  }

  void _save_depth_first_as_linkage_format(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      if (n->is_valid_net_persistence)
        ofs << "! ";
        
      ofs << n->birth << " " << n->death << " " << n->net_persistence << " " << n->volume;
      for (const auto p : n->pIDs)
        ofs << " " << p;
      ofs << "\n";
      
      _save_depth_first(ofs, n->left);
      _save_depth_first(ofs, n->right);
    }
  }

  size_t _size(Node* n) {
    if (n) {
      return _size(n->left) + 1 + _size(n->right);
    }
    else return 0;
  }
  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree()
  {
    if (root) deleteNode(root);
  }

  size_t size() {return _size(root);}
  double root_persistence() {return root->death - root->birth;}
  
  void load(std::ifstream& ifs) {
    _load(ifs, root, true);
    _load(ifs, root, false);
  }

  void save(std::ofstream& ofs) const {
    _save_depth_first(ofs, root);
  }

  void save_as_linkage_format(std::ofstream& ofs) const {
    _save_depth_first_as_linkage_format(ofs, root);
  }
};


class Cforest {
private:
  std::vector<Ctree*> ctrees;

public:
  Cforest() {};
  ~Cforest() {};

  size_t size() {return ctrees.size();}
  
  void load(const std::string filename) {
    std::ifstream ifs(filename);
    std::string line;

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);

      std::string not_used;
      bool is_valid_net_persistence = true;
      if (line[0] == '!') {
        ss >> not_used;
        is_valid_net_persistence = false;
      }
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      // new root
      Node* root = new Node(birth, death, net_persistence, volume, pIDs);
      root->is_valid_net_persistence = is_valid_net_persistence;
      Ctree* ctree = new Ctree(root);
      
      ctree->load(ifs);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  void save(const std::string out_filename) const
  {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees)
        ctree->save(ofs);
    ofs.close();
  }

  void save_as_linkage_format(const std::string out_filename) const
  {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees)
        ctree->save_as_linkage_format(ofs);
    ofs.close();
  }
};


void convert_cforest_to_linkage_format(const std::string cforest_filename,
                                       const std::string out_filename)
{  
  std::cout << "Load cavity forest" << std::endl;
  Cforest cforest;
  cforest.load(cforest_filename);
  
  std::cout << "Save" << std::endl;
  cforest.save_as_linkage_format(out_filename);
}


int main(int argc, char** argv)
{
  std::string cforest_filename = argv[1];
  std::string out_filename = argv[2];

  convert_cforest_to_linkage_format(cforest_filename, out_filename);
}
