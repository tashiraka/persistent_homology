#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from scipy.cluster import hierarchy
import sys

linkage_filename = sys.argv[1]
out_filename = sys.argv[2]

with open(linkage_filename) as fin:
    for line in fin:
        fields = line.strip().split()

        if fileds[0] == '!':
            birth = float(fields[0])
            death = float(fields[1])
            net_persistence = float(fields[2])
            volume = float(fields[3])
            
            b.append(birth)
            d.append(death)
            p.append(death - birth)
            n.append(net_persistence)
            v.append(volume)

print "create a histgram"
data = [b, d, p, n, v]
labels = ["Birth time",
          "Death time",
          "Persistence",
          "Net persistence",
          "Volume"]

fig = plt.figure()
dn = hierarchy.dendrogram(Z)
for i in range(0, 5):
    ax = fig.add_subplot(5, 1, i + 1)
    y, ind, pacthes = ax.hist(data[i], bins=100, range=(0, max(data[i])))
    ax.set_xlabel(labels[i])
    ax.set_ylabel("Log frequency")
    ax.set_yscale("log")
    ax.set_ylim([0, y.max()])

plt.savefig(out_filename, dpi=300, format='png')
