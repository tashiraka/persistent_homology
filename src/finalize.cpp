#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <cmath>

typedef std::pair<double, std::vector<long>> map_type;
typedef std::pair<long, std::vector<long>> phat_type;


class Point
{
public:
  double x, y, z;
  
  Point(const double a,
        const double b,
        const double c)
    : x(a), y(b), z(c) {}
};


Point operator+(const Point& p, const Point& q) {
  return Point(p.x + q.x,
               p.y + q.y,
               p.z + q.z);
}

Point operator-(const Point& p, const Point& q) {
  return Point(p.x - q.x,
               p.y - q.y,
               p.z - q.z);
}

double dotProduct(const Point& u, const Point& v) {
  return u.x * v.x + u.y * v.y + u.z * v.z;
}


Point outerProduct(const Point& u, const Point& v)
{
  return Point(u.y * v.z - u.z * v.y,
               u.z * v.x - u.x * v.z,
               u.x * v.y - u.y * v.x);
}


double tripleProduct(const Point&  u, const Point& v, const Point& w)
{
  auto outer = outerProduct(u, v);
  return dotProduct(outer, w);
}


double tetrahedron_volume(const Point& p1,
                          const Point& p2,
                          const Point& p3,
                          const Point& p4) {
  return std::abs(tripleProduct(p2 - p1, p3 - p1, p4 - p1)) / 6.0;
}


class Node {
public:
  long birth, death;
  std::vector<long> cells; // sorted
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  double birth_alpha, death_alpha, net_persistence, volume;
  std::vector<long> pIDs;
  std::vector<long> boundaries;
  
  Node(long b, long d, std::vector<long> c) : birth(b), death(d), cells(c) {};
  ~Node() {};

  void compute_volume(const std::vector<Point>& points) {
    if (pIDs.size() % 4 != 0) {
      std::cerr << "Error: point IDs do not represent tetrahedrons." << std::endl;
      std::exit(1);
    }
    else {
      volume = 0;

      int num_cells = pIDs.size() / 4;
      for (int i=0; i<num_cells; i++) {
        volume += tetrahedron_volume(points[pIDs[4 * i]],
                                     points[pIDs[4 * i + 1]],
                                     points[pIDs[4 * i + 2]],
                                     points[pIDs[4 * i + 3]]);
      }
    }
  }
};


class Ctree {
private:
  Node* root;
  
  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _load_ctree(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);
      
      long birth;
      ss >> birth;
      
      long death;
      ss >> death;
        
      std::vector<long> cells;
      long cell;
      while (ss >> cell)
        cells.push_back(cell);

      Node* n = new Node(birth, death, cells);
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;
      
      _load_ctree(ifs, n, true);
      _load_ctree(ifs, n, false);
    }
  }

  void _replace_indices(Node* n,
                        const std::vector<phat_type>& phat_filtration,
                        const std::vector<map_type>& phat_point_map) {
    if (!n) return;

    // replace the indices of PHAT filtration with point IDs
    //
    // IDs of the cells in PHAT filtration
    // -> IDs of the boundaries in PHAT filtration
    // -> point IDs
    std::vector<long> boundaries;
    for (const auto cell : n->cells) {
      std::vector<long> tmp;
      std::set_symmetric_difference(boundaries.begin(), boundaries.end(),
                                    phat_filtration[cell].second.begin(), phat_filtration[cell].second.end(),
                                    std::back_inserter(tmp));
      boundaries = tmp;
    }

    std::vector<long> pIDs;
    for (const auto& b : boundaries) {
      auto p = phat_point_map[b].second;
      pIDs.insert(pIDs.end(), p.begin(), p.end()); // append the three IDs of the vertices of a triangle
    }

    n->pIDs = pIDs;

    n->birth_alpha = phat_point_map[n->birth].first;
    n->death_alpha = phat_point_map[n->death].first;
    
    _replace_indices(n->left, phat_filtration, phat_point_map);
    _replace_indices(n->right, phat_filtration, phat_point_map);
  }

  void _replace_indices(Node* parent, Node* left, Node* right,
                        const std::vector<phat_type>& phat_filtration,
                        const std::vector<map_type>& phat_point_map) {
    
    if (!left) return; // right is NULl too

    // replace the indices of PHAT filtration with point IDs
    //
    // IDs of the cells in PHAT filtration
    // -> IDs of the boundaries in PHAT filtration
    // -> point IDs
    Node *small_n, *large_n;
    if (left->cells.size() < right->cells.size()) {
      small_n = left;
      large_n = right;
    }
    else {
      small_n = right;
      large_n = left;
    }

    // Calculate the boundaries of small one
    small_n->boundaries.clear();
    for (const auto cell : small_n->cells) {
      std::vector<long> tmp;
      std::set_symmetric_difference(small_n->boundaries.begin(), small_n->boundaries.end(),
                                    phat_filtration[cell].second.begin(), phat_filtration[cell].second.end(),
                                    std::back_inserter(tmp));
      small_n->boundaries = tmp;
    }

    small_n->pIDs.clear();
    for (const auto& b : small_n->boundaries) {
      auto p = phat_point_map[b].second;
      small_n->pIDs.insert(small_n->pIDs.end(), p.begin(), p.end());
      // append the three IDs of the vertices of a triangle
    }

    small_n->birth_alpha = phat_point_map[small_n->birth].first;
    small_n->death_alpha = phat_point_map[small_n->death].first;

    // Calculate the boundaries of large one by set symmetric difference between parent and small one
    large_n->boundaries.clear();
    std::set_symmetric_difference(parent->boundaries.begin(), parent->boundaries.end(),
                                  small_n->boundaries.begin(), small_n->boundaries.end(),
                                  std::back_inserter(large_n->boundaries));

    large_n->pIDs.clear();
    for (const auto& b : large_n->boundaries) {
      auto p = phat_point_map[b].second;
      large_n->pIDs.insert(large_n->pIDs.end(), p.begin(), p.end());
      // append the three IDs of the vertices of a triangle
    }    

    // To save memory
    parent->boundaries.clear();
    
    _replace_indices(left, left->left, left->right, phat_filtration, phat_point_map);
    _replace_indices(right, right->left, right->right, phat_filtration, phat_point_map);
  }

  void _replace_indices_inside(Node* n,
                               const std::vector<phat_type>& phat_filtration,
                               const std::vector<map_type>& phat_point_map) {
    if (!n) return;

    // replace the indices of PHAT filtration with point IDs
    //
    // IDs of the cells in PHAT filtration
    // -> point IDs
    n->pIDs.clear();
    for (const auto cell : n->cells) {
      auto p = phat_point_map[cell].second;
      n->pIDs.insert(n->pIDs.end(), p.begin(), p.end()); // append the three IDs of the vertices of a triangle
    }

    n->birth_alpha = phat_point_map[n->birth].first;
    n->death_alpha = phat_point_map[n->death].first;
    
    _replace_indices_inside(n->left, phat_filtration, phat_point_map);
    _replace_indices_inside(n->right, phat_filtration, phat_point_map);
  }

  
  void _save_depth_first(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      ofs << n->birth_alpha << " " << n->death_alpha << " " << n->net_persistence << " " << n->volume;
      for (const auto p : n->pIDs)
        ofs << " " << p;
      ofs << "\n";
      
      _save_depth_first(ofs, n->left);
      _save_depth_first(ofs, n->right);
    }
  }

  size_t _size(Node* n) {
    if (n) {
      return _size(n->left) + 1 + _size(n->right);
    }
    else return 0;
  }

  void _compute_net_persistence(Node* n)
  {
    if (n) {
      // the birth time of left and right are same
      if (n->left) n->net_persistence = n->left->birth_alpha - n->birth_alpha;
      else n->net_persistence = n->death_alpha - n->birth_alpha; // same as persistence length
                  
      _compute_net_persistence(n->left);
      _compute_net_persistence(n->right);
    }
  }

  void _compute_volume(const std::vector<Point>& points, Node* n) {
    if (!n) return;
    else {
      n->compute_volume(points);
      _compute_volume(points, n->left);
      _compute_volume(points, n->right);
    }
  }
  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree() {deleteNode(root);}

  size_t size() {return _size(root);}
  
  void load_ctree(std::ifstream& ifs) {
    _load_ctree(ifs, root, true);
    _load_ctree(ifs, root, false);
  }

  void replace_indices(const std::vector<phat_type>& phat_filtration,
                       const std::vector<map_type>& phat_point_map) {
    // replace the indices of PHAT filtration with point IDs
    //
    // IDs of the cells in PHAT filtration
    // -> IDs of the boundaries in PHAT filtration
    // -> point IDs
    root->pIDs.clear();
    for (const auto cell : root->cells) {
      std::vector<long> tmp;
      std::set_symmetric_difference(root->boundaries.begin(), root->boundaries.end(),
                                    phat_filtration[cell].second.begin(), phat_filtration[cell].second.end(),
                                    std::back_inserter(tmp));
      root->boundaries = tmp;
    }

    for (const auto& b : root->boundaries) {
      auto p = phat_point_map[b].second;
      root->pIDs.insert(root->pIDs.end(), p.begin(), p.end());
      // append the three IDs of the vertices of a triangle
    }

    root->birth_alpha = phat_point_map[root->birth].first;
    root->death_alpha = phat_point_map[root->death].first;
    
    _replace_indices(root, root->left, root->right, phat_filtration, phat_point_map);
  }

  void replace_indices_inside(const std::vector<phat_type>& phat_filtration,
                              const std::vector<map_type>& phat_point_map) {
    _replace_indices_inside(root, phat_filtration, phat_point_map);
  }

  void compute_net_persistence()
  {
    _compute_net_persistence(root);
  }


  void compute_volume(const std::vector<Point>& points) {
    _compute_volume(points, root);
  }
  
  void save(std::ofstream& ofs) const {
    _save_depth_first(ofs, root);
  }
};


class Cforest {
private:
  std::vector<Ctree*> ctrees;

  void _load_ph_tree(std::ifstream& ifs, Ctree* ctree) {
    ctree->load_ctree(ifs);
  }
  
public:
  Cforest() {};
  ~Cforest() {};

  void load_ph_tree(const std::string ph_tree_filename) {
    std::ifstream ifs(ph_tree_filename);
    std::string line;

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);
      
      long birth;
      ss >> birth;
      
      long death;
      ss >> death;
        
      std::vector<long> cells;
      long cell;
      while (ss >> cell)
        cells.push_back(cell);

      // new root
      Node* root = new Node(birth, death, cells);      
      Ctree* ctree = new Ctree(root);
      
      _load_ph_tree(ifs, ctree);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  void replace_indices(const std::vector<phat_type>& phat_filtration,
                       const std::vector<map_type>& phat_point_map) {
    for (auto& ctree : ctrees) {
      ctree->replace_indices(phat_filtration, phat_point_map);
    }
  }

  void replace_indices_inside(const std::vector<phat_type>& phat_filtration,
                              const std::vector<map_type>& phat_point_map) {
    for (auto& ctree : ctrees) {
      ctree->replace_indices_inside(phat_filtration, phat_point_map);
    }
  }

  
  void compute_net_persistence()
  {
    for (auto& ctree : ctrees) ctree->compute_net_persistence();
  }

  void compute_volume(const std::vector<Point>& points) {
    for (auto& ctree : ctrees) ctree->compute_volume(points);
  }
  
  void save(const std::string out_filename) const {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees) ctree->save(ofs);
    ofs.close();
  }
};


std::vector<Point> load_points(const std::string xyz_filename)
{
  std::vector<Point> points;
  
  std::ifstream ifs(xyz_filename);
  double x, y, z;
  while (ifs >> x >> y >> z) points.push_back(Point(x, y, z));

  return points;
}


void finalize(const std::string ph_tree_filename,
              const std::string corresponding_table_filename,
              const std::string phat_filtration_filename,
              const std::string xyz_filename,
              const std::string out_filename,
              const std::string out_filename_inside) {
  std::cout << "Load" << std::endl;

  // Load a correspoinding table from a PHAT filtration to point IDs
  std::vector<map_type> phat_point_map;
  std::ifstream ifs_ctable(corresponding_table_filename);
  std::string line;

  while (std::getline(ifs_ctable, line)) {
    std::stringstream ss(line);
    
    double alpha;
    ss >> alpha;

    std::vector<long> points;
    long p;
    while (ss >> p) points.push_back(p);

    phat_point_map.push_back({alpha, points});
  }

  // Load a PHAT filtration
  std::vector<phat_type> phat_filtration;
  std::ifstream ifs_phat(phat_filtration_filename);

  while (std::getline(ifs_phat, line)) {
    std::stringstream ss(line);
    
    double dim;
    ss >> dim;

    std::vector<long> boundaries;
    long b;
    while (ss >> b) boundaries.push_back(b);

    phat_filtration.push_back({dim, boundaries});
  }

  // Load cavity tree
  Cforest cforest;
  cforest.load_ph_tree(ph_tree_filename);

  // replace the indices of PHAT filtration with point IDs
  //
  // IDs of the cells in PHAT filtration
  // -> point IDs of all vertices of the cells
  std::cout << "Boundary and Inside" << std::endl;
  cforest.replace_indices_inside(phat_filtration, phat_point_map);
  
  std::cout << "Compute net persistence" << std::endl;
  cforest.compute_net_persistence(); // after replace indices

  std::cout << "Load points" << std::endl;
  auto points = load_points(xyz_filename);
  
  std::cout << "Compute volume" << std::endl;
  cforest.compute_volume(points);

  std::cout << "Save" << std::endl;  
  cforest.save(out_filename_inside);
  
  // replace the indices of PHAT filtration with point IDs
  //
  // IDs of the cells in PHAT filtration
  // -> IDs of the boundaries in PHAT filtration
  // -> point IDs
  std::cout << "Boundaries" << std::endl;
  cforest.replace_indices(phat_filtration, phat_point_map);
  
  // Save the results
  std::cout << "Save" << std::endl;  
  cforest.save(out_filename);
}


int main(int argc, char** argv) {
  std::string ph_tree_filename = argv[1];
  std::string corresponding_table_filename = argv[2];
  std::string phat_filtration_filename = argv[3];
  std::string xyz_filename = argv[4];
  std::string out_filename = argv[5];
  std::string out_filename_inside = argv[6];
  
  finalize(ph_tree_filename, corresponding_table_filename,
           phat_filtration_filename, xyz_filename, out_filename, out_filename_inside);
}
