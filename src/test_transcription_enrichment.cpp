#include <iostream>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <set>
#include <unordered_set>
#include <random>
#include <limits>
#include <omp.h>


class Transcript {
public:
  std::string gene_id;
  double TPM;
  std::vector<long> point_ids;
  double rank = 0;
  
  Transcript() {};
  Transcript(const std::string gid, const double tpm, const std::vector<long> pids)
    : gene_id(gid), TPM(tpm), point_ids(pids) {}
  Transcript(const std::string gid, const double tpm)
    : gene_id(gid), TPM(tpm) {}
};


std::vector<Transcript*> load_transcript(const std::string transcript_filename) {
  std::vector<Transcript*> transcripts;
  std::ifstream ifs(transcript_filename);
  std::string line;

  // Load each line
  std::getline(ifs, line); // header
    
  while (std::getline(ifs, line)) {
    std::stringstream ss(line);
    std::string gene_id, pids;
    double TPM;
    ss >> gene_id >> TPM >> pids;

    std::vector<long> point_ids;
    std::stringstream ss2(pids);
    std::string pid;
    while (std::getline(ss2, pid, ','))
      point_ids.push_back(std::stol(pid));
    
    transcripts.push_back(new Transcript(gene_id, TPM, point_ids));
  }

  return transcripts;
}



class Node {
public:
  double birth, death, net_persistence, volume;
  std::vector<long> pIDs;
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  std::vector<Transcript*> transcripts;
  bool is_valid_net_persistence = true;
  double sum_pval = std::numeric_limits<double>::quiet_NaN();
  double avg_pval = std::numeric_limits<double>::quiet_NaN();
  double rank_pval = std::numeric_limits<double>::quiet_NaN();
  double avg_rank_pval = std::numeric_limits<double>::quiet_NaN();
  double med_pval = std::numeric_limits<double>::quiet_NaN();
  
  
  Node(double b, double d, double np, double v, std::vector<long> c)
    : birth(b), death(d), net_persistence(np), volume(v), pIDs(c) {};
  ~Node() {};

  void permutation_test(const size_t num_samples,
                        const size_t sample_size,
                        const std::unordered_map<long, std::vector<Transcript*>*>& map_pids_to_transcripts)
  {
    // Compute the observed values
    double obs_sum=0.0, obs_avg=0.0, obs_rank=0.0, obs_avg_rank=0.0, obs_med=0.0;

    for (const auto t : transcripts) obs_sum += t->TPM;
    obs_avg = transcripts.size() == 0 ? 0 : obs_sum / transcripts.size();
    for (const auto t : transcripts) obs_rank += t->rank;
    obs_avg_rank = transcripts.size() == 0 ? 0 : obs_rank / transcripts.size();
    std::sort(transcripts.begin(), transcripts.end(),
              [](const Transcript* a, const Transcript* b) {return a->TPM < b->TPM;});
    obs_med = transcripts.size() == 0 ? 0 : transcripts[(transcripts.size() - 1) / 2]->TPM;


    int sum_pval_count = 0;
    int avg_pval_count = 0;
    int rank_pval_count = 0;
    int avg_rank_pval_count = 0;
    int med_pval_count = 0;

    // Permutation test
#pragma omp parallel for num_threads(12) reduction(+:sum_pval_count, avg_pval_count, rank_pval_count, avg_rank_pval_count, med_pval_count)
    for (size_t i=0; i<num_samples; i++) {
      std::unordered_set<int> sampled_pids;
      size_t counter = 0;

      // Sample point IDs
      std::random_device seed_gen;
      //std::mt19937 mt(seed_gen());
      std::default_random_engine mt(seed_gen());
      std::uniform_int_distribution<> rand_uni(0, sample_size - 1); // closed interval [0, sample_size - 1]
            
      while (sampled_pids.size() < pIDs.size()) {
        // Sometimes sagmentation fault occur this line. I guess this is a bug of gcc.
        // This may be solved by replacing mt19937 with default_random_engine
        int sample = rand_uni(mt);

        sampled_pids.insert(sample);
        if (counter++ > sample_size) {
          std::cerr << "Error: this random sample method is inefficient" << std::endl;
          std::exit(1);
        }
      }


      // Sample TPM
      /* 
         Sum (== Average of the number of points)
         Average of the number of genes
         Rank sum (because over-dispersion is expected due to highly expressed genes
         Median
      */
      std::vector<Transcript*> sampled_transcripts;
      for (const auto pid : sampled_pids) {
        if (map_pids_to_transcripts.find(pid) != map_pids_to_transcripts.end()) {
          for (const auto& t : *map_pids_to_transcripts.at(pid)) {
            sampled_transcripts.push_back(t);
          }
        }
      }

      std::unordered_set<Transcript*> s;
      for (const auto t: sampled_transcripts) s.insert(t);
      sampled_transcripts.assign(s.begin(), s.end());

      // Sum
      double sum = 0.0;
      for (const auto t : sampled_transcripts) sum += t->TPM;
      if (sum >= obs_sum) sum_pval_count += 1;
 
      // Average of the number of genes
      double avg = sampled_transcripts.size() == 0 ? 0 : sum / sampled_transcripts.size();
      if (avg >= obs_avg) avg_pval_count += 1;
      
      // Rank sum
      // transcript must be sorted and ranked.
      double rank_sum = 0;
      for (const auto t : sampled_transcripts) rank_sum += t->rank;
      if (rank_sum <= obs_rank) rank_pval_count += 1;

      double avg_rank_sum = sampled_transcripts.size() == 0 ? 0 : rank_sum / sampled_transcripts.size();
      if (avg_rank_sum <= obs_avg_rank) avg_rank_pval_count += 1;

      // Median
      std::sort(sampled_transcripts.begin(), sampled_transcripts.end(),
                [](const Transcript* a, const Transcript* b) {return a->TPM < b->TPM;});

      double median = sampled_transcripts.size() == 0 ?
        0 : sampled_transcripts[(sampled_transcripts.size() - 1) / 2]->TPM;
      if (median >= obs_med) med_pval_count += 1;
    }

    // Compute P values
    sum_pval = sum_pval_count; // int to float
    avg_pval = avg_pval_count;
    rank_pval = rank_pval_count;
    avg_rank_pval = avg_rank_pval_count;
    med_pval = med_pval_count;

    sum_pval /= num_samples;
    avg_pval /= num_samples;
    rank_pval /= num_samples;
    avg_rank_pval /= num_samples;
    med_pval /= num_samples;
  }
};


class Ctree {
private:
  Node* root;
  
  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _load(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);

      std::string not_used;
      bool is_valid_net_persistence = true;
      if (line[0] == '!') {
        ss >> not_used;
        is_valid_net_persistence = false;
      }
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      Node* n = new Node(birth, death, net_persistence, volume, pIDs);
      n->is_valid_net_persistence = is_valid_net_persistence;
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;
      
      _load(ifs, n, true);
      _load(ifs, n, false);
    }
  }

  void _load_with_transcript(std::ifstream& ifs, std::ifstream& ifs_t, Node* parent, bool is_left)
  {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') {
      std::getline(ifs_t, line);
      return;
    }
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);

      std::string not_used;
      bool is_valid_net_persistence = true;
      if (line[0] == '!') {
        ss >> not_used;
        is_valid_net_persistence = false;
      }
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      Node* n = new Node(birth, death, net_persistence, volume, pIDs);
      n->is_valid_net_persistence = is_valid_net_persistence;
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;

      // Load transcripts
      std::string gene_id;
      double tpm;
      while (ifs_t >> gene_id >> tpm) {
        n->transcripts.push_back(new Transcript(gene_id, tpm));
      }      
      
      _load_with_transcript(ifs, ifs_t, n, true);
      _load_with_transcript(ifs, ifs_t, n, false);
    }
  }

  void _map_pids_to_caves(std::unordered_map<long, std::vector<Node*>*>& map_pids_to_caves, Node* n) {
    if (!n) {
      return;
    }
    else {
      // Remove duplicates
      std::vector<long> pIDs;
      std::set<long> s;
      for (const auto p : n->pIDs) s.insert(p);
      pIDs.assign(s.begin(), s.end());

      for (const auto p : pIDs) {
        // [] accessor makes entry if it does not exist.
        if (!map_pids_to_caves[p]) map_pids_to_caves[p] = new std::vector<Node*>; 
        map_pids_to_caves[p]->push_back(n);
      }
      
      _map_pids_to_caves(map_pids_to_caves, n->left);
      _map_pids_to_caves(map_pids_to_caves, n->right);
    }
  }

  void _remove_duplicated_genes(Node* n) {
    if (!n) {
      return;
    }
    else {
      // Remove duplicates
      std::unordered_set<Transcript*> s;
      for (const auto t : n->transcripts) s.insert(t);
      n->transcripts.assign(s.begin(), s.end());      
      
      _remove_duplicated_genes(n->left);
      _remove_duplicated_genes(n->right);
    }
  }

  void _save_depth_first(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      if (!n->is_valid_net_persistence)
        ofs << "! ";
        
      ofs << n->birth << " " << n->death << " " << n->net_persistence << " " << n->volume;
      for (const auto p : n->pIDs)
        ofs << " " << p;
      ofs << "\n";
      
      _save_depth_first(ofs, n->left);
      _save_depth_first(ofs, n->right);
    }
  }

  void _save_depth_first_transcripts(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      if (n->transcripts.size() > 0) {
        ofs << n->transcripts[0]->gene_id << " " << n->transcripts[0]->TPM;
        for (size_t i=1; i<n->transcripts.size(); i++) {
          ofs << " " << n->transcripts[i]->gene_id << " " << n->transcripts[i]->TPM;
        }
      }
      
      ofs << "\n";
      
      _save_depth_first_transcripts(ofs, n->left);
      _save_depth_first_transcripts(ofs, n->right);
    }
  }

  void _save_depth_first_pval(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      ofs << n->sum_pval << " " << n->avg_pval << " " << n->rank_pval << " "
          << n->avg_rank_pval << " " << n->med_pval << "\n";
      
      _save_depth_first_pval(ofs, n->left);
      _save_depth_first_pval(ofs, n->right);
    }
  }

  size_t _size(Node* n) {
    if (n) {
      return _size(n->left) + 1 + _size(n->right);
    }
    else return 0;
  }


  void _permutation_test(const size_t num_samples,
                         const size_t sample_size,
                         std::unordered_map<long, std::vector<Transcript*>*>& map_pids_to_transcripts,
                         Node* n) {
    if (n) {
      if (n->is_valid_net_persistence)
        n->permutation_test(num_samples, sample_size, map_pids_to_transcripts);

      _permutation_test(num_samples, sample_size, map_pids_to_transcripts, n->left);
      _permutation_test(num_samples, sample_size, map_pids_to_transcripts, n->right);
    }
  }
  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree() {deleteNode(root);}

  size_t size() {return _size(root);}
  double root_persistence() {return root->death - root->birth;}
  
  void load_ctree(std::ifstream& ifs) {
    _load(ifs, root, true);
    _load(ifs, root, false);
  }

  void load_ctree_with_transcript(std::ifstream& ifs, std::ifstream& ifs_t) {
    _load_with_transcript(ifs, ifs_t, root, true);
    _load_with_transcript(ifs, ifs_t, root, false);
  }

  void map_pids_to_caves(std::unordered_map<long, std::vector<Node*>*>& map_pids_to_caves) {
    _map_pids_to_caves(map_pids_to_caves, root);
  } 

  void remove_duplicated_genes() {
    _remove_duplicated_genes(root);
  }
  
  void save_transcripts(std::ofstream& ofs) const {
    _save_depth_first_transcripts(ofs, root);
  }

  void save_pval(std::ofstream& ofs) const {
    _save_depth_first_pval(ofs, root);
  }

  void permutation_test(const size_t num_samples,
                        const size_t sample_size,
                        std::unordered_map<long, std::vector<Transcript*>*>& map_pids_to_transcripts)
  {
    _permutation_test(num_samples, sample_size, map_pids_to_transcripts, root);
  }
};


class Cforest {
private:
  std::vector<Ctree*> ctrees;

public:
  Cforest() {};
  ~Cforest() {};

  void load(const std::string filename) {
    std::ifstream ifs(filename);
    std::string line;

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);

      std::string not_used;
      bool is_valid_net_persistence = true;
      if (line[0] == '!') {
        ss >> not_used;
        is_valid_net_persistence = false;
      }

      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      // new root
      Node* root = new Node(birth, death, net_persistence, volume, pIDs);
      root->is_valid_net_persistence = is_valid_net_persistence;
      Ctree* ctree = new Ctree(root);
      
      ctree->load_ctree(ifs);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  
  void load_with_transcripts(const std::string filename,
                             const std::string transcript_filename)
  {
    std::ifstream ifs(filename);
    std::string line;

    std::ifstream ifs_t(transcript_filename);

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);

      std::string not_used;
      bool is_valid_net_persistence = true;
      if (line[0] == '!') {
        ss >> not_used;
        is_valid_net_persistence = false;
      }

      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      // new root
      Node* root = new Node(birth, death, net_persistence, volume, pIDs);
      root->is_valid_net_persistence = is_valid_net_persistence;
      Ctree* ctree = new Ctree(root);
            
      // Load transcripts
      std::string gene_id;
      double tpm;
      while (ifs_t >> gene_id >> tpm) {
        root->transcripts.push_back(new Transcript(gene_id, tpm));
      }
      
      ctree->load_ctree_with_transcript(ifs, ifs_t);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  void map_pids_to_caves(std::unordered_map<long, std::vector<Node*>*>& map_pids_to_caves) {
    for (const auto& ctree : ctrees)
      ctree->map_pids_to_caves(map_pids_to_caves);
  }

  void remove_duplicated_genes() {
    for (const auto& ctree : ctrees)
      ctree->remove_duplicated_genes();
  }
  
  void save_with_transcripts(const std::string out_filename) const {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees) {
      ctree->save_transcripts(ofs);
    }
    ofs.close();
  }

  void save_pval(const std::string out_filename) const {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees) {
      ctree->save_pval(ofs);
    }
    ofs.close();
  }

  void permutation_test(const size_t num_samples,
                        const size_t sample_size,
                        std::unordered_map<long, std::vector<Transcript*>*>& map_pids_to_transcripts) {
    for (auto& ctree : ctrees) {
      ctree->permutation_test(num_samples, sample_size, map_pids_to_transcripts);
    }
  }
};


size_t get_num_points(const std::string xyz_filename)
{
  size_t count;
  std::ifstream ifs(xyz_filename);
  std::string line;
  while (std::getline(ifs, line)) count++;
  return count;
}


void assign_pids_to_transcripts(std::unordered_map<long, std::vector<Transcript*>*>& map_pids_to_transcripts,
                             const std::vector<Transcript*>& transcripts)
{
  for (const auto t : transcripts) {
    for (const auto pid : t->point_ids) {
      if (map_pids_to_transcripts.find(pid) != map_pids_to_transcripts.end())
        map_pids_to_transcripts[pid]->push_back(t);
      else
        map_pids_to_transcripts[pid] = new std::vector<Transcript*>;
    }
  }
}


void assign_transcripts(std::vector<Transcript*>& transcripts,
                        std::unordered_map<long, std::vector<Node*>*>& map_pids_to_caves,
                        Cforest& cforest) {
  for (auto& transcript : transcripts) {
    for (const auto pid : transcript->point_ids) {
      if (map_pids_to_caves.find(pid) != map_pids_to_caves.end()) {
        for (auto& n : *(map_pids_to_caves.at(pid))) {
          n->transcripts.push_back(transcript);
        }
      }
    }
  }

  // Remove duplicated genes
  cforest.remove_duplicated_genes();
}


void permutation_test(const size_t num_samples,
                      const std::string cforest_filename,
                      const std::string xyz_filename,
                      const std::string transcript_on_point_filename,
                      const std::string out_filename)
{
  std::cout << "Load cavity trees with transcripts" << std::endl;
  Cforest cforest;
  //cforest.load_with_transcripts(cforest_filename, transcript_filename);
  cforest.load(cforest_filename);

  std::cout << "Get the number of points" << std::endl;
  size_t num_points = get_num_points(xyz_filename);

  std::cout << "Load transcripts at points" << std::endl;
  std::vector<Transcript*> transcripts = load_transcript(transcript_on_point_filename);

  // Sort and ranked by TPM
  std::sort(transcripts.begin(), transcripts.end(),
            [](const Transcript* a, const Transcript* b) {return a->TPM < b->TPM;});

  std::vector<Transcript*> ties = {transcripts[0]};
      
  for (size_t i=1; i<transcripts.size(); i++) {
    transcripts[i]->rank = transcripts.size() - i;

    if (transcripts[i - 1]->TPM == transcripts[i]->TPM) {
      ties.push_back(transcripts[i]);
    }
    else {
      double avg_rank = 0;
      for (const auto t : ties) avg_rank += t->rank;
      avg_rank /= ties.size();
      for (const auto t : ties) t->rank = avg_rank;
      ties = {transcripts[i]};
    }
  }

  double avg_rank = 0;
  for (const auto t : ties) avg_rank += t->rank;
  avg_rank /= ties.size();
  for (const auto t : ties) t->rank = avg_rank;

  std::cout << "Map point ID to caves" << std::endl;
  std::unordered_map<long, std::vector<Node*>*> map_pids_to_caves;
  cforest.map_pids_to_caves(map_pids_to_caves);

  std::cout << "Assign transcripts to caves using the map" << std::endl;
  assign_transcripts(transcripts, map_pids_to_caves, cforest);

  std::cout << "Map point ID to transcripts" << std::endl;
  std::unordered_map<long, std::vector<Transcript*>*> map_pids_to_transcripts;
  assign_pids_to_transcripts(map_pids_to_transcripts, transcripts);

  std::cout << "Perform permutation test" << std::endl;
  cforest.permutation_test(num_samples, num_points, map_pids_to_transcripts);

  std::cout << "Save" << std::endl;
  cforest.save_pval(out_filename);
}



int main(int argc, char** argv) {
  size_t num_samples = std::atoi(argv[1]);
  std::string cforest_filename = argv[2];
  std::string xyz_filename = argv[3];
  std::string transcript_on_point_filename = argv[4];
  std::string out_filename = argv[5];

  permutation_test(num_samples, cforest_filename, xyz_filename,
                   transcript_on_point_filename, out_filename);
}
