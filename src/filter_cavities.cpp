/*
  A point represents 20 kb of DNA.
  
  The nucleosome is represented as a cylinder 
  with 6 nm hight and 11 nm diameter.

  Each nucleosome includes 100 bp of DNA 
  which are a linker DNA and a twisted DNA around a nucleosome.

  The scale of a 3D model is determined as the diameter.
  I assume the diameter is 10 micro meter(um) for every 3D model.
  

  [Smallest radius of a bead]
  The volume of a nucleosome is 6 * (11/2)^2 * PI = 570.199... ~= 570
  The number of nucleosomes is 20000 / 100 = 200

  Let r be the radius of a bead
  4/3 * PI * r^3 = 570 * 200
  r = (570 * 200 * 3/4 / PI)^(1/3) = 30.07... ~= 30

  The smallest radius of a bead is 30 nm.


  [Largest radius of a bead]
  Linker B-form DNA length (~60 bp) is 0.34 nm/bp * 60 bp = 20.4 ~= 20 nm
  If nucleosomes are elongated, 20 * 200 = 4000 nm = 4 um
  This shape is a stick. However, the model is represented as the union of spheres.
  I have to consider the largest shape as sphere.
  Assuming a sphere is the circumsphere of a tetrahedron, 
  the largest sphere forms when a 4 um chromatin fiber traverse all four vertices of a tetrahedron.
  In this case, a chromatin fiber bends twice and is stretched as three edge connecting the four vetices.
  The radius of sphere is the circumradius of the four vertices.
  The length of a edge is 4 um / 3
  The circumradius is 6^(1/2) / 4 * 4 / 3 =  0.81649658092... um = 816.49658092... nm ~= 816 nm

  The largest radius of a bead is 816 nm


  [Smallest persistence length]
  The small persistence of a cave indicates that the cave is not truely cave,
  just is caused by noise or a loci-interacting point.
  
  To recognize nuclear speckles from transcription factories,
  I select the cavities with persistence length >= 100 nm.
  (ref http://jcs.biologists.org/content/124/21/3676)
  Note that the diameter of a nuclear speckle is very varied up to several micro meter. 
  
  The smallest persistence length is 100 nm.


  [Smallest net persistence of a cave]
  If a cave are divided immediately after the birth, 
  it cannot be said that it is not a true cave, but two divided ones are true.

  Net persistence is defined as
  (the birth time of a cave) - (the birth time of their child caves) in a cavity tree.

  I set this thresold the same value of the smallest persistence length 100 nm.
  
  The smallest net persistence is 100 nm 

 */


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <cmath>


class Node {
  void _delete_all_children(Node* n)
  {
    if (n) {
      _delete_all_children(n->left);
      _delete_all_children(n->right);

      delete n;
    }
  }

public:
  double birth, death, net_persistence, volume;
  std::vector<long> pIDs;
  Node *parent=nullptr, *left=nullptr, *right=nullptr;
  bool is_valid_net_persistence = true;
  
  Node(double b, double d, double np, double v, std::vector<long> c)
    : birth(b), death(d), net_persistence(np), volume(v), pIDs(c) {};
  ~Node() {};

  void delete_all_children()
  {
    _delete_all_children(left);
    _delete_all_children(right);
    
    left = nullptr;
    right = nullptr;
  }
};


class Ctree {
private:
  Node* root;
  
  void deleteNode(Node *n) {
    deleteNode(n->left);
    deleteNode(n->right);
    delete n;
  }

  void _load_ctree(std::ifstream& ifs, Node* parent, bool is_left) {
    std::string line;
    std::getline(ifs, line);

    if (line[0] == '$') return;
    else {
      // load a node
      // and go to children
      std::stringstream ss(line);
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      Node* n = new Node(birth, death, net_persistence, volume, pIDs);
      n->parent = parent;
      if (is_left) parent->left = n;
      else parent->right = n;
      
      _load_ctree(ifs, n, true);
      _load_ctree(ifs, n, false);
    }
  }

  
  void _save_depth_first(std::ofstream& ofs, const Node* const n) const {
    if (!n) {
      // No node, then output "$"
      ofs << "$\n";
    }
    else {
      if (!n->is_valid_net_persistence)
        ofs << "! ";
        
      ofs << n->birth << " " << n->death << " " << n->net_persistence << " " << n->volume;
      for (const auto p : n->pIDs)
        ofs << " " << p;
      ofs << "\n";
      
      _save_depth_first(ofs, n->left);
      _save_depth_first(ofs, n->right);
    }
  }

  size_t _size(Node* n) {
    if (n) {
      if (n->is_valid_net_persistence)
        return _size(n->left) + 1 + _size(n->right);
      else
        return _size(n->left) + _size(n->right);
    }
    else return 0;
  }

  std::vector<Ctree*> _filter_min_birth(const double min_birth, Node* n)
  {
    // Divide ctree
    // because the birth time of a parent is smaller than those of children.
    if (!n) {
      // This branch were filtered out.
      return std::vector<Ctree*>();
    }
    else {
      if (n->birth < min_birth) {
        // Divide
        if (n->left) n->left->parent = nullptr;
        if (n->right) n->right->parent = nullptr;
    
        auto new_ctrees = _filter_min_birth(min_birth, n->left);
        auto another_new_ctrees = _filter_min_birth(min_birth, n->right);
        new_ctrees.insert(new_ctrees.begin(), another_new_ctrees.begin(), another_new_ctrees.end());

        delete n;
        return new_ctrees;
      }
      else {
        // Keep a tree whose root is n.
        return std::vector<Ctree*>{new Ctree(n)};
      }
    }
  }


  // If the branch whose root is n was pruned, return true; otherwise return false.
  bool _filter_max_birth(const double max_birth, Node* n)
  {
    // Prune ctree
    // because the birth time of a parent is smaller than those of children.
    if (!n) {
      // No node was filtered out in this branch.
      return false;
    }
    else {
      if (n->birth > max_birth) {
        // Prune
        n->delete_all_children();
        delete n;

        return true;
      }
      else {
        // Keep this brach
        // If left branch was pruned,
        if (_filter_max_birth(max_birth, n->left)) n->left = nullptr;
        if (_filter_max_birth(max_birth, n->right)) n->right = nullptr;

        return false;
      }
    } 
  }


  // If the branch whose root is n was pruned, return true; otherwise return false.
  bool _filter_min_persistence(const double min_persistence, Node* n)
  {
    // Prune ctree
    // because the persistence length of a parent is larger than those of children.
    if (!n) {
      // No node was filtered out in this branch.
      return false;
    }
    else {
      double persistence = n->death - n->birth;
      
      if (persistence < min_persistence) {
        // Prune
        n->delete_all_children();
        delete n;

        return true;
      }
      else {
        // Keep this brach
        // If left branch was pruned,
        if (_filter_min_persistence(min_persistence, n->left)) n->left = nullptr;
        if (_filter_min_persistence(min_persistence, n->right)) n->right = nullptr;

        return false;
      }
    } 
  }

  void _filter_min_net_persistence(const double min_net_persistence, Node* n)
  {
    // Mark nodes insted of removing them to keep binary structure of ctree
    if (n) {
      // This node was filtered out
      if (n->net_persistence < min_net_persistence)
        n->is_valid_net_persistence = false;
      
      _filter_min_net_persistence(min_net_persistence, n->left);
      _filter_min_net_persistence(min_net_persistence, n->right);
    }
  }


  void _reduce_redundancy(Node* n)
  {
    if (!n) return;
    else {
      // Continue loops while there is a redundancy node.
      while ((!n->left && n->right) || (n->left && !n->right)) {
        // This node has only one child which is redundant.
        // Remove it.
        auto rm_node = n->left ? n->left : n->right;
        rm_node->parent = nullptr;
      
        // Replace left
        n->left = rm_node->left;
        rm_node->left = nullptr;
        if (n->left) n->left->parent = n;

        // Replace right
        n->right = rm_node->right;
        rm_node->right = nullptr;
        if (n->right) n->right->parent = n;

        delete rm_node;
      }

      _reduce_redundancy(n->left);
      _reduce_redundancy(n->right);
    }
  }

  
public:
  Ctree(Node *n) : root(n) {};
  ~Ctree()
  {
    if (root) deleteNode(root);
  }

  size_t size() {return _size(root);}
  double root_persistence() {return root->death - root->birth;}
  
  void load_ctree(std::ifstream& ifs) {
    _load_ctree(ifs, root, true);
    _load_ctree(ifs, root, false);
  }

  void save(std::ofstream& ofs) const {
    _save_depth_first(ofs, root);
  }

  std::vector<Ctree*> filter_min_birth(const double min_birth)
  {
    return _filter_min_birth(min_birth, root);
  }

  bool filter_max_birth(const double max_birth) {
    // If whole the ctree was filtered out, return true.
    if (_filter_max_birth(max_birth, root)) {
      root = nullptr;
      return true;
    }
    else {
      return false;
    }
  }

  bool filter_min_persistence(const double min_persistence)
  {
    if (_filter_min_persistence(min_persistence, root)) {
      root = nullptr;
      return true;
    }
    else {
      return false;
    }
  }
  
  void filter_min_net_persistence(const double min_net_persistence)
  {
    _filter_min_net_persistence(min_net_persistence, root);
  }

  void reduce_redundancy()
  {
    _reduce_redundancy(root);
  }
};


class Cforest {
private:
  std::vector<Ctree*> ctrees;

public:
  Cforest() {};
  ~Cforest() {};

  size_t size() {return ctrees.size();}

  size_t num_cavities()
  {
    size_t n = 0;
    for (const auto& ctree : ctrees) n += ctree->size();
    return n;
  }
  
  void load_ph_tree(const std::string ph_tree_filename) {
    std::ifstream ifs(ph_tree_filename);
    std::string line;

    while (std::getline(ifs, line)) {
      std::stringstream ss(line);
      
      double birth;
      ss >> birth;
      
      double death;
      ss >> death;

      double net_persistence;
      ss >> net_persistence;

      double volume;
      ss >> volume;
      
      std::vector<long> pIDs;
      long cell;
      while (ss >> cell)
        pIDs.push_back(cell);

      // new root
      Node* root = new Node(birth, death, net_persistence, volume, pIDs);      
      Ctree* ctree = new Ctree(root);
      
      ctree->load_ctree(ifs);

      ctrees.push_back(ctree);
    }

    ifs.close();
  }

  void save(const std::string out_filename) const
  {
    std::ofstream ofs(out_filename);
    for (const auto& ctree : ctrees)
        ctree->save(ofs);
    ofs.close();
  }

  // Filter the cavity forest with a minimum birth time
  void filter_min_birth(const double min_birth)
  {
    // Divide ctree
    // because the birth time of a parent is smaller than those of children.
    std::vector<Ctree*> filtered_ctrees;

    for (auto& ctree : ctrees) {
      auto new_ctrees = ctree->filter_min_birth(min_birth);
      filtered_ctrees.insert(filtered_ctrees.end(), new_ctrees.begin(), new_ctrees.end());
    }

    ctrees = filtered_ctrees;
  }
  
  // Filter the cavity forest with a maximum birth time
  void filter_max_birth(const double max_birth)
  {
    // Prune ctree
    // because the birth time of a parent is smaller than those of children.
    std::vector<Ctree*> filtered_ctrees;
    
    for (auto ctree : ctrees) {
      // If whole the ctree was not filtered out,
      if (!ctree->filter_max_birth(max_birth)) {
        filtered_ctrees.push_back(ctree);        
      }
      else {
        delete ctree;
      }
    }

    ctrees = filtered_ctrees;    
  }

  // Filter the cavity forest with a minimum persistence length
  void filter_min_persistence(const double min_persistence)
  {
    // Prune ctree
    // because the persistence length of a parent is larger than those of children.
    std::vector<Ctree*> filtered_ctrees;
    
    for (auto& ctree : ctrees) {
      // If whole the ctree was not filtered out,
      if (!ctree->filter_min_persistence(min_persistence))
        filtered_ctrees.push_back(ctree);
      else delete ctree;
    }

    ctrees = filtered_ctrees;    

  }
  
  // Filter the cavity forest with a minimum net persistence length
  void filter_min_net_persistence(const double min_net_persistence)
  {
    // Mark nodes insted of removing them to keep binary structure of ctree
    for (auto& ctree : ctrees)
      ctree->filter_min_net_persistence(min_net_persistence);
  }


  // Reduce the redundunt nodes to a single largest nodes among them
  void reduce_redundancy()
  {
    for (auto& ctree : ctrees)
      ctree->reduce_redundancy();
  }
};



class Point
{
public:
  double x, y, z;
  
  Point(const double a,
        const double b,
        const double c)
    : x(a), y(b), z(c) {}
};


double l2_dist(const Point& p, const Point& q)
{
  double x = p.x - q.x;
  double y = p.y - q.y;
  double z = p.z - q.z;
  return std::sqrt(x*x + y*y + z*z);
}


double compute_model_diameter(const std::string xyz_filename)
{
  std::vector<Point> points;
  
  std::ifstream ifs(xyz_filename);
  double x, y, z;
  while (ifs >> x >> y >> z) points.push_back(Point(x, y, z));
  

  double model_diameter = 0;

  for (size_t i=0; i<points.size(); i++) {
    for (size_t j=i+1; j<points.size(); j++) {
      auto d = l2_dist(points[i], points[j]);
      model_diameter = d > model_diameter ? d : model_diameter;
    }
  }
  
  return model_diameter;
}


void filter_cforest(const std::string cforest_filename,
                    const std::string xyz_filename,
                    const std::string out_filename)
{  
  std::cout << "Load cavity forest" << std::endl;
  Cforest cforest;
  cforest.load_ph_tree(cforest_filename);
  
  std::cout << "Get the diameter of a 3D model" << std::endl;
  //double model_diameter = compute_model_diameter(xyz_filename);
  double model_diameter = 5.13571;

  // Threshold
  double nm_to_model_scale = model_diameter / 10000.0; // Assuming model_diameter == 10 micro meter
  double min_birth = 30.0 * nm_to_model_scale;
  double max_birth = 816.0 * nm_to_model_scale;
  double min_persistence = 100 * nm_to_model_scale;
  double min_net_persistence = 1e-10;//1 * nm_to_model_scale;

  std::cout << min_birth << " " << max_birth << " " << min_persistence
            << " " << min_net_persistence << std::endl;  
  std::cout << cforest.size() << " " << cforest.num_cavities() <<  std::endl;

  std::cout << "Filter the cavity forest with a minimum birth time" << std::endl;
  cforest.filter_min_birth(min_birth);
  std::cout << cforest.size() << " " << cforest.num_cavities() <<  std::endl;
  
  std::cout << "Filter the cavity forest with a maximum birth time" << std::endl;
  cforest.filter_max_birth(max_birth);
  std::cout << cforest.size() << " " << cforest.num_cavities() <<  std::endl;

  std::cout << "Filter the cavity forest with a minimum persistence length" << std::endl;
  cforest.filter_min_persistence(min_persistence);
  std::cout << cforest.size() << " " << cforest.num_cavities() <<  std::endl;
  
  std::cout << "Filter the cavity forest with a minimum net persistence length" << std::endl;
  cforest.filter_min_net_persistence(min_net_persistence);
  std::cout << cforest.size() << " " << cforest.num_cavities() << std::endl;

  std::cout << "Reduce the redundunt nodes to a single largest nodes among them." << std::endl;
  cforest.reduce_redundancy();
  std::cout << cforest.size() << " " << cforest.num_cavities() << std::endl;
  
  std::cout << "Save" << std::endl;
  cforest.save(out_filename);
}


int main(int argc, char** argv)
{
  std::string cforest_filename = argv[1];
  std::string xyz_filename = argv[2];
  std::string out_filename = argv[3];

  filter_cforest(cforest_filename, xyz_filename, out_filename);
}
