#!/bin/bash

data_dir=data

if [ ! -d $data_dir ]; then mkdir data; fi

cd $data_dir
wget ftp://ftp.ncbi.nlm.nih.gov/geo/series/GSE117nnn/GSE117874/suppl/GSE117874_00README.md.txt
wget ftp://ftp.ncbi.nlm.nih.gov/geo/series/GSE117nnn/GSE117874/suppl/GSE117874_RAW.tar
tar xvf GSE117874_RAW.tar
find . -name "*.tar.gz" | xargs -n 1 tar zxvf
rm *.tar.gz *pairs*
gzip -d *.gz
cd ..
