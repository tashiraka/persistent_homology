#!/bin/bash

cur_dir=$(pwd)
data_dir=data/GSE117874
bin_dir=bin
processing_dir=/home/yuichi/opt/processing.py-3017-linux64

#GM12878 used

for name in $(find ${data_dir}/gm12878_*.3dg | xargs -n 1 basename)
do
    name=${name%.3dg}
    echo $name

    out_dir=products/${name}
    if [ ! -d $out_dir ]; then mkdir $out_dir; fi

    
    threedg=${data_dir}/${name}.3dg
    prefix=${out_dir}/${name}
    xyz=${prefix}.xyz
    filtration=${prefix}.filtration
    phat_input=${prefix}.filtration_for_phat
    map_phat_to_xyz=${prefix}.map_phat_to_xyz
    persistence=${prefix}.ph

    cforest=${prefix}.cforest
    cforest_pre=${cforest}_pre
    cforest_filtered=${cforest}_filtered

    cforest_inside=${cforest}_inside
    cforest_inside_pre=${cforest_inside}_pre
    cforest_inside_filtered=${cforest_inside}_filtered

    transcript=data/RNAseq_Barbara/ENCFF300QDV.tsv
    annotation=data/RNAseq_Barbara/gencode.v29lift37.annotation.gtf
    tpm_xyz=${out_dir}/tpm_xyz
    tpm=${cforest_filtered}_tpm
    tpm_inside=${cforest_inside_filtered}_tpm

    tpm_pval=${tpm}_pval
    tpm_inside_pval=${tpm_inside}_pval

    #set -x
    #time bash ${bin_dir}/dipCFormat3dg_to_xyz.sh $threedg $xyz
    #time ${bin_dir}/compute_filtration $xyz $filtration
    #time ${bin_dir}/change_format_of_filtration_for_phat $filtration $phat_input $map_phat_to_xyz
    #time ${bin_dir}/phat --verbose --ascii --vector_vector --standard $phat_input $persistence
    #time ${bin_dir}/compute_cavity_tree $persistence $cforest_pre
    #time ${bin_dir}/finalize $cforest_pre $map_phat_to_xyz $phat_input $xyz $cforest $cforest_inside

    #time ${bin_dir}/filter_cavities $cforest $xyz $cforest_filtered
    #time ${bin_dir}/filter_cavities $cforest_inside $xyz $cforest_inside_filtered

    #time ${bin_dir}/map_transcripts_to_points $transcript $annotation $threedg $tpm_xyz
    #time ${bin_dir}/map_transcripts_to_cavities $tpm_xyz $cforest_filtered $tpm
    #time ${bin_dir}/map_transcripts_to_cavities $tpm_xyz $cforest_inside_filtered $tpm_inside

    num_samples=(100 500 1000 2000 5000 7500 10000)
    rounds=10
    for num_sample in ${num_samples[@]}
    do
        for round in $(seq 1 ${rounds})
        do
            tpm_pval=${tpm}_pval_${num_sample}_${round}
            tpm_inside_pval=${tpm_inside}_pval_${num_sample}_${round}

            #time ${bin_dir}/test_transcription_enrichment $num_sample $cforest_filtered $xyz $tpm_xyz $tpm_pval
            #time ${bin_dir}/test_transcription_enrichment $num_sample $cforest_inside_filtered $xyz $tpm_xyz $tpm_inside_pval
        done
    done
    
    #set +x
done

tpms=$(find products/gm12878_*/gm12878_*.cforest_filtered_tpm)
tpm_xyzs=$(find products/gm12878_*/tpm_xyz)
cforests=$(find products/gm12878_*/gm12878_*.cforest_filtered)
threedgs=$(find ${data_dir}/gm12878_*.3dg)
xyzs=$(find products/gm12878_*/gm12878_*.xyz)

common_genes=products/common_genes
common_loci=products/common_loci

set -x
#time ${bin_dir}/intersect_cave_faced_genes_between_replicates $tpms $common_genes
time ${bin_dir}/intersect_cave_faced_loci_between_replicates $threedgs $cforests $common_loci


num_samples=(100) #(100 500 1000 2000 5000 7500 10000 100000)
rounds=1
for num_sample in ${num_samples[@]}
do
    for round in $(seq 1 ${rounds})
    do
        num_gene_pval=${common_genes}_pval_${num_sample}_${round}
        num_loci_pval=${common_loci}_pval_${num_sample}_${round}
#        time ${bin_dir}/test_gene_intersection $num_sample $common_genes $cforests $xyzs $tpm_xyzs $num_gene_pval
#        time ${bin_dir}/test_loci_intersection $num_sample $common_loci $threedgs $cforests $xyzs $num_loci_pval
    done
done

set +x

name=gm12878_01
out_dir=products/${name}
if [ ! -d $out_dir ]; then mkdir $out_dir; fi
threedg=${data_dir}/${name}.3dg
prefix=${out_dir}/${name}
xyz=${prefix}.xyz
filtration=${prefix}.filtration
phat_input=${prefix}.filtration_for_phat
map_phat_to_xyz=${prefix}.map_phat_to_xyz
persistence=${prefix}.ph

cforest=${prefix}.cforest
cforest_pre=${cforest}_pre
cforest_filtered=${cforest}_filtered

cforest_inside=${cforest}_inside
cforest_inside_pre=${cforest_inside}_pre
cforest_inside_filtered=${cforest_inside}_filtered

transcript=data/RNAseq_Barbara/ENCFF300QDV.tsv
annotation=data/RNAseq_Barbara/gencode.v29lift37.annotation.gtf
tpm_xyz=${out_dir}/tpm_xyz
tpm=${cforest_filtered}_tpm
tpm_inside=${cforest_inside_filtered}_tpm


cforest_no_break=${cforest}_no_break
cforest_inside_no_break=${cforest_inside}_no_break

cforest_filtered_roots=${cforest_filtered}_roots
cforest_inside_filtered_roots=${cforest_inside_filtered}_roots

hist=${cforest_inside}.png


set -x
#time bash ${bin_dir}/dipCFormat3dg_to_xyz.sh $threedg $xyz
#time ${bin_dir}/compute_filtration $xyz $filtration
#time ${bin_dir}/change_format_of_filtration_for_phat $filtration $phat_input $map_phat_to_xyz
#time ${bin_dir}/phat --verbose --ascii --vector_vector --standard $phat_input $persistence
#time ${bin_dir}/compute_cavity_tree $persistence $cforest_pre
#time ${bin_dir}/finalize $cforest_pre $map_phat_to_xyz $phat_input $xyz $cforest $cforest_inside

#time ${bin_dir}/remove_break_point_from_cavities $threedg $cforest $cforest_inside $cforest_no_break $cforest_inside_no_break

#time ${bin_dir}/filter_cavities $cforest $xyz $cforest_filtered
#time ${bin_dir}/filter_cavities $cforest_inside $xyz $cforest_inside_filtered


#time python ${bin_dir}/plot_histogram.py $cforest_inside $hist
#${bin_dir}/draw_3d_diff_color $threedg $cforest_filtered

#time ${bin_dir}/map_transcripts_to_points $transcript $annotation $threedg $tpm_xyz
#time ${bin_dir}/map_transcripts_to_cavities $tpm_xyz $cforest_filtered $tpm
#time ${bin_dir}/map_transcripts_to_cavities $tpm_xyz $cforest_inside_filtered $tpm_inside

#${bin_dir}/draw_3d_polygon $threedg $cforest_filtered $tpm $tpm_xyz

num_samples=(1000) #(100 500 1000 2000 5000 7500 10000)
rounds=1
for num_sample in ${num_samples[@]}
do
    for round in $(seq 1 ${rounds})
    do
    tpm_pval=${tpm}_pval_${num_sample}_${round}
    tpm_inside_pval=${tpm_inside}_pval_${num_sample}_${round}

    #time ${bin_dir}/test_transcription_enrichment $num_sample $cforest_filtered $xyz $tpm_xyz $tpm_pval
    #time ${bin_dir}/test_transcription_enrichment $num_sample $cforest_inside_filtered $xyz $tpm_xyz $tpm_inside_pval
    done
done

#time ${bin_dir}/extract_roots $cforest_filtered $cforest_filtered_roots
#time ${bin_dir}/extract_roots $cforest_inside_filtered $cforest_inside_filtered_roots
set +x

# 重すぎて動かない
#THREEDG=${cur_dir}/${data_dir}/${name}.3dg CAVITY=${cur_dir}/${out_dir}/${name}.cforest_finalized_persistence0_tpm_roots CAVITY_NUM=1 OUT_FILE=${cur_dir}/${out_dir}/${name}.cforest_finalized_persistence0_tpm_roots.png java -jar ${processing_dir}/processing-py.jar ${cur_dir}/src/draw_root.py


# これは後でいいか
#python src/plot_persistence_TPM.py ${out_dir}/${name}.cforest_finalized_persistence0_tpm ${out_dir}/${name}.cforest_finalized_persistence0_tpm_roots.png
#time ${bin_dir}/compute_volume ${out_dir}/${name}.cforest_finalized_inside_persistence0 ${out_dir}/${name}.xyz ${out_dir}/${name}.cforest_finalized_inside_persistence0_volume
#python src/plot_persistence_volume.py ${out_dir}/${name}.cforest_finalized_inside_persistence0_volume ${out_dir}/${name}.cforest_finalized_inside_persistence0_volume.png

#python src/plot_volume_TPM.py ${out_dir}/${name}.cforest_finalized_tpm ${out_dir}/${name}.cforest_finalized_inside_persistence0_volume ${out_dir}/${name}.cforest_finalized_inside_persistence0_volume_tpm.png

#${bin_dir}/draw_3d ${data_dir}/${name}.3dg
#${bin_dir}/draw_3d_diff_color ${data_dir}/${name}.3dg ${out_dir}/${name}.cforest_finalized

data_dir=examples
out_dir=examples
bin_dir=bin
name=tetrahedron

#time ${bin_dir}/compute_filtration ${out_dir}/${name}.xyz ${out_dir}/${name}.filtration
#time ${bin_dir}/change_format_of_filtration_for_phat ${out_dir}/${name}.filtration ${out_dir}/${name}.filtration_for_phat ${out_dir}/${name}.corresponding_table_phat_xyz
#time ${bin_dir}/phat --verbose --ascii --vector_vector --standard ${out_dir}/${name}.filtration_for_phat ${out_dir}/${name}.ph
#time ${bin_dir}/compute_cavity_tree ${out_dir}/${name}.ph ${out_dir}/${name}.cforest


name=two_tetrahedron

#time ${bin_dir}/compute_filtration ${out_dir}/${name}.xyz ${out_dir}/${name}.filtration
#time ${bin_dir}/change_format_of_filtration_for_phat ${out_dir}/${name}.filtration ${out_dir}/${name}.filtration_for_phat ${out_dir}/${name}.corresponding_table_phat_xyz
#time ${bin_dir}/phat --verbose --ascii --vector_vector --standard ${out_dir}/${name}.filtration_for_phat ${out_dir}/${name}.ph
#time ${bin_dir}/compute_cavity_tree ${out_dir}/${name}.ph ${out_dir}/${name}.cforest
