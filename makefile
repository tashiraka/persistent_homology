.PHONY: all debug clean download run


all:
	if [ ! -d bin ]; then mkdir bin; fi
	if [ ! -d products ]; then mkdir products; fi
	-cd src && make && \
	cp dipCFormat3dg_to_xyz.sh compute_filtration change_format_of_filtration_for_phat compute_cavity_tree finalize filter_cavities remove_break_point_from_cavities map_transcripts_to_points map_transcripts_to_cavities extract_roots compute_volume draw_3d draw_3d_diff_color draw_3d_polygon plot_histogram.py test_transcription_enrichment intersect_cave_faced_genes_between_replicates intersect_cave_faced_loci_between_replicates test_gene_intersection test_loci_intersection ../bin
	cd externals/phat-master && cmake -DCMAKE_CXX_FLAGS=-std=c++11 . && make && cp phat ../../bin

debug:
	if [ ! -d bin ]; then mkdir bin; fi
	if [ ! -d products ]; then mkdir products; fi
	-cd src && make debug && \
	cp dipCFormat3dg_to_xyz.sh compute_filtration change_format_of_filtration_for_phat compute_cavity_tree finalize filter_cavities remove_break_point_from_cavities map_transcripts_to_points map_transcripts_to_cavities extract_roots compute_volume draw_3d draw_3d_diff_color draw_3d_polygon plot_histogram.py test_transcription_enrichment intersect_cave_faced_genes_between_replicates intersect_cave_faced_loci_between_replicates test_gene_intersection test_loci_intersection ../bin
	cd externals/phat-master && cmake -DCMAKE_CXX_FLAGS=-std=c++11 . && make && cp phat ../../bin


download:
	cd data && bash download.sh


clean:
	cd src && make clean
	cd externals/phat-master && make clean && rm CMakeCache.txt Makefile cmake_install.cmake && rm -r CMakeFiles/
	-rm -r bin
	-rm *~
	-rm \#*\#
	-rm .\#*
