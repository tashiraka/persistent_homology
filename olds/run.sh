#!/bin/bash


data_dir=data/GSE117874
out_dir=products
bin_dir=bin


for downloaded_file in $(find ${data_dir}/*.3dg)
do
    echo "Job of $downloaded_file"
    bn=$(basename $downloaded_file)
    name=${bn%.*} # ".3dg" is removed.
    log=${out_dir}/${name}.log

    qsub -S /bin/bash -V -cwd -l mem_free=16g -o ${log} -e ${log} \
         -j y -r y -N ${name}_ph <<EOF
#!/bin/bash

echo "3dg to xyz..."
time bash ${bin_dir}/dipCFormat3dg_to_xyz.sh ${data_dir}/${name}.3dg ${out_dir}/${name}.xyz
echo -e "Done.\n\n"

echo "Computing filtration..."
time ${bin_dir}/compute_filtration ${out_dir}/${name}.xyz ${out_dir}/${name}.filtration
echo -e"Done.\n\n"

echo "Computing persistent homology..."
time ${bin_dir}/compute_persistent_homology ${out_dir}/${name}.filtration ${out_dir}/${name}.xyz ${out_dir}/${name}.persistence
echo -e "Done.\n\n"
EOF
    
done

#name=small_test #test #gm12878_01
#name=gm12878_01

#time bash ${bin_dir}/dipCFormat3dg_to_xyz.sh ${data_dir}/${name}.3dg ${out_dir}/${name}.xyz
#time ${bin_dir}/compute_filtration ${out_dir}/${name}.xyz ${out_dir}/${name}.filtration
#time ${bin_dir}/compute_persistent_homology ${out_dir}/${name}.filtration ${out_dir}/${name}.xyz ${out_dir}/${name}.persistence


#no use
#time ${bin_dir}/indexing_filtration ${out_dir}/${name}.xyz ${out_dir}/${name}.filtration ${out_dir}/${name}.xyz ${out_dir}/${name}.filtration_indexed
#time ${bin_dir}/compute_persistent_homology ${out_dir}/${name}.filtration_indexed ${out_dir}/${name}.xyz ${out_dir}/gm12878_01.persistence
