#include <iostream>
#include <string>
#include <vector>

#include <mpi.h>

// external include for distributed sorting
#include <psort-1.0/src/psort.h>


/*
 * Global variables
 */
namespace globals
{
  // MPI has some 32bit limitations, so we call it blockwise
  int MPI_BLOCK_SIZE = 1 << 23;
}

/*
 * MPI utils
 */
inline bool is_root() { return get_rank() == 0; }

inline int get_rank()
{
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  return rank;
}


inline int get_num_processes()
{
  int num_processes;
  MPI_Comm_size(MPI_COMM_WORLD, &num_processes);
  return num_processes;
}


inline int64_t get_range_width(int64_t global_num_elems)
{
  return (global_num_elems + get_num_processes() - 1) / get_num_processes();
}


// Global boundary matrix での index であるrankのノードが担当するindexを返す。
inline int64_t get_local_begin(int64_t global_num_elems, int rank)
{
  return std::min(get_range_width(global_num_elems) * rank, global_num_elems);
}


inline int64_t get_local_begin(int64_t global_num_elems)
{
  return get_local_begin(global_num_elems, get_rank());
}


inline int64_t get_local_end(int64_t global_num_elems, int rank)
{
  return std::min(get_range_width(global_num_elems) * (rank + 1), global_num_elems);
}


inline int64_t get_local_end(int64_t global_num_elems)
{
  return get_local_end(global_num_elems, get_rank());
}


/*
 * MPI IO
 */
inline MPI_File file_open(const std::string& filename, int access)
{
  MPI_File file;
  if (MPI_File_open(MPI_COMM_WORLD, const_cast<char*>(filename.c_str()), access, MPI_INFO_NULL, &file) != 0)
    {
      if (is_root())
        {
          std::cerr << "Error while opening file " << filename << std::endl;
          MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        }
    }
  return file;
}


inline MPI_File file_open_read_only(const std::string& filename)
{
  return file_open(filename, MPI_MODE_RDONLY);
}


template< typename T >
void file_read_at_vector(const MPI_File& file,
                         const MPI_Offset& offset,
                         int64_t size,
                         std::vector< T >& content)
{
  content.resize(size);
  int chunk_size = globals::MPI_BLOCK_SIZE / sizeof(T);
  int64_t num_chunks = size / chunk_size + 1;

  for (int64_t cur_chunk = 0; cur_chunk < num_chunks; cur_chunk++) {
      int bytes_to_read = (cur_chunk < num_chunks - 1)
        ? chunk_size * sizeof(T) : (size % chunk_size) * sizeof(T);

      MPI_Offset cur_offset = offset + cur_chunk * chunk_size * sizeof(T);
      MPI_File_read_at(file, cur_offset, content.data() + cur_chunk * chunk_size,
                       bytes_to_read, MPI_BYTE, MPI_STATUS_IGNORE);
  }
}


/*
 * Data structure
 */
class heap_column : public std::vector< int64_t >
{
private:
  std::vector< int64_t > temp_col;
  int64_t pushes_since_last_prune;

public:
  heap_column() : pushes_since_last_prune(0) {};

  template< typename Iterator >
  void add_column(Iterator first, Iterator last)
  {
    for (Iterator it = first; it != last; it++)
      push(*it);
    pushes_since_last_prune += std::distance(first, last);
    if (2 * pushes_since_last_prune > (int64_t)size() + 1024 * 1024)
      prune();
  }

  void prune()
  {
    if (pushes_since_last_prune > 0)
      {
        temp_col.clear();
        int64_t max_index = pop_max_index();
        while (max_index != -1)
          {
            temp_col.push_back(max_index);
            max_index = pop_max_index();
          }
        for (const auto& index : temp_col)
          push(index);
        pushes_since_last_prune = 0;
      }
  }

  void clear()
  {
    pushes_since_last_prune = 0;
    static_cast<std::vector< int64_t >*>(this)->clear();
  }


  int64_t get_max_index()
  {
    int64_t max_index = pop_max_index();
    if (max_index != -1)
      push(max_index);
    return max_index;
  }

private:
  void push(int64_t index)
  {
    push_back(index);
    std::push_heap(begin(), end());
  }

  int64_t pop()
  {
    int64_t top = front();
    std::pop_heap(begin(), end());
    pop_back();
    return top;
  }

  int64_t pop_max_index()
  {
    if (empty())
      return -1;
    else
      {
        int64_t max_element = pop();
        while (!empty() && front() == max_element)
          {
            pop();
            if (empty())
              return -1;
            else
              max_element = pop();
          }
        return max_element;
      }
  }
};


class flat_column_stack
{
  // We make the internal data representation public so that we can easily transmit the object using MPI
public:
  std::vector< int64_t > stack_data;

public:
  void swap(flat_column_stack& other) { std::swap(other.stack_data, stack_data); }
  void clear() { stack_data.clear(); }
  bool empty() const { return stack_data.empty(); }

  void shrink_to_fit()
  {
    if (stack_data.capacity() > 2 * stack_data.size())
      stack_data.shrink_to_fit();
  }

  void push(int64_t index, const heap_column& col)
  {
    stack_data.push_back(index);
    stack_data.insert(stack_data.end(), col.begin(), col.end());
    stack_data.push_back(col.size());
  }

  void pop(int64_t& index, heap_column& col)
  {
    const int64_t col_size = stack_data.back();
    col.clear();
    col.resize(col_size);
    std::copy(stack_data.rbegin() + 1, stack_data.rbegin() + 1 + col_size, col.rbegin());
    index = stack_data[stack_data.size() - 2 - col_size];
    stack_data.resize(stack_data.size() - 2 - col_size);
  }
};


template< typename ValueType >
class Distributed_vector
{
public:
  typedef ValueType value_type;

protected:
  std::vector< value_type > entries;
  int64_t local_begin;
  int64_t local_end;
  int64_t global_num_elems;

public:
  void init(int64_t arg_global_num_elems)
  {
    local_begin = get_local_begin(arg_global_num_elems);
    local_end = get_local_end(arg_global_num_elems);
    const int64_t local_num_elems = local_end - local_begin;
    entries.resize(local_num_elems);
    global_num_elems = arg_global_num_elems;
  }

  void set_local_value(int64_t idx, const value_type& value)
  {
    assert(idx >= local_begin && idx < local_end);
    entries[idx - local_begin] = value;
  }

  value_type get_local_value(int64_t idx) const
  {
    assert(idx >= local_begin && idx < local_end);
    return entries[idx - local_begin];
  }

  void get_global_values(const std::vector< int64_t >& indices,
                         std::vector< value_type >& values) const
  {
    std::vector< std::vector< int64_t > > queries_buffer;
    element_distribution::scatter_queries(indices, global_num_elems, queries_buffer);

    // process queries
    std::vector< std::vector< int64_t > > answers_buffer(mpi_utils::get_num_processes());
    for (int source = 0; source < mpi_utils::get_num_processes(); source++)
      {
        for (const auto& query : queries_buffer[source])
          answers_buffer[source].push_back(entries[query - local_begin]);
      }

    element_distribution::gather_answers(indices, global_num_elems, answers_buffer, values);
  }


  void set_global_values(const std::vector< std::pair< int64_t, value_type > >& indices_and_values)
  {
    // 1. generate pairs for all ranks
    std::vector< std::vector< std::pair< int64_t, value_type > > > pairs_for_ranks(mpi_utils::get_num_processes());
    for (const auto& index_and_value : indices_and_values)
      pairs_for_ranks[element_distribution::get_rank(global_num_elems, index_and_value.first)].push_back(index_and_value);

    // 2. send pairs to other ranks
    std::vector< MPI_Request > requests;
    for (int target = 0; target < mpi_utils::get_num_processes(); target++)
      mpi_utils::non_blocking_send_vector(pairs_for_ranks[target], target, mpi_utils::MSG_SET_GLOBAL_VALUES, requests);

    // 3. receive pairs from other ranks
    std::vector< std::pair< int64_t, value_type > > buffer;
    for (int idx = 0; idx < mpi_utils::get_num_processes(); idx++)
      {
        const int source = (mpi_utils::get_rank() + idx) % mpi_utils::get_num_processes();
        mpi_utils::receive_vector(buffer, source, mpi_utils::MSG_SET_GLOBAL_VALUES);
        for (const auto& buffer_elem : buffer)
          set_local_value(buffer_elem.first, buffer_elem.second);
      }

    // 4. need to make sure that above send completed before its buffer goes out of scope
    MPI_Waitall((int)requests.size(), requests.data(), MPI_STATUSES_IGNORE);
  }
};


class Write_once_column_array
{
protected:
  std::vector<int64_t> column_begin;
  std::vector<int64_t> column_end;
  std::vector<int64_t> cell_index;
  std::vector<int64_t> flat_column_entries;

  int64_t local_begin;
  int64_t local_end;

public:
  void init(int64_t global_num_cols)
  {
    local_begin = get_local_begin(global_num_cols);
    local_end = get_local_end(global_num_cols);
    const int64_t local_num_cols = local_end - local_begin + 1;
    column_begin.resize(local_num_cols, -1);
    column_end.resize(local_num_cols, -1);
    cell_index.resize(local_num_cols, -1);
  }
};


class Complex
{
  int64_t global_num_cells;
  int64_t max_dim;
  int64_t local_begin;

  std::vector< int64_t > dims;
  std::vector< double > values;
  std::vector< int64_t > offsets;
  std::vector< int64_t > entries;

  
  // Read the boundary matrices in DIPHA binary format -- all symbols are 64 bit wide
  // Format: file_types::DIPHA % file_types::WEIGHTED_BOUNDARY_MATRIX % boundary_type % num_cells (N)
  //         % max_dim % dim1 % ... % dimN % value1 % ... % valueN % offset1 % ... % offsetN
  //         % num_entries (M) % entry1 % ... % entryM 
  void _read_binary_boundary_matrices(MPI_File file)
  {
    // Read preamble
    std::vector<int64_t> preamble;
    int64_t preamble_size = 5;
    file_read_at_vector(file, 0, preamble_size, preamble);

    // Contents in the preamble
    int64_t dipha_identifier = preamble[0];
    int64_t file_type = preamble[1];
    int64_t boundary_type = preamble[2];
    global_num_cells = preamble[3];
    max_dim = preamble[4];

    local_begin = get_local_begin(global_num_cells);
    const int64_t num_local_cells = get_local_end(global_num_cells) - local_begin;

    // Read dimension data
    MPI_Offset dimensions_begin = (preamble.size() + local_begin) * sizeof(int64_t);
    file_read_at_vector(file, dimensions_begin, num_local_cells, dims);

        // read values data
    MPI_Offset values_begin = (preamble.size() + global_num_cells + local_begin) * sizeof(int64_t);
    file_read_at_vector(file, values_begin, num_local_cells, values);

    // read offsets data
    MPI_Offset offsets_begin = (preamble.size() + 2 * global_num_cells + local_begin) * sizeof(int64_t);
    file_read_at_vector(file, offsets_begin, num_local_cells + 1, offsets);

    // read entries data
    int64_t entries_start = offsets.front();
    int64_t local_num_entries = offsets.back() - entries_start;
    MPI_Offset entries_begin = (preamble.size() + 3 * global_num_cells + 1 + entries_start) * sizeof(int64_t);
    mpi_utils::file_read_at_vector(file, entries_begin, local_num_entries, entries);
  }

  
public:
  int64_t get_num_cells() const { return global_num_cells; }

  double get_local_value(int64_t idx) const { return values[idx - local_begin]; }

  int64_t get_local_dim(int64_t idx) const { return dims[idx - local_begin]; }
  
  void read_binary_boundary_matrices(const std::string& filename)
  {
    MPI_File file = file_open_read_only(filename);
    _read_binary_boundary_matrices(file);    
    MPI_File_close(&file);
  }
};


/*
 * Algorithm
 */
/*
 * The map of alpha values to the indices of simplices
 */
void get_filtration_to_cell_map(const Complex& complex,
                               Distributed_vector<int64_t>& filtration_to_cell_map)
{
  const int64_t global_num_cells = complex.get_num_cells();
  const int64_t local_begin = get_local_begin(global_num_cells);
  const int64_t local_end = get_local_end(global_num_cells);
  const int64_t local_num_cells = local_end - local_begin;

  // (alpha value, (dim, simplex idx))
  typedef std::pair< double, std::pair< int64_t, int64_t > > sort_value_type;
  std::vector<sort_value_type> filtration(local_num_cells);

  for (int64_t cur_cell = local_begin; cur_cell < local_end; cur_cell++) {
    filtration[cur_cell - local_begin].first = complex.get_local_value(cur_cell);
    filtration[cur_cell - local_begin].second.first = complex.get_local_dim(cur_cell);
    filtration[cur_cell - local_begin].second.second = cur_cell;
  }

  // psort unfortunately uses long for the size. This will cause problems on Win64 for large data
  std::vector<long> cell_distribution;
  int num_processes = get_num_processes();
  for (int cur_rank = 0; cur_rank < num_processes; cur_rank++)
    cell_distribution.push_back((long)(get_local_end(global_num_cells, cur_rank)
                                       - get_local_begin(global_num_cells, cur_rank)));

  p_sort::parallel_sort(filtration.begin(), filtration.end(), std::less< sort_value_type >(),
                        cell_distribution.data(), MPI_COMM_WORLD);

  filtration_to_cell_map.init(global_num_cells);
  for (int64_t cur_cell = local_begin; cur_cell < local_end; cur_cell++)
    filtration_to_cell_map.set_local_value(cur_cell, filtration[cur_cell - local_begin].second.second);
}


inline void get_cell_to_filtration_map(int64_t global_num_cells,
                                       const distributed_vector< int64_t >& filtration_to_cell_map,
                                       distributed_vector< int64_t >& cell_to_filtration_map)
{
  const int64_t local_begin = get_local_begin(global_num_cells);
  const int64_t local_end = get_local_end(global_num_cells);
  const int64_t local_num_cells = local_end - local_begin;

  std::vector < std::pair< int64_t, int64_t > > indices_and_values;
  for (int64_t idx = local_begin; idx < local_end; idx++)
    indices_and_values.push_back(std::make_pair(filtration_to_cell_map.get_local_value(idx), idx));

  cell_to_filtration_map.init(global_num_cells);
  cell_to_filtration_map.set_global_values(indices_and_values);
}


void generate_unreduced_columns(const Complex& complex,
                                const Distributed_vector<int64_t>& filtration_to_cell_map,
                                const Distributed_vector<int64_t>& cell_to_filtration_map,
                                int64_t dim,
                                flat_column_stack& unreduced_columns)
{
  const int64_t global_num_cells = complex.get_num_cells();
  const int64_t local_begin = get_local_begin(global_num_cells); // Matrix で担当する index の範囲
  const int64_t local_end = get_local_end(global_num_cells);  // [local_begin, local_end)
  const int64_t local_num_cells = local_end - local_begin + 1;

  const int64_t num_chunks = (local_num_cells + globals::DIPHA_BLOCK_SIZE - 1) / globals::DIPHA_BLOCK_SIZE;

  std::vector< int64_t > dim_queries;
  std::vector< int64_t > dim_answers;
  std::vector< int64_t > cell_to_filtration_map_queries;
  std::vector< int64_t > cell_to_filtration_map_answers;
  heap_column boundary; // Derivative of std::vector
  std::vector< int64_t > boundary_queries;
  write_once_array_of_arrays< int64_t > boundary_answers;

  // Traverse the rows locally by chanks
  // 下からpivotを探して行くからindexが逆順になることに注意。
  for (int64_t chunk_idx = 0; chunk_idx < num_chunks; chunk_idx++) {
    const int64_t chunk_begin = (local_end - 1) - chunk_idx * globals::DIPHA_BLOCK_SIZE;
    const int64_t chunk_end = std::max((local_end - 1) - (chunk_idx + 1) * globals::DIPHA_BLOCK_SIZE,
                                       local_begin);

    dim_queries.clear();

    // 行を下から見ていく。
    for (int64_t idx = chunk_begin; idx >= chunk_end; idx--) {
      int64_t cur_cell = filtration_to_cell_map.get_local_value(idx);
      dim_queries.push_back(cur_cell);
    }
    
    complex.get_global_dims(dim_queries, dim_answers);

    auto dim_answers_iterator = dim_answers.cbegin();
    boundary_queries.clear();
    
    for (int64_t idx = chunk_begin; idx >= chunk_end; idx--) {
      int64_t cur_cell = filtration_to_cell_map.get_local_value(idx);
      if (*dim_answers_iterator++ == dim)
        boundary_queries.push_back(cur_cell);
    }
    
    complex.get_global_boundaries(boundary_queries, boundary_answers);

    dim_answers_iterator = dim_answers.cbegin();
    int64_t boundary_answers_index = 0;
    cell_to_filtration_map_queries.clear();

    for (int64_t idx = chunk_begin; idx >= chunk_end; idx--) {
      if (*dim_answers_iterator++ == dim) {
          boundary.assign(boundary_answers.begin(boundary_answers_index),
                          boundary_answers.end(boundary_answers_index));
          boundary_answers_index++;

          for (const auto& boundary_elem : boundary)
            cell_to_filtration_map_queries.push_back(boundary_elem);
      }
    }
    cell_to_filtration_map.get_global_values(cell_to_filtration_map_queries, cell_to_filtration_map_answers);

    boundary_answers_index = 0;
    dim_answers_iterator = dim_answers.cbegin();
    auto cell_to_filtration_map_answers_iterators = cell_to_filtration_map_answers.cbegin();

    for (int64_t idx = chunk_begin; idx >= chunk_end; idx--) {
      if (*dim_answers_iterator++ == dim) {
        boundary.assign(boundary_answers.begin(boundary_answers_index),
                        boundary_answers.end(boundary_answers_index));
        boundary_answers_index++;

        for (auto& boundary_elem : boundary)
          boundary_elem = *cell_to_filtration_map_answers_iterators++;

        
        std::make_heap(boundary.begin(), boundary.end()); // Heap-structured std::vector 
        unreduced_columns.push(idx, boundary);
      }
    }
  }
}


inline void reduction_kernel(int64_t global_num_cols,
                             flat_column_stack& unreduced_columns,
                             write_once_column_array& reduced_columns)
{
  const int64_t local_begin = get_local_begin(global_num_cols);
  const double reduction_kernel_start = MPI_Wtime();
  const int num_processes = get_num_processes();
  const int rank = get_rank();

  heap_column col;
  for (int round = 0; round < num_processes - rank; round++) {
    flat_column_stack temp_columns;
    
    while (!unreduced_columns.empty()) {
      int64_t index;
      unreduced_columns.pop(index, col);

      // apply twist optimization
      if (round == 0 && !reduced_columns.empty(index))
        col.clear();

      int64_t maximal_index = col.get_max_index();

      // (partially) reduce col using reduced_columns
      if (maximal_index >= local_begin && !reduced_columns.empty(maximal_index))
        {
          while (maximal_index >= local_begin && !reduced_columns.empty(maximal_index))
            {
              col.add_column(reduced_columns.begin(maximal_index), reduced_columns.end(maximal_index));
              maximal_index = col.get_max_index();
            }
          col.prune();
        }

      if (!col.empty())
        {
          // store locally or pass col on?
          if (maximal_index >= local_begin)
            reduced_columns.set(maximal_index, col.begin(), col.end(), index);
          else
            temp_columns.push(index, col);
        }

      unreduced_columns.shrink_to_fit();
    }

    // reverse column stack
    while (!temp_columns.empty())
      {
        int64_t index;
        temp_columns.pop(index, col);
        unreduced_columns.push(index, col);
      }

    // depending on rank: send unreduced_columns to next rank
    std::vector< MPI_Request > requests;
    if (rank > 0)
      non_blocking_send_vector(unreduced_columns.stack_data,
                               rank - 1,
                               MSG_UNREDUCED_COLUMNS,
                               requests);

    // depending on round and rank: receive unreduced_columns from previous rank
    if (round < num_processes - rank - 1)
      receive_vector(temp_columns.stack_data,
                     rank + 1,
                     MSG_UNREDUCED_COLUMNS);

    if (rank > 0)
      MPI_Waitall((int)requests.size(), requests.data(), MPI_STATUSES_IGNORE);

    temp_columns.swap(unreduced_columns);
  }
}


void reduce_columns(const Complex& complex,
                    Distributed_vector<int64_t>& filtration_to_cell_map,
                    Write_once_column_array& reduced_columns)
{
  get_filtration_to_cell_map(complex, filtration_to_cell_map);
  Distributed_vector<int64_t> cell_to_filtration_map;
  get_cell_to_filtration_map(complex.get_num_cells(), filtration_to_cell_map, cell_to_filtration_map);

  reduced_columns.init(complex.get_num_cells());
  const int64_t max_dim = complex.get_max_dim();

  // Compute column reduction for each dimension
  for (int64_t idx = 0; idx < max_dim; idx++) {
    int64_t cur_dim = idx;
    flat_column_stack unreduced_columns;

    generate_unreduced_columns(complex, filtration_to_cell_map, cell_to_filtration_map,
                               cur_dim, unreduced_columns);
    reduction_kernel(complex.get_num_cells(), unreduced_columns, reduced_columns);
    MPI_Barrier(MPI_COMM_WORLD);
  }
}


void compute_persistence(std::string input_filename, std::string output_filename)
{
  Complex complex;
  complex.read_binary_boundary_matrices(input_filename);
  Distributed_vector<int64_t> filtration_to_cell_map;
  Write_once_column_array reduced_columns;
  reduce_columns(complex, filtration_to_cell_map, reduced_columns);
  save(output_filename, complex, filtration_to_cell_map, reduce_columns);
}


int main(int argc, char** argv)
{
  // mandatory MPI initialization
  MPI_Init(&argc, &argv);

  std::string input_filename = argv[0];
  std::string output_filename = argv[1];

  compute_persistence(input_filename, output_filename);
  
  MPI_Finalize();
}
