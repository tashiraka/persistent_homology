import std.stdio, std.string, std.conv, std.algorithm;

version(unittest) {}
 else {
   void main(string[] args)
   {
     auto filtrationFile = args[1];
     auto filtration = readFiltration(filtrationFile);
     foreach (i, f; filtration)
       writeln("Dim ", i, ": Is there a tie? > ", checkTie(f));
   }
 }


bool checkTie(double[] arr)
{
  auto tmp = sort(arr);
  auto prev = double.nan;
  foreach (current; tmp) {
    if (current == prev) return true;
    prev = current;
  }
  return false;
}


double[][] readFiltration(string filename)
{
  double[][] filtration;
  filtration.length = 4;
  
  foreach (line; File(filename).byLine) {
    auto fields = line.to!string.strip.split("\t");
    auto dim = fields.length - 2;
    auto deg = fields[0].to!double;
    //auto pointIDs = fields[1..$].map!(to!ulong).array;

    filtration[dim] ~= deg;
  }
  
  return filtration;
}
