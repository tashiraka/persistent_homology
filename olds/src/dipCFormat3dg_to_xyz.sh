#!/bin/bash


dipCFile=$1
outFile=$2


cat $dipCFile | awk 'BEGIN{OFS="\t"} $0!~/^#/{print($3, $4, $5)}' > $outFile
