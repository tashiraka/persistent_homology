/**
 * Assumption
 * The IDs of points in filtration have been already sorted.
 * Z_2 coefficient
 * If any cycles become boundaries, the cycles will be optimized.
 **/


import std.stdio, std.typecons, std.array, std.conv, std.algorithm, std.string, std.parallelism, std.numeric, std.math, std.typecons;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto filtrationFilePrefix = args[1];
     auto pointsFile = args[2];
     auto outFilePrefix = args[3];

     writeln("Reading points");
     auto points = readPoints(pointsFile);

     writeln("Reading filtration");
     auto T = readFiltration(filtrationFilePrefix, points);

     //writeln("Boundary operator");
     //boundaryOperator(T);

     writeln("Computing intervals");
     computeIntervals(T);

     writeln("Writing voids and intervals");
     writeIntervals(outFilePrefix, T);
     
     writeln("Inserting missing voids");
     auto cycleForest = interpolateMissingVoids(T);

     writeln("Writing voids and intervals with interpolated forest");
     writeIntervals(outFilePrefix, T, cycleForest);
   }
 }


double[][] readPoints(string filename)
{
  double[][] points;
  foreach (line; File(filename).byLine) {
    if (line[0] == '#') continue;
    points ~= line.to!string.strip.split("\t")[0..3].map!(to!double).array;
  }
  return points;
}


CycleTree[][] interpolateMissingVoids(TStructure[][] T)
{
  CycleTree[][] cycleForest;
  cycleForest.length = 4;
  
  foreach(dim, TByDim; T) {
    if (dim == 0) continue;

    // Sort by birth time
    auto L = TByDim.filter!(x => !x.mark).array
      .sort!((a,b) => a.birthTime < b.birthTime).array;

    CycleTree[] cycleTrees;

    // Construct the trees contains the basis
    foreach (t; L) {      
      if (t.birthTime == double.nan)
        throw new Exception("Marked but without birth time.");
      
      auto notFound = true;
      foreach (tree; cycleTrees) {
        if (setDifference(t.addedStandardBasis,
                          tree.root.standardBasis).array is null) {
          tree.insertToLeaf(t);
          notFound = false;
          break;
        }
      }
      if (notFound) {
        cycleTrees
          ~= new CycleTree(dim, new Node(dim, t.addedStandardBasis,
                                         t.birthTime, t.deathTime, null));
      }
    }

    // Insert missing voids
    foreach (tree; cycleTrees) {
      tree.insertMissingVoids;
    }

    cycleForest[dim] = cycleTrees;
  }

  return cycleForest;
}


// double-linked list
class Node
{
  ulong dim;
  ulong[] standardBasis;
  double birthTime, deathTime;
  Node parent;
  Node[] children;
  bool wachedFlag = false;
  
  this (ulong dimension, ulong[] s, double b, double d, Node p)
  {
    dim = dimension;
    standardBasis = s;
    birthTime = b;
    deathTime = d;
    parent = p;
  }

  void addChild(Node c)
  {
    children ~= c;
  }

  Tuple!(ulong[][], double) cycleSimplices(TStructure[][] T)
  {
    ulong[] cycle;
    double interiorVolume = 0;
    foreach (i; standardBasis) {
      cycle = setSymmetricDifference(cycle, T[dim][i].boundaryIDs).array;
      interiorVolume += T[dim][i].volume;
    }
    ulong[][] pids;
    foreach (i ;cycle) {
      pids ~= T[dim - 1][i].pointIDs;
    }
    
    return tuple(pids, interiorVolume);
  }

  Tuple!(ulong[], double) cyclePointIDs(TStructure[][] T)
  {
    ulong[] cycle;
    double interiorVolume = 0;
    foreach (i; standardBasis) {
      cycle = setSymmetricDifference(cycle, T[dim][i].boundaryIDs).array;
      interiorVolume += T[dim][i].volume;      
    }
    ulong[] pids;
    foreach (i ;cycle) {
      pids = merge(pids, T[dim - 1][i].pointIDs).uniq.array;
    }
    
    return tuple(pids, interiorVolume);
  }

  Tuple!(ulong[], double) allPointIDs(TStructure[][] T)
  {
    ulong[] all;
    double interiorVolume = 0;
    foreach (i; standardBasis) { 
      all = merge(all, T[dim][i].pointIDs).uniq.array;
      interiorVolume += T[dim][i].volume;
    }
    return tuple(all, interiorVolume);
  }

  Tuple!(ulong[], double) interiorPointIDs(TStructure[][] T)
  {
    auto all = allPointIDs(T);
    auto cycle = cyclePointIDs(T);
    return tuple(setDifference(all[0], cycle[0]).array, double.nan);
  }
}


class CycleTree
{
  ulong dim;
  Node[] nodes;
  
  this(ulong dimension, Node root)
  {
    dim = dimension;
    nodes = [root];
  }

  Node root() @property {return nodes[0];}

  void insertToLeaf(TStructure t)
  {
    if (t.dim != dim) throw new Exception("Different dimension");
    // The birth time in nodes is in ascending order.
    foreach_reverse (node; nodes) {
      if (setDifference(t.addedStandardBasis,
                        node.standardBasis).array is null) {
        auto n = new Node(dim, t.addedStandardBasis,
                          t.birthTime, t.deathTime, node);
        nodes ~= n;
        node.addChild(n);
        return;
      }
    }
    throw new Exception("The cycle is not included in this tree.");
  }

  void insertMissingVoids()
  {
    Node[] allVoids = [root];
    root.wachedFlag = true;
    
    foreach (i, node; nodes) {
      if (node.wachedFlag) continue;
      allVoids ~= node;
      node.wachedFlag = true;
      auto newBasis = setDifference(node.parent.standardBasis,
                                    node.standardBasis).array;

      // considering the ties
      foreach (sibling; node.parent.children) {
        if (sibling is node) continue;
        assert (!sibling.wachedFlag);
        if (sibling.birthTime == node.birthTime) {
          allVoids ~= sibling;
          sibling.wachedFlag = true;
          newBasis = setDifference(newBasis, sibling.standardBasis).array;
        }
        else {
          break;
        }
      }

      auto newNode = new Node(dim, newBasis, node.birthTime,
                              node.parent.deathTime, node.parent);
      allVoids ~= newNode;
      newNode.wachedFlag = true;

      // Insertion
      node.parent.children
        = node.parent.children.filter!(x => !x.wachedFlag).array;
      node.parent.addChild(newNode);
      foreach (child;
               node.parent.children.filter!(x => !x.wachedFlag)) {
        child.parent = newNode;
      }
    }

    nodes = allVoids;
  }
}

/*
void boundaryOperator(TStructure[][] T)
{
  foreach (dim, TByDim; T)
    if (dim > 0)
      foreach (t; TByDim.parallel)
        foreach (i; 0..(dim+1)) { // dim == points.length - 1
          t.colVec ~= searchIndex(T[dim - 1],
                                  t.pointIDs[0..i] ~ t.pointIDs[(i + 1)..$]);
          t.colVec = sort(t.colVec).array;
          t.boundaryIDs = t.colVec.dup;
        }
}
*/

/*
unittest {
  auto p0 = 0;
  auto p1 = 1;
  auto p2 = 2;
  auto p3 = 3;
  //auto p0 = new Point([0, 0, 0]);
  //auto p1 = new Point([1, 0, 0]);
  //auto p2 = new Point([0, 1, 0]);
  //auto p3 = new Point([0, 0, 1]);

  auto s0 = [new TStructure([p0], 0, 0), new TStructure([p1], 0, 1),
             new TStructure([p2], 0, 2), new TStructure([p3], 0, 3)];
  auto s1 = [new TStructure([p0, p1], 1, 0), new TStructure([p0, p2], 1, 1),
             new TStructure([p0, p3], 1, 2), new TStructure([p1, p2], 2, 3),
             new TStructure([p2, p3], 2, 4), new TStructure([p1, p3], 2, 5)];
  auto s2 = [new TStructure([p0, p1, p2], 2, 0), new TStructure([p0, p1, p3], 2, 1),
             new TStructure([p0, p2, p3], 2, 2), new TStructure([p1, p2, p3], 3, 3)];
  auto s3 = [new TStructure([p0, p1, p2, p3], 3, 0)];
  
  TStructure[][] s = [s0, s1, s2, s3];
  s.boundaryOperator;
  
  assert (s[1][0].colVec == [0, 1], s[1][0].colVec.to!string);
  assert (s[1][1].colVec == [0, 2], s[1][1].colVec.to!string);
  assert (s[1][2].colVec == [0, 3], s[1][2].colVec.to!string);
  assert (s[1][3].colVec == [1, 2], s[1][3].colVec.to!string);
  assert (s[1][4].colVec == [2, 3], s[1][4].colVec.to!string);
  assert (s[1][5].colVec == [1, 3], s[1][5].colVec.to!string);
  assert (s[2][0].colVec == [0, 1, 3], s[2][0].colVec.to!string);
  assert (s[2][1].colVec == [0, 2, 5], s[2][1].colVec.to!string);
  assert (s[2][2].colVec == [1, 2, 4], s[2][2].colVec.to!string);
  assert (s[2][3].colVec == [3, 4, 5], s[2][3].colVec.to!string);
  assert (s[3][0].colVec == [0, 1, 2, 3], s[3][0].colVec.to!string);
}
*/

/*
ulong searchIndex(TStructure[] TByDim, ulong[] targetPointIDs)
{
  foreach (i, t; TByDim) 
    if (t.pointIDs == targetPointIDs) return i;
  throw new Exception("k-simplex's boundary is not included in (k-1)-simplex: "
                      ~ targetPointIDs.to!string);
}
*/

/*
unittest {
  auto p0 = 0;
  auto p1 = 1;
  auto p2 = 2;
  auto p3 = 3;
  //auto p0 = new Point([0, 0, 0]);
  //auto p1 = new Point([1, 0, 0]);
  //auto p2 = new Point([0, 1, 0]);
  //auto p3 = new Point([0, 0, 1]);

  auto s0 = [new TStructure([p0], 0, 0), new TStructure([p1], 0, 1),
             new TStructure([p2], 0, 2), new TStructure([p3], 0, 3)];
  auto s1 = [new TStructure([p0, p1], 1, 0), new TStructure([p0, p2], 1, 1),
             new TStructure([p0, p3], 1, 2), new TStructure([p1, p2], 2, 3),
             new TStructure([p2, p3], 2, 4), new TStructure([p1, p3], 2, 5)];
  auto s2 = [new TStructure([p0, p1, p2], 2, 0), new TStructure([p0, p1, p3], 2, 1),
             new TStructure([p0, p2, p3], 2, 2), new TStructure([p1, p2, p3], 3, 3)];
  auto s3 = [new TStructure([p0, p1, p2, p3], 3, 0)];

  assert (searchIndex(s0, [p0]) == 0, searchIndex(s0, [p0]).to!string);
  assert (searchIndex(s0, [p1]) == 1, searchIndex(s0, [p1]).to!string);
  assert (searchIndex(s0, [p2]) == 2, searchIndex(s0, [p2]).to!string);
  assert (searchIndex(s0, [p3]) == 3, searchIndex(s0, [p3]).to!string);
  assert (searchIndex(s1, [p0, p1]) == 0, searchIndex(s1, [p0, p1]).to!string);
  assert (searchIndex(s1, [p0, p2]) == 1, searchIndex(s1, [p0, p2]).to!string);
  assert (searchIndex(s1, [p0, p3]) == 2, searchIndex(s1, [p0, p3]).to!string);
  assert (searchIndex(s1, [p1, p2]) == 3, searchIndex(s1, [p1, p2]).to!string);
  assert (searchIndex(s1, [p2, p3]) == 4, searchIndex(s1, [p2, p3]).to!string);
  assert (searchIndex(s1, [p1, p3]) == 5, searchIndex(s1, [p1, p3]).to!string);
  assert (searchIndex(s2, [p0, p1, p2]) == 0, searchIndex(s2, [p0, p1, p2]).to!string);
  assert (searchIndex(s2, [p0, p1, p3]) == 1, searchIndex(s2, [p0, p1, p3]).to!string);
  assert (searchIndex(s2, [p0, p2, p3]) == 2, searchIndex(s2, [p0, p2, p3]).to!string);
  assert (searchIndex(s2, [p1, p2, p3]) == 3, searchIndex(s2, [p1, p2, p3]).to!string);
}
*/

void computeIntervals(TStructure[][] T)
{
  writeln("Reduce matrix");
  matrixReduction(T);

  writeln("Localize cycles...");
  foreach (j, t; T[3]) 
    t.optimizeCycle(j, T[3], T);

  // cannot optimized
  foreach (dim, TByDim; T) {
    foreach (j, t; TByDim) {
      if (dim != 0 && t.mark && t.isEmpty) {
        assert (t.birthTime == double.nan);
        //throw new Exception("Not dead cycle");
        t.birthTime = t.deg;
        t.deathTime = double.infinity;
      }
    }
  }
}


void matrixReduction(TStructure[][] T)
{
  foreach(dim, TByDim; T) {
    foreach(j, t; TByDim) {
      if(dim != 0) t.removePivotRow(T[dim - 1], T[dim]);

      if (dim == 0 && t.colVec !is null)
        throw new Exception("ERROR: dim == 0 && d !is null");

      if (t.colVec is null) t.mark = true;
      else {
        auto i = maxIndex(t.colVec);
        T[dim - 1][i].pivotColID = j;

        t.birthTime = T[dim - 1][i].deg;
        t.deathTime = t.deg;        
      }
    }
  }
}


ulong maxIndex(ulong[] d)
{
  return d[$-1]; // d is sorted in this program.
  //return d.reduce!max;
}


class TStructure
{
  // When row of (k+1)-th boundary matrix
  ulong pivotColID = ulong.max; //Let this T be in C_{k-1}. j is C_k ID corresponding to this.
 
  // When column of k-th boundary matrix
  bool mark = false; //redued column vecter is 0.
  ulong[] addedStandardBasis = null; //Trace of column adding
  ulong[] colVec = null; //Column vector, always sorted

  // Original standard basis
  ulong[] pointIDs = null;
  ulong[] boundaryIDs = null;
  ulong standardBasisID = ulong.max;
  double deg = double.nan;
  ulong dim() @property {return pointIDs.length - 1;}
  double volume = double.nan;

  // Corresponding persistence intervals
  double birthTime = double.nan;
  double deathTime = double.nan;
  
  this(ulong[] pids, double dg, ulong selfID, double[][] points, ulong[] boundaries)
  {
    pointIDs = pids;
    deg = dg;
    standardBasisID = selfID;
    addedStandardBasis = [selfID];
    if (dim == 3)
      volume = simplexVolume(pids.map!(x => points[x]).array);

    colVec = boundaries.dup;
    boundaryIDs = boundaries.dup;
  }


private:
  double simplexVolume(double[][] points)
  {
    if (dim == 0) return 0;
    //else if (dim == 1) return euclideanDistance(points[0], points[1]);
    //else if (dim == 2) return triangleArea(points[0], points[1], points[2]);
    else if (dim == 3) return tetrahedronVolume(points[0], points[1], points[2], points[3]);
    else throw new Exception("dimmension > 3 !!!");
  }

  /*
  double triangleArea(double[] p1, double[] p2, double[] p3)
  {
    auto u = p2.dup;
    auto v = p3.dup;
    u[] -= p1[];
    v[] -= p1[];

    auto areaVec = outerProduct(u, v);
    return euclideanDistance(areaVec, areaVec) / 2.0;
  }
  */
  double tetrahedronVolume(double[] p1, double[] p2, double[] p3, double[] p4)
  {
    auto u = p2.dup;
    auto v = p3.dup;
    auto w = p4.dup;
    u[] -= p1[];
    v[] -= p1[];
    w[] -= p1[];

    return tripleProduct(u, v, w).abs / 6.0;
  }

  double[] outerProduct(double[] u, double[] v)
  {
    return [u[1] * v[2] - u[2] * v[1],
            u[2] * v[0] - u[0] * v[2],
            u[0] * v[1] - u[1] * v[0]];
  }
    
  double tripleProduct(double[] u, double[] v, double[] w)
  {
    auto outer = outerProduct(u, v);
    return dotProduct(outer, w);
  }
  
public:
  
  bool isEmpty()
  {
    return pivotColID == ulong.max;
  }
  
  // This method keeps colVec sorted.
  void removePivotRow(TStructure[] TRows, TStructure[] TCols)
  {
    // Remove unmarked terms in boundary of this base
    colVec = colVec.filter!(x => TRows[x].mark).array; // keeping sorted
    assert (colVec.isSorted);

    // rowID: candidate of pivot
    // TColmns[TRow[rowID].pivotColID]: column corresponds to rowID by pivot
    while (colVec !is null) {
      auto rowID = maxIndex(colVec);

      if (TRows[rowID].isEmpty) break;

      addColumn(TCols[TRows[rowID].pivotColID].colVec); // keeping sorted
      assert (colVec.isSorted);

      // Column adding must be traced
      addedStandardBasis
        = setSymmetricDifference(addedStandardBasis,
                                 TCols[TRows[rowID].pivotColID].addedStandardBasis).array;
    }
  }

  /**
   * Column adding operation of Z_2-coefficient module 
   * eqaul to symmetric difference (in terms of logical operation, XOR).
   */
  void addColumn(ulong[] c)
  {
    // The two ranges, facet and addinColumn are assumed
    // to be sorted, and the output is also sorted.
    assert (colVec.isSorted);
    assert (c.isSorted);
    colVec = setSymmetricDifference(colVec, c).array;
  }

  
  void optimizeCycle(ulong j, TStructure[] T_k, TStructure[][] T)
  {
    assert (birthTime != double.nan);
    assert (addedStandardBasis.isSorted);

    bool firstFlag = true;
    foreach (l, t; T_k) {
      if (this is t) continue;

      if (deathTime < t.deathTime) break;

      assert (t.birthTime != double.nan);

      // only optimizing 3-simplexes in R^3, so there is no cycle.
      if (t.mark)
        throw new Exception("There is a cycle in optimization.");
      
      if (t.birthTime <= birthTime &&
          t.deathTime <= deathTime) {
        assert (t.addedStandardBasis.isSorted);
        auto intersection
          = setIntersection(addedStandardBasis,
                            t.addedStandardBasis).array;

        if (deathTime == t.deathTime) {
          if (intersection != null) {
          }
        }
        
        // keep sorted
        if (t.birthTime == birthTime &&
            t.deathTime == deathTime &&
            intersection == addedStandardBasis) {
          // This is included in t.
          // Calculating set difference before optimization of t.
          t.addedStandardBasis
            = setDifference(t.addedStandardBasis,
                            intersection).array;
        }
        else if (intersection != addedStandardBasis) {
          if (intersection.canFind(standardBasisID)) {
            throw new Exception("inclusion of standard basis");
          }
          addedStandardBasis
            = setDifference(addedStandardBasis,
                            intersection).array;
        }
      }
    }    
  }

  Tuple!(ulong[][], double) cycleSimplices(TStructure[][] T)
  {
    ulong[] cycle;
    double interiorVolume = 0;
    foreach (i; addedStandardBasis) {
      cycle = setSymmetricDifference(cycle, T[dim][i].boundaryIDs).array;
      interiorVolume += T[dim][i].volume;
    }

    ulong[][] pids;
    foreach (i ;cycle) {
      pids ~= T[dim - 1][i].pointIDs;
    }
    
    return tuple(pids, interiorVolume);
  }

  
  Tuple!(ulong[], double) cyclePointIDs(TStructure[][] T)
  {
    ulong[] cycle;
    double interiorVolume = 0;
    foreach (i; addedStandardBasis) {
      cycle = setSymmetricDifference(cycle, T[dim][i].boundaryIDs).array;
      interiorVolume += T[dim][i].volume;
    }

    ulong[] pids;
    foreach (i ;cycle) {
      pids = merge(pids, T[dim - 1][i].pointIDs).uniq.array;
    }
    
    return tuple(pids, interiorVolume);
  }

  Tuple!(ulong[], double) allPointIDs(TStructure[][] T)
  {
    ulong[] all;
    double interiorVolume = 0;
    foreach (i; addedStandardBasis) { 
      all = merge(all, T[dim][i].pointIDs).uniq.array;
      interiorVolume += T[dim][i].volume;
    }
    return tuple(all, interiorVolume);
  }

  Tuple!(ulong[], double) interiorPointIDs(TStructure[][] T)
  {
    auto all = allPointIDs(T);
    auto cycle = cyclePointIDs(T);
    return tuple(setDifference(all[0], cycle[0]).array, double.nan);
  }
}


void writeIntervals(string filenamePrefix,
                    TStructure[][] T, CycleTree[][] cycleForest)
{
  File fout;
  /*
  // Write the simplices in a cycle
  fout = File(filenamePrefix ~ "_cycleSimplices", "w");
  foreach (dim, TByDim; T) {
    if (dim < 2) continue;
    foreach (t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");
          
      fout.write(t.dim - 1, "\t", t.birthTime, "\t", t.deathTime, "\t");
      auto cycleSimplices = t.cycleSimplices(T);
      foreach (i, simplex; cycleSimplices) {
        foreach (j, id; simplex) {
          fout.write(id);
          if (j < simplex.length - 1) fout.write(" "); 
        }
        if (i < cycleSimplices.length - 1) fout.write("\t");
      }
      fout.writeln;
    }
  }

  // Write the points in a cycle
  fout = File(filenamePrefix ~ "_cycles", "w");
  foreach (dim, TByDim; T) {
    if (dim < 2) continue;
    foreach (t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");
          
      fout.write(t.dim - 1, "\t", t.birthTime, "\t", t.deathTime);
      foreach (i; t.cyclePointIDs(T)) fout.write("\t", i);
      fout.writeln;          
    }
  }

  // Write all points on and in a cycle
  fout = File(filenamePrefix ~ "_all_points", "w");
  foreach (dim, TByDim; T) {
    if (dim < 2) continue;
    foreach (t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");

      fout.write(t.dim - 1, "\t", t.birthTime, "\t", t.deathTime);
      foreach (i; t.allPointIDs(T)) fout.write("\t", i);
      fout.writeln;    
    }
  }

  //Write the points in a cycle
  fout = File(filenamePrefix ~ "_interior_points", "w");
  foreach (dim, TByDim; T) {
    if (dim < 2) continue;
    foreach (t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");

      fout.write(t.dim - 1, "\t", t.birthTime, "\t", t.deathTime);
      foreach (i; t.interiorPointIDs(T)) fout.write("\t", i);
      fout.writeln;

    }
  }
  */
  
  // All voids and persistence intervals
  // Write the simplices in a cycle
  fout = File(filenamePrefix ~ "_voidSimplices", "w");  
  foreach (dim, cycleTrees; cycleForest) {
    if (dim < 2) continue;
    foreach (tree; cycleTrees) {
      foreach (node; tree.nodes) { // all nodes are not marked.        
        fout.write(dim - 1, "\t", node.birthTime, "\t", node.deathTime);

        auto cycleSimplices = node.cycleSimplices(T);

        fout.write("\t", cycleSimplices[1]);

        foreach (i, simplex; cycleSimplices[0]) {
          fout.write("\t");
          foreach (j, id; simplex) {
            fout.write(id);
            if (j < simplex.length - 1) fout.write(" "); 
          }
        }
        fout.writeln;
      }
    }
  }
            
  // Write the points in a cycle
  fout = File(filenamePrefix ~ "_voids", "w");
  foreach (dim, cycleTrees; cycleForest) {
    if (dim < 2) continue;
    foreach (tree; cycleTrees) {
      foreach (node; tree.nodes) { // all nodes are not marked.
        fout.write(node.dim - 1, "\t", node.birthTime, "\t", node.deathTime);
        auto cyclePoints = node.cyclePointIDs(T);
        fout.write("\t", cyclePoints[1]);
        foreach (i; cyclePoints[0]) fout.write("\t", i);
        fout.writeln;          
      }
    }
  }
  
  // Write all points on and in a cycle
  fout = File(filenamePrefix ~ "_all_points_in_void", "w");
  foreach (dim, cycleTrees; cycleForest) {
    if (dim < 2) continue;
    foreach (tree; cycleTrees) {
      foreach (node; tree.nodes) { // all nodes are not marked.
        fout.write(node.dim - 1, "\t", node.birthTime, "\t", node.deathTime);
        auto allPoints = node.allPointIDs(T);
        fout.write("\t", allPoints[1]);
        foreach (i; allPoints[0]) fout.write("\t", i);
        fout.writeln;    
      }
    }
  }

  //Write the points in a cycle
  fout = File(filenamePrefix ~ "_interior_points_in_void", "w");
  foreach (dim, cycleTrees; cycleForest) {
    if (dim < 2) continue;
    foreach (tree; cycleTrees) {
      foreach (node; tree.nodes) { // all nodes are not marked.
        fout.write(node.dim - 1, "\t", node.birthTime, "\t", node.deathTime);
        auto interiorPoints = node.interiorPointIDs(T);
        fout.write("\t", interiorPoints[1]);
        foreach (i; interiorPoints[0]) fout.write("\t", i);
        fout.writeln;
      }
    }
  }
}


void writeIntervals(string filenamePrefix, TStructure[][] T)
{
  File fout;
  
  // Write the simplices in a cycle
  fout = File(filenamePrefix ~ "_nointerp_voidSimplices", "w");
  foreach (dim, TByDim; T) {
    if (dim < 2) continue;
    foreach (t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");
          
      fout.write(t.dim - 1, "\t", t.birthTime, "\t", t.deathTime, "\t");

      auto cycleSimplices = t.cycleSimplices(T);
      fout.write("\t", cycleSimplices[1]);

      foreach (i, simplex; cycleSimplices[0]) {
        foreach (j, id; simplex) {
          fout.write(id);
          if (j < simplex.length - 1) fout.write(" "); 
        }
        if (i < cycleSimplices.length - 1) fout.write("\t");
      }
      fout.writeln;
    }
  }

  // Write the points in a cycle
  fout = File(filenamePrefix ~ "_nointerp_voids", "w");
  foreach (dim, TByDim; T) {
    if (dim < 2) continue;
    foreach (t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");
          
      fout.write(t.dim - 1, "\t", t.birthTime, "\t", t.deathTime);

      auto cyclePoints = t.cyclePointIDs(T);
      fout.write("\t", cyclePoints[1]);
      foreach (i; cyclePoints[0]) fout.write("\t", i);
      fout.writeln;          
    }
  }

  // Write all points on and in a cycle
  fout = File(filenamePrefix ~ "_nointerp_all_points_in_voids", "w");
  foreach (dim, TByDim; T) {
    if (dim < 2) continue;
    foreach (t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");

      fout.write(t.dim - 1, "\t", t.birthTime, "\t", t.deathTime);
      auto allPoints = t.allPointIDs(T);
      fout.write("\t", allPoints[1]);
      foreach (i; allPoints[0]) fout.write("\t", i);
      fout.writeln;    
    }
  }

  //Write the points in a cycle
  fout = File(filenamePrefix ~ "_nointerp_interior_points", "w");
  foreach (dim, TByDim; T) {
    if (dim < 2) continue;
    foreach (t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");

      auto interiorPoints = t.interiorPointIDs(T);
      fout.write("\t", interiorPoints[1]);
      foreach (i; interiorPoints[0]) fout.write("\t", i);
      fout.writeln;
    }
  }
}




TStructure[][] readFiltration(string filenamePrefix, double[][] points)
{
  auto app = new Appender!(TStructure[])[](4);
  foreach (dim; 0..4) {
    auto filename = filenamePrefix ~ "_" ~ dim.to!string;

    ulong degOrder = 0;


    foreach (line; File(filename).byLine) {
      // 0: degree
      // [1, dim + 2): point IDs of a simplex
      // [dim + 2, the last of fields}: simplex IDs of boundaries
      auto fields = line.to!string.strip.split("\t");
      auto deg = fields[0].to!double;
      auto pointIDs = fields[1..(dim + 2)].map!(x => x.to!ulong).array;
      assert(pointIDs.length == dim + 1);
      auto boundaryIDs = fields[(dim + 2)..$].map!(x => x.to!ulong).array;
      
      app[dim].put(new TStructure(pointIDs, deg, degOrder, points, boundaryIDs));
      degOrder++;
    }
  }

  return [app[0].data, app[1].data, app[2].data, app[3].data];
}
