/**A
 * Assumption
 * The IDs of points in filtration have been already sorted.
 * Z_2 coefficient
 * If any cycles become boundaries, the cycles will be optimized.
 **/


import std.stdio, std.typecons, std.array, std.conv, std.algorithm, std.string, std.parallelism, std.numeric, std.math, std.typecons;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto filtrationFile = args[1];
     auto pointsFile = args[2];
     auto outFilePrefix = args[3];

     writeln("Reading points");
     auto points = readPoints(pointsFile);

     writeln("Reading filtration");
     auto T = readFiltration(filtrationFile, points);

     /*
     foreach (TByDim; T) {
       writeln(TByDim.isSorted!((a,b) => a.deg < b.deg));
     }

     writeln(T[3][10677].deg);
     writeln(T[3][10678].deg);
     */
     writeln("Boundary operator");
     boundaryOperator(T);

     writeln("Computing intervals");
     computeIntervals(T);

     writeln("Writing voids and intervals");
     writeIntervals(outFilePrefix, T);
   }
 }


void boundaryOperator(TStructure[][] T)
{
  foreach (dim, TByDim; T)
    if (dim > 0)
      foreach (t; TByDim.parallel)
        foreach (i; 0..(dim+1)) { // dim == points.length - 1
          t.colVec ~= searchIndex(T[dim - 1],
                                  t.pointIDs[0..i] ~ t.pointIDs[(i + 1)..$]);
          t.colVec = sort(t.colVec).array;
          t.boundaryIDs = t.colVec.dup;
        }
}


ulong searchIndex(TStructure[] TByDim, ulong[] targetPointIDs)
{
  foreach (i, t; TByDim) 
    if (t.pointIDs == targetPointIDs) return i;
  throw new Exception("k-simplex's boundary is not included in (k-1)-simplex: "
                      ~ targetPointIDs.to!string);
}


void computeIntervals(TStructure[][] T)
{
  foreach(dim, TByDim; T) {
    foreach(j, t; TByDim) {
      
      if(dim != 0) t.removePivotRow(T[dim - 1], T[dim]);

      if (dim == 0 && t.colVec !is null)
        throw new Exception("ERROR: dim == 0 && d !is null");

      if(t.colVec is null) t.mark = true;
      else {
        auto i = maxIndex(t.colVec);
        T[dim - 1][i].pivotColID = j;

        t.birthTime = T[dim - 1][i].deg;
        t.deathTime = t.deg;

        /*
        if (dim == 3 && j == 10678) {
          writeln(t.deg);
        }
        */
        //if (dim == 3) // Optimizing only 2-cycles in R^3.
        //  t.optimizeCycle(j, TByDim, T);
      }
    }
  }

  /*
  writeln("Pre-check");
  foreach (j, t; T[3]) {
    auto dim = 3;
    ulong[] tmp = [];
    foreach(e; t.addedStandardBasis) {
      auto bid = T[dim][e].boundaryIDs;
      tmp = setSymmetricDifference(tmp, bid).array;
    }
    auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
    auto currentDeathTime = t.addedStandardBasis.map!(x => T[dim][x].deg).reduce!max;
    auto optSuccessFlag = (t.birthTime == currentBirthTime) && (t.deathTime == currentDeathTime);
    if (!optSuccessFlag) {
      writeln(dim, " ", j, " ", t.birthTime, " ", t.deathTime, " ", currentBirthTime, " ", currentDeathTime);
      //throw new Exception("Before optimization, the birth or death time is different");
    }
  }
  writeln("Pre-end");
  */
  /*
  foreach (j, t; T[3]) {
    if (10677 <= j && j <= 10678) {
      auto dim = 3;
      ulong[] tmp = [];
      foreach(e; t.addedStandardBasis) {
        auto bid = T[dim][e].boundaryIDs;
        tmp = setSymmetricDifference(tmp, bid).array;
      }
      auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
      writeln(j, " added ", t.addedStandardBasis, " ", tmp.map!(x => T[dim - 1][x].deg),
              " ", t.birthTime, " ", currentBirthTime);
      
      tmp = [];
      foreach(e; T[3][10678].addedStandardBasis) {
        auto bid = T[dim][e].boundaryIDs;
        tmp = setSymmetricDifference(tmp, bid).array;
      }
      currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
      writeln(10678, " added ", T[3][10678].addedStandardBasis, " ", tmp.map!(x => T[dim - 1][x].deg),
              " ", T[3][10678].birthTime, " ", currentBirthTime);
    }
  }
  writeln("-------------");
  */

  // optimization after matrix reduction
  //writeln("opt");
  foreach (j, t; T[3]) {
    /*
    if (10677 <= j && j <= 10678) {
      auto dim = 3;
      ulong[] tmp = [];
      foreach(e; t.addedStandardBasis) {
        auto bid = T[dim][e].boundaryIDs;
        tmp = setSymmetricDifference(tmp, bid).array;
      }
      auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
      writeln(j, " added ", t.addedStandardBasis, " ", tmp.map!(x => T[dim - 1][x].deg),
              " ", t.birthTime, " ", currentBirthTime);

      tmp = [];
      foreach(e; T[3][10678].addedStandardBasis) {
        auto bid = T[dim][e].boundaryIDs;
        tmp = setSymmetricDifference(tmp, bid).array;
      }
      currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
      writeln(10678, " added ", T[3][10678].addedStandardBasis, " ", tmp.map!(x => T[dim - 1][x].deg),
              " ", T[3][10678].birthTime, " ", currentBirthTime);
    }
    */
    t.optimizeCycle(j, T[3], T);
    /*
    if (10677 <= j && j <= 10678) {
      auto dim = 3;
      ulong[] tmp = [];
      foreach(e; t.addedStandardBasis) {
        auto bid = T[dim][e].boundaryIDs;
        tmp = setSymmetricDifference(tmp, bid).array;
      }
      auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
      writeln(j, " opt-added ", t.addedStandardBasis, " ", tmp.map!(x => T[dim - 1][x].deg),
              " ", t.birthTime, " ", currentBirthTime);

      tmp = [];
      foreach(e; T[3][10678].addedStandardBasis) {
        auto bid = T[dim][e].boundaryIDs;
        tmp = setSymmetricDifference(tmp, bid).array;
      }
      currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
      writeln(10678, " opt-added ", T[3][10678].addedStandardBasis, " ", tmp.map!(x => T[dim - 1][x].deg),
              " ", T[3][10678].birthTime, " ", currentBirthTime);
    
     }

   /*
    foreach (jj, tt; T[3]) {
      if (tt.birthTime == 102.843 && tt.deathTime == 102.853 && j == 10677) {
        auto dim = 3;
        ulong[] tmp = [];
        foreach(e; tt.addedStandardBasis) {
          auto bid = T[dim][e].boundaryIDs;
          tmp = setSymmetricDifference(tmp, bid).array;
        }
        auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
        auto currentDeathTime = tt.addedStandardBasis.map!(x => T[dim][x].deg).reduce!max;
        auto optSuccessFlag = (tt.birthTime == currentBirthTime) && (tt.deathTime == currentDeathTime);
        if (!optSuccessFlag) {
          writeln(j, "-th opt ", dim, " ", jj, " ", t.birthTime, " ", t.deathTime, " ", tt.birthTime, " ", tt.deathTime, " ", currentBirthTime, " ", currentDeathTime);
          //throw new Exception("Before optimization, the birth or death time is different");
        }
      }
    }
    */
  }
  //writeln("opt");
  
  /*
  foreach (j, t; T[3]) {
    auto dim = 3;
    ulong[] tmp = [];
    foreach(e; t.addedStandardBasis) {
      auto bid = T[dim][e].boundaryIDs;
      tmp = setSymmetricDifference(tmp, bid).array;
    }
    
    auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
    auto currentDeathTime = t.addedStandardBasis.map!(x => T[dim][x].deg).reduce!max;
    auto optSuccessFlag = (t.birthTime == currentBirthTime) && (t.deathTime == currentDeathTime);
    if (!optSuccessFlag) {
      writeln(dim, " ", j, " ", t.birthTime, " ", t.deathTime, " ", currentBirthTime, " ", currentDeathTime);
      
      ulong[] all;
      foreach (g; T[dim][0..j+1]) {
        all = merge(all, g.addedStandardBasis).array;
      }
      
      if (all.length == j + 1)
        throw new Exception("Kesson");
      
      //throw new Exception("Before optimization, the birth or death time is different");
    }
  }
  */
  // This program assumes that all cycles become dead.
  foreach (dim, TByDim; T) 
    foreach (j, t; TByDim)
      if (dim != 0 && t.mark && t.isEmpty) {
        throw new Exception("Not dead cycle");

        //if (t.birthTime != double.nan)
        //  throw new Exception("t.birthTime == double.nan");
        //t.birthTime = t.deg;
        //t.deathTime = double.infinity;
      }
}


ulong maxIndex(ulong[] d)
{
  return d[$-1]; // d is sorted in this program.
  //return d.reduce!max;
}


//---------------------------------------------------------------------------
// DATA STRUCTURE
//---------------------------------------------------------------------------

class TStructure
{
  // When row of (k+1)-th boundary matrix
  ulong pivotColID = ulong.max; //Let this T be in C_{k-1}. j is C_k ID corresponding to this.
 
  // When column of k-th boundary matrix
  bool mark = false; //redued column vecter is 0.
  ulong[] addedStandardBasis = null; //Trace of column adding
  ulong[] colVec = null; //Column vector, always sorted

  // Original standard basis
  ulong[] pointIDs = null;
  ulong[] boundaryIDs = null;
  ulong standardBasisID = ulong.max;
  double deg = double.nan;
  ulong dim() @property {return pointIDs.length - 1;}
  double volume = double.nan;

  // Corresponding persistence intervals
  double birthTime = double.nan;
  double deathTime = double.nan;
  
  this(ulong[] pids, double dg, ulong selfID, double[][] points)
  {
    pointIDs = pids;
    deg = dg;
    standardBasisID = selfID;
    addedStandardBasis = [selfID];
    if (dim == 3) {
      volume = simplexVolume(pids.map!(x => points[x]).array);
      //writeln(volume);
    }
  }

private:
  double simplexVolume(double[][] points)
  {
    // This program uses volume of tetrahedron
    if (dim == 3) return tetrahedronVolume(points[0], points[1], points[2], points[3]);
    else throw new Exception("Callculating the volume of the simplex with dimension != 3.");
    /*
    if (dim == 0) return 0;
    else if (dim == 1) return euclideanDistance(points[0], points[1]);
    else if (dim == 2) return triangleArea(points[0], points[1], points[2]);
    else if (dim == 3) return tetrahedronVolume(points[0], points[1], points[2], points[3]);
    else throw new Exception("dimmension > 3 !!!");
    */
  }

  /*
  double triangleArea(double[] p1, double[] p2, double[] p3)
  {
    auto u = p2.dup;
    auto v = p3.dup;
    u[] -= p1[];
    v[] -= p1[];

    auto areaVec = outerProduct(u, v);
    return euclideanDistance(areaVec, areaVec) / 2.0;
  }
  */
  
  double tetrahedronVolume(double[] p1, double[] p2, double[] p3, double[] p4)
  {
    auto u = p2.dup;
    auto v = p3.dup;
    auto w = p4.dup;
    u[] -= p1[];
    v[] -= p1[];
    w[] -= p1[];

    return tripleProduct(u, v, w).abs / 6.0;
  }

  
  double[] outerProduct(double[] u, double[] v)
  {
    return [u[1] * v[2] - u[2] * v[1],
            u[2] * v[0] - u[0] * v[2],
            u[0] * v[1] - u[1] * v[0]];
  }
    
  double tripleProduct(double[] u, double[] v, double[] w)
  {
    auto outer = outerProduct(u, v);
    return dotProduct(outer, w);
  }
  
public:
  
  bool isEmpty()
  {
    return pivotColID == ulong.max;
  }
  
  // This method keeps colVec sorted.
  void removePivotRow(TStructure[] TRows, TStructure[] TCols)
  {
    // Remove unmarked terms in boundary of this base
    colVec = colVec.filter!(x => TRows[x].mark).array; // keeping sorted
    assert (colVec.isSorted);

    // rowID: candidate of pivot
    // TColmns[TRow[rowID].pivotColID]: column corresponds to rowID by pivot
    while (colVec !is null) {
      auto rowID = maxIndex(colVec);

      if (TRows[rowID].isEmpty) break;

      columnAdding(TCols[TRows[rowID].pivotColID].colVec); // keeping sorted
      assert (colVec.isSorted);

      // Column adding must be traced
      addedStandardBasis
        = setSymmetricDifference(addedStandardBasis,
                                 TCols[TRows[rowID].pivotColID].addedStandardBasis).array;
    }
  }

  /**
   * Column adding operation of Z_2-coefficient module 
   * eqaul to symmetric difference (in terms of logical operation, XOR).
   */
  void columnAdding(ulong[] addingColumn)
  {
    // The two ranges, facet and addinColumn are assumed
    // to be sorted, and the output is also sorted.
    assert (colVec.isSorted);
    assert (addingColumn.isSorted);
    colVec = setSymmetricDifference(colVec, addingColumn).array;
  }

  void optimizeCycle(ulong j, TStructure[] T_k, TStructure[][] T)
  {
    /*
    ulong[] tmp = [];
    foreach(e; addedStandardBasis) {
      auto bid = T[dim][e].boundaryIDs;
      tmp = setSymmetricDifference(tmp, bid).array;
    }    
    auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
    auto currentDeathTime = addedStandardBasis.map!(x => T[dim][x].deg).reduce!max;
    auto optSuccessFlag = (birthTime == currentBirthTime) && (deathTime == currentDeathTime);
    if (!optSuccessFlag) {
      writeln("???Before??? ", dim, " ", j, " ", birthTime, " ", deathTime, " ", currentBirthTime, " ", currentDeathTime);      
      //throw new Exception("Before optimization, the birth or death time is different");
    }
    */
    assert (birthTime != double.nan);
    assert (addedStandardBasis.isSorted);

    bool firstFlag = true;
    foreach (l, t; T_k) {
      if (this is t) continue;

      if (deathTime < t.deathTime) break;

      assert (t.birthTime != double.nan);

      // only optimizing 3-simplexes in R^3, so there is no cycle.
      if (t.mark)
        throw new Exception("There is a cycle in optimization.");
      
      if (t.birthTime <= birthTime &&
          t.deathTime <= deathTime) {
        assert (t.addedStandardBasis.isSorted);
        auto intersection
          = setIntersection(addedStandardBasis,
                            t.addedStandardBasis).array;

        if (deathTime == t.deathTime) {
          if (intersection != null) {
            //writeln("test");
          }
        }
        
        // keep sorted
        if (t.birthTime == birthTime &&
            t.deathTime == deathTime &&
            intersection == addedStandardBasis) {
          // This is included in t.
          // Calculating set difference before optimization of t.
          
          t.addedStandardBasis
            = setDifference(t.addedStandardBasis,
                            intersection).array;
        }
        else if (intersection != addedStandardBasis) {
          if (intersection.canFind(standardBasisID)) {
            throw new Exception("inclusion of standard basis");
          }
          addedStandardBasis
            = setDifference(addedStandardBasis,
                            intersection).array;
        }
      }
      /*
      if (t.birthTime == 137.664 && t.deathTime == 143.526) {
        ulong[] tmp = [];
        foreach(e; t.addedStandardBasis) {
          auto bid = T[dim][e].boundaryIDs;
          tmp = setSymmetricDifference(tmp, bid).array;
        }
        
        auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
        auto currentDeathTime = t.addedStandardBasis.map!(x => T[dim][x].deg).reduce!max;
        auto optSuccessFlag = (t.birthTime == currentBirthTime) &&
          (t.deathTime == currentDeathTime);
        if (!optSuccessFlag) {
          writeln(j, " ", birthTime, " ", deathTime);
          throw new Exception("Ha?");
        }
      }
      */
    }
  }
  
  Tuple!(ulong[], double) cyclePointIDs(TStructure[][] T)
  {
    ulong[] cycle;
    double interiorVolume = 0;

    foreach (i; addedStandardBasis) {
      cycle = setSymmetricDifference(cycle, T[dim][i].boundaryIDs).array;
      interiorVolume += T[dim][i].volume;      
    }
    
    ulong[] pids;

    foreach (i ;cycle) {
      pids = merge(pids, T[dim - 1][i].pointIDs).uniq.array;
    }

    return tuple(pids, interiorVolume);
  }
}


string checkIntersection(ulong jj, TStructure t, TStructure[] T)
{
  foreach (j, elem; T) {
    if (t is elem) continue;
    if (t.deathTime < elem.deathTime) break;

    if (t.mark) throw new Exception("Why here cycle?");

    /*
    if (t.mark && t.deathTime <= elem.deathTime) {
      if (setInclusion(t.addedStandardBasis, elem.addedStandardBasis)) {
        return "included";
      }
      if (setInclusion(elem.addedStandardBasis, t.addedStandardBasis)) {
        return "including";
      }
      auto intersection = setIntersection(t.addedStandardBasis,
                                          elem.addedStandardBasis);
      if (!intersection.empty) return "intersect";
    }
    */

    if (t.birthTime >= elem.birthTime && t.deathTime >= elem.deathTime) {
      if (setInclusion(t.addedStandardBasis, elem.addedStandardBasis)) {
        writeln(j, " ", elem.birthTime, " ", elem.deathTime, " ", elem.addedStandardBasis);
        writeln(jj, " ", t.birthTime, " ", t.deathTime, " ", t.addedStandardBasis);

        if (t.birthTime > elem.birthTime) {
          writeln("included but OK");
          return "none";
        }
        else {
          writeln("included");
          return "included";
        }
      }
      if (setInclusion(elem.addedStandardBasis, t.addedStandardBasis)) {
        
        writeln(j, " ", elem.birthTime, " ", elem.deathTime, " ", elem.addedStandardBasis);
        writeln(jj, " ", t.birthTime, " ", t.deathTime, " ", t.addedStandardBasis);
        writeln("including");
        return "including";
      }
      
      auto intersection = setIntersection(t.addedStandardBasis,
                                          elem.addedStandardBasis);
      if (!intersection.empty) {
        writeln(j, " ", elem.birthTime, " ", elem.deathTime, " ", elem.addedStandardBasis);
        writeln(jj, " ", t.birthTime, " ", t.deathTime, " ", t.addedStandardBasis);
        writeln("intersect");
        return "intersect";
      }
    }
  }

  return "none";
}


/// a is included in b ?
bool setInclusion(T)(T[] a, T[] b)
{
  assert (a.isSorted);
  assert (b.isSorted);
  
  if (a.length > b.length) return false;

  auto intersection = setIntersection(a, b).array;
  /*
  foreach (i; 0..a.length) {
    
    if (a[i] != b[i]) return false;
  }
  return true;
  */
  if (intersection == a) return true;
  else return false;
}


//----------------------------------------------------------
// OUTPUT
//----------------------------------------------------------


void writeIntervals(string filenamePrefix, TStructure[][] T)
{
  File fout;
  // Write the points in a cycle
  // dim, birth time, death time, flag indicating whather it intercects
  fout = File(filenamePrefix ~ "_cycles", "w");
  foreach (dim, TByDim; T) {
    if (dim < 3) continue;
    foreach (jj, t; TByDim) {
      if (t.mark) continue;
      if (t.birthTime == double.nan)
        throw new Exception("Not marked but without birth time.");
      
      ulong[] tmp = [];
      foreach(e; t.addedStandardBasis) {
        auto bid = T[dim][e].boundaryIDs;
        tmp = setSymmetricDifference(tmp, bid).array;
      }
      
      auto currentBirthTime = tmp.map!(x => T[dim - 1][x].deg).reduce!max;
      auto currentDeathTime = t.addedStandardBasis.map!(x => T[dim][x].deg).reduce!max;
      auto optSuccessFlag = (t.birthTime == currentBirthTime) && (t.deathTime == currentDeathTime);
      
      //auto currentBirthTime = "dummy";
      //auto currentDeathTime = "dummy";
      //auto optSuccessFlag = "dummy";
      
      fout.write(t.dim - 1, "\t", t.birthTime, "\t", t.deathTime,
                 "\t", checkIntersection(jj, t, TByDim), "\t",
                 optSuccessFlag, "\t", currentBirthTime, "\t", currentDeathTime);
      foreach (i; t.cyclePointIDs(T)) fout.write("\t", i);
      fout.writeln;          
    }
  }
}



//---------------------------------------------------------------------------
// INPUT
//---------------------------------------------------------------------------


TStructure[][] readFiltration(string filename, double[][] points)
{
  auto app = new Appender!(TStructure[])[](4);
  ulong[4] counter = [0, 0, 0, 0];
  
  foreach (line; File(filename).byLine) {
    auto fields = line.to!string.strip.split("\t");
    auto dim = fields.length - 2;
    auto deg = fields[0].to!double;
    auto pointIDs = fields[1..$].map!(x => x.to!ulong).array;

    app[dim].put(new TStructure(pointIDs, deg, counter[dim], points));
    counter[dim]++;
  }
  
  return [app[0].data, app[1].data, app[2].data, app[3].data];
}


double[][] readPoints(string filename)
{
  double[][] points;
  foreach (line; File(filename).byLine) {
    if (line[0] == '#') continue;
    points ~= line.to!string.strip.split("\t")[0..3].map!(to!double).array;
  }
  return points;
}

