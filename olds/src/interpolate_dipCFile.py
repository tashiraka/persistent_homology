import sys
import pandas as pd
import numpy as np


input_file_name = sys.argv[1] # 3dg files of GSE117874.
bin_size = int(sys.argv[2]) # [bp], e.g. 20000 for GSE117874
out_file_name = sys.argv[3]


# Read chromosome sizes
chrom_sizes = {}
chrom_size_line_count = 0

with open(input_file_name) as fin:
    for line in fin:
        if line[0] == '#':
            # Chromosome size lines
            fields = line.split()
            chrom = fields[1]
            size = int(fields[2])

            chrom_sizes[chrom] = size
            chrom_size_line_count += 1
        else:
            break


# Read the three-dimensional positions of a 3D model
#model = pd.read_table(input_file_name, header=None, skiprows=chrom_size_line_count,
#                      usecols=[0, 1, 2, 3, 4],
#                      names=['chrom', 'geomic_pos', 'x', 'y', 'z'],
#                      dtype={'chrom': str, 'geomic_pos': int, 'x': float, 'y': float, 'z': float})
model = pd.read_table(input_file_name, header=None, skiprows=chrom_size_line_count,
                      usecols=[2, 3, 4],
                      names=['x', 'y', 'z'],
                      dtype={'x': float, 'y': float, 'z': float})


# Interpolate linearly the missing positions
#chroms = model.chrom.values
#genomic_pos = model.genomic_pos.values
#x = model.x.values
#y = model.y.values
#z = model.z.values


model.to_csv(out_file, sep='\t', header=None, index=False)
