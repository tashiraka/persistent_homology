/**
 * Assumption
 * The IDs of points in filtration have been already sorted.
 * Z_2 coefficient
 * If any cycles become boundaries, the cycles will be optimized.
 **/


import std.stdio, std.typecons, std.array, std.conv, std.algorithm, std.string, std.parallelism;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto filtrationFile = args[1];
     auto outFilePrefix = args[2];

     writeln("Reading filtration");
     auto T = readFiltration(filtrationFile);
     writeln("Boundary operator");
     boundaryOperator(T);
     writeln("Computing intervals");
     computerIntervals(T);
     writeln("Writing intervals");
     writeIntervals(outFilePrefix, T);
   }
 }


void writeIntervals(string filenamePrefix, TStructure[][] T)
{
  auto outCycleSimplicesFile = filenamePrefix ~ "_cycleSimplices";
  auto outCyclesFile = filenamePrefix ~ "_cycles";

  auto foutCycleSimplices = File(outCycleSimplicesFile, "w");
  auto foutCycles = File(outCyclesFile, "w");
  
  foreach (dim, TByDim; T) {
    if (dim != 0) {
      foreach (t; TByDim) {
        if (t.mark) { // colVec == 0, This is cycle.
          
          if (t.birthTime != double.nan) {
            foutCycleSimplices.write(t.dim, "\t",
                                     t.birthTime, "\t", t.deathTime, "\t");
            foreach (k, i; t.addedStandardBasis) {
              foreach (j, id; TByDim[i].pointIDs) {
                foutCycleSimplices.write(id);
                if (j < TByDim[i].pointIDs.length - 1)
                  foutCycleSimplices.write(" "); 
              }
              if (k < t.addedStandardBasis.length - 1)
                foutCycleSimplices.write("\t");
            }
            foutCycleSimplices.writeln;
          
            foutCycles.write(t.dim, "\t", t.birthTime, "\t", t.deathTime);
            foreach (i; t.cyclePointIDs(TByDim)) foutCycles.write("\t", i);
            foutCycles.writeln;          
          }
        }
      }
    }
  }
}



TStructure[][] readFiltration(string filename)
{
  auto app = new Appender!(TStructure[])[](4);
  ulong[4] counter = [0, 0, 0, 0];
  
  foreach (line; File(filename).byLine) {
    auto fields = line.to!string.strip.split("\t");
    auto dim = fields.length - 2;
    auto deg = fields[0].to!double;
    auto pointIDs = fields[1..$].map!(x => x.to!ulong).array;

    app[dim].put(new TStructure(pointIDs, deg, counter[dim]));
    counter[dim]++;
  }
  
  return [app[0].data, app[1].data, app[2].data, app[3].data];
}


void boundaryOperator(TStructure[][] T)
{
  foreach (dim, TByDim; T)
    if (dim > 0)
      foreach (ref t; TByDim.parallel)
        foreach (i; 0..(dim+1)) { // dim == points.length - 1
          t.colVec ~= searchIndex(T[dim - 1],
                                  t.pointIDs[0..i] ~ t.pointIDs[(i + 1)..$]);
          t.colVec = sort(t.colVec).array;
          t.boundaryIDs = t.colVec;
        }
}


unittest {
  auto p0 = 0;
  auto p1 = 1;
  auto p2 = 2;
  auto p3 = 3;
  //auto p0 = new Point([0, 0, 0]);
  //auto p1 = new Point([1, 0, 0]);
  //auto p2 = new Point([0, 1, 0]);
  //auto p3 = new Point([0, 0, 1]);

  auto s0 = [new TStructure([p0], 0, 0), new TStructure([p1], 0, 1),
             new TStructure([p2], 0, 2), new TStructure([p3], 0, 3)];
  auto s1 = [new TStructure([p0, p1], 1, 0), new TStructure([p0, p2], 1, 1),
             new TStructure([p0, p3], 1, 2), new TStructure([p1, p2], 2, 3),
             new TStructure([p2, p3], 2, 4), new TStructure([p1, p3], 2, 5)];
  auto s2 = [new TStructure([p0, p1, p2], 2, 0), new TStructure([p0, p1, p3], 2, 1),
             new TStructure([p0, p2, p3], 2, 2), new TStructure([p1, p2, p3], 3, 3)];
  auto s3 = [new TStructure([p0, p1, p2, p3], 3, 0)];
  
  TStructure[][] s = [s0, s1, s2, s3];
  s.boundaryOperator;
  
  assert (s[1][0].colVec == [0, 1], s[1][0].colVec.to!string);
  assert (s[1][1].colVec == [0, 2], s[1][1].colVec.to!string);
  assert (s[1][2].colVec == [0, 3], s[1][2].colVec.to!string);
  assert (s[1][3].colVec == [1, 2], s[1][3].colVec.to!string);
  assert (s[1][4].colVec == [2, 3], s[1][4].colVec.to!string);
  assert (s[1][5].colVec == [1, 3], s[1][5].colVec.to!string);
  assert (s[2][0].colVec == [0, 1, 3], s[2][0].colVec.to!string);
  assert (s[2][1].colVec == [0, 2, 5], s[2][1].colVec.to!string);
  assert (s[2][2].colVec == [1, 2, 4], s[2][2].colVec.to!string);
  assert (s[2][3].colVec == [3, 4, 5], s[2][3].colVec.to!string);
  assert (s[3][0].colVec == [0, 1, 2, 3], s[3][0].colVec.to!string);
}


ulong searchIndex(TStructure[] TByDim, ulong[] targetPointIDs)
{
  foreach (i, t; TByDim) 
    if (t.pointIDs == targetPointIDs) return i;
  throw new Exception("k-simplex's boundary is not included in (k-1)-simplex: "
                      ~ targetPointIDs.to!string);
}


unittest {
  auto p0 = 0;
  auto p1 = 1;
  auto p2 = 2;
  auto p3 = 3;
  //auto p0 = new Point([0, 0, 0]);
  //auto p1 = new Point([1, 0, 0]);
  //auto p2 = new Point([0, 1, 0]);
  //auto p3 = new Point([0, 0, 1]);

  auto s0 = [new TStructure([p0], 0, 0), new TStructure([p1], 0, 1),
             new TStructure([p2], 0, 2), new TStructure([p3], 0, 3)];
  auto s1 = [new TStructure([p0, p1], 1, 0), new TStructure([p0, p2], 1, 1),
             new TStructure([p0, p3], 1, 2), new TStructure([p1, p2], 2, 3),
             new TStructure([p2, p3], 2, 4), new TStructure([p1, p3], 2, 5)];
  auto s2 = [new TStructure([p0, p1, p2], 2, 0), new TStructure([p0, p1, p3], 2, 1),
             new TStructure([p0, p2, p3], 2, 2), new TStructure([p1, p2, p3], 3, 3)];
  auto s3 = [new TStructure([p0, p1, p2, p3], 3, 0)];

  assert (searchIndex(s0, [p0]) == 0, searchIndex(s0, [p0]).to!string);
  assert (searchIndex(s0, [p1]) == 1, searchIndex(s0, [p1]).to!string);
  assert (searchIndex(s0, [p2]) == 2, searchIndex(s0, [p2]).to!string);
  assert (searchIndex(s0, [p3]) == 3, searchIndex(s0, [p3]).to!string);
  assert (searchIndex(s1, [p0, p1]) == 0, searchIndex(s1, [p0, p1]).to!string);
  assert (searchIndex(s1, [p0, p2]) == 1, searchIndex(s1, [p0, p2]).to!string);
  assert (searchIndex(s1, [p0, p3]) == 2, searchIndex(s1, [p0, p3]).to!string);
  assert (searchIndex(s1, [p1, p2]) == 3, searchIndex(s1, [p1, p2]).to!string);
  assert (searchIndex(s1, [p2, p3]) == 4, searchIndex(s1, [p2, p3]).to!string);
  assert (searchIndex(s1, [p1, p3]) == 5, searchIndex(s1, [p1, p3]).to!string);
  assert (searchIndex(s2, [p0, p1, p2]) == 0, searchIndex(s2, [p0, p1, p2]).to!string);
  assert (searchIndex(s2, [p0, p1, p3]) == 1, searchIndex(s2, [p0, p1, p3]).to!string);
  assert (searchIndex(s2, [p0, p2, p3]) == 2, searchIndex(s2, [p0, p2, p3]).to!string);
  assert (searchIndex(s2, [p1, p2, p3]) == 3, searchIndex(s2, [p1, p2, p3]).to!string);
}


void computerIntervals(TStructure[][] T)
{
  foreach(dim, ref TByDim; T) {
    foreach(j, ref t; TByDim) {
      if(dim != 0) t.removePivotRow(T[dim - 1], T[dim]);

      if (dim == 0 && t.colVec !is null)
        throw new Exception("ERROR: dim == 0 && d !is null");

      if(t.colVec is null) {
        t.mark = true;
        t.birthTime = t.deg;
      }
      else {
        auto i = maxIndex(t.colVec);
        T[dim - 1][i].pivotColID = j;
        T[dim - 1][i].deathTime = t.deg;        
      }
    }
  }

  // cannot optimized
  foreach (dim, ref TByDim; T) 
    foreach (j, ref t; TByDim)
      if (dim != 0 && t.mark && t.isEmpty) {
        assert (t.birthTime == double.nan);
        throw new Exception("Not dead cycle"); 
        //t.birthTime = t.deg;
        //t.deathTime = double.infinity;
      }
}


ulong maxIndex(ulong[] d)
{
  return d[$-1]; // d is sorted in this program.
  //return d.reduce!max;
}


class TStructure
{
  // When row of (k+1)-th boundary matrix
  ulong pivotColID = ulong.max; //Let this T be in C_{k-1}. j is C_k ID corresponding to this.
 
  // When column of k-th boundary matrix
  bool mark = false; //redued column vecter is 0.
  ulong[] addedStandardBasis = null; //Trace of column adding
  ulong[] colVec = null; //Column vector, always sorted

  // Original standard basis
  ulong[] pointIDs = null;
  ulong[] boundaryIDs = null;
  double deg = double.nan;
  ulong dim() @property {return pointIDs.length - 1;}

  // Corresponding persistence intervals
  double birthTime = double.nan;
  double deathTime = double.nan;
  
  this(ulong[] pids, double dg, ulong selfID)
  {
    pointIDs = pids;
    deg = dg;
    addedStandardBasis = [selfID];
  }

  bool isEmpty()
  {
    return pivotColID == ulong.max;
  }
  
  // This method keeps colVec sorted.
  void removePivotRow(TStructure[] TRows, TStructure[] TCols)
  {
    // Remove unmarked terms in boundary of this base
    colVec = colVec.filter!(x => TRows[x].mark).array; // keeping sorted
    assert (colVec.isSorted);

    // rowID: candidate of pivot
    // TColmns[TRow[rowID].pivotColID]: column corresponds to rowID by pivot
    while (colVec !is null) {
      auto rowID = maxIndex(colVec);

      if (TRows[rowID].isEmpty) break;

      columnAdding(TCols[TRows[rowID].pivotColID].colVec); // keeping sorted
      assert (colVec.isSorted);

      // Column adding must be traced
      addedStandardBasis
        = setSymmetricDifference(addedStandardBasis,
                                 TCols[TRows[rowID].pivotColID].addedStandardBasis).array;
    }
  }

  /**
   * Column adding operation of Z_2-coefficient module 
   * eqaul to symmetric difference (in terms of logical operation, XOR).
   */
  void columnAdding(ulong[] addingColumn)
  {
    // The two ranges, facet and addinColumn are assumed
    // to be sorted, and the output is also sorted.
    assert (colVec.isSorted);
    assert (addingColumn.isSorted);
    colVec = setSymmetricDifference(colVec, addingColumn).array;
  }

  
  ulong[] cyclePointIDs(TStructure[] TByDim)
  {
    ulong[] pids;
    foreach (i; addedStandardBasis) {
      pids = merge(pids, TByDim[i].pointIDs).uniq.array;
    }
    return pids;
  }
}
